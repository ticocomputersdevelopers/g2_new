<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Service\Mailer;

class SendMailCrmCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'send:mail:crm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail notification about actions on CRM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        
       if(AdminCrm::checkDateActionCrm(date('Y-m-d'))){
                $mailFrom='info@selltico.com';
            $subject='Danas imate aktivne akcije';

            $imenik_id=DB::table('imenik_crm')->select('imenik_crm.imenik_id')->join('crm_akcija', 'crm_akcija.crm_id', '=', 'imenik_crm.crm_id')->whereDate('crm_akcija.datum','=',date('Y-m-d'))->get();
            
        foreach ($imenik_id as $id) {
            $body= '';
            $mailTO= '';
            $akcije_danas=DB::table('crm_akcija')
                ->select('crm_akcija.opis','crm_akcija.datum','crm_akcija_tip.naziv','crm_akcija.flag_zavrseno','crm_akcija.crm_id', 'crm_akcija.datum_zavrsetka','partner.naziv as partner_ime','imenik.ime as komercijalista', 'imenik.login as mail')
                ->whereDate('crm_akcija.datum','=',date('Y-m-d'))
                ->where('imenik_crm.imenik_id',$id->imenik_id)
                ->join('crm', 'crm_akcija.crm_id', '=', 'crm.crm_id')
                ->join('imenik_crm', 'imenik_crm.crm_id', '=', 'crm.crm_id')
                ->join('imenik','imenik_crm.imenik_id', '=', 'imenik.imenik_id')
                ->join('partner', 'crm.partner_id', '=', 'partner.partner_id')
                ->join('crm_akcija_tip', 'crm_akcija.crm_akcija_tip_id', '=', 'crm_akcija_tip.crm_akcija_tip_id')->orderBy('datum','asc')->get();
               foreach ($akcije_danas as $akcija) {
                    $body .= 'Postovani '.$akcija->komercijalista. '<br>' .$akcija->datum.'<br>'.'Imate akciju: '. $akcija->naziv . '<br>'.'Partner:  ' . $akcija->partner_ime . '<br>'.'Detalji: ' . $akcija->opis. '<br>';
                    $mailTO=$akcija->mail;
          
                }
                
            Mailer::send($mailFrom,$mailTO,$subject,$body);
            }
            
        
        }
        if(AdminCrm::checkDateSertifikatCrm(date('Y-m-d'))){
                $mailFrom='info@selltico.com';
                $subject='Za 14 dana istice sertifikat';


        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [

        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [

        ];
    }

}