<?php
ini_set('memory_limit', '-1');

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class LinkedRobaAndDCCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'roba:dc';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'LinkedRobaAndDCCommand.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[strval($article->id_is)] = $article->roba_id;
		}
		return $mapped;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$mappedArticles = self::getMappedArticles();

		$products_file = 'el_rasveta.xlsx';
		// PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
        $excelObj = $excelReader->load($products_file);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

       	$mappedCodes = array();
        for ($row = 1; $row <= $lastRow; $row++) {

            $id_is = trim($worksheet->getCell('A'.$row)->getValue());
            $skd = strtoupper(trim(str_replace('rel-','',str_replace('REL-','',trim($worksheet->getCell('B'.$row)->getValue())))));

            $codesMapped[$skd] = $id_is;
        }


        foreach(DB::table('dobavljac_cenovnik')->where('partner_id',850)->get() as $dc){
        	if(isset($codesMapped[trim($dc->sifra_kod_dobavljaca)])){
        		$id_is = $codesMapped[trim($dc->sifra_kod_dobavljaca)];
        		if(isset($mappedArticles[strval($id_is)])){
        			$roba_id = $mappedArticles[strval($id_is)];
        			
        			//DB::table('dobavljac_cenovnik')->where('dobavljac_cenovnik_id',$dc->dobavljac_cenovnik_id)->update(array('roba_id'=>$roba_id,'povezan'=>1));
        			//DB::table('roba')->where('roba_id',$roba_id)->update(array('dobavljac_id'=>$dc->partner_id,'sifra_d'=>trim($dc->sifra_kod_dobavljaca)));
        		}else{
        			echo "REL-" .$dc->sifra_kod_dobavljaca , PHP_EOL; 
        		}
        	}
        }

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('width', InputArgument::OPTIONAL, 'Image width.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}