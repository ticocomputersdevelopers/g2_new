@extends('b2b.templates.main')
 
@section('content')  

<!-- START PAGE ANCHOR -->
<input type="hidden" name="JSstart_page">

<div class="row">
    <div class="col-xs-12"> 
 
        <!-- BANNERS SLIDER -->  
        @include('b2b.partials.banners_slider')


        @if(null!=(B2bCommon::akcija()))
            <h2><span class="section-title">Na Akciji</span></h2>
        @endif

        <div class="JSproducts_slick">
            @foreach(B2bCommon::akcija() as $row)
                @include('b2b.partials/product_on_grid')
            @endforeach
        </div> 

        @if(B2bOptions::web_options(151)==1) 
           
            @if(null!=B2bCommon::mostPopularArticlesB2B())
            <h2><span class="section-title">Najpopularniji proizvodi</span></h2>
            <div class="JSproducts_slick">
                @foreach(B2bCommon::mostPopularArticlesB2B() as $row)
                    @include('b2b.partials/product_on_grid')       
                @endforeach            
            </div> 
            @endif 

            @if(null!=B2bCommon::bestSellerB2B())
            <h2><span class="section-title">Najprodavanjiji proizvodi</span></h2>
            <div class="JSproducts_slick">
                @foreach(B2bCommon::bestSellerB2B() as $row)
                    @include('b2b.partials/product_on_grid')          
                @endforeach            
            </div> 
            @endif

            <h2><span class="section-title">Najnoviji proizvodi</span></h2>
            <div class="JSproducts_slick">
                @foreach(B2bCommon::latestAdded() as $row)
                    @include('b2b.partials/product_on_grid')         
                @endforeach            
            </div> 
        @endif  

        <?php $tipovi = DB::table('tip_artikla')->where('tip_artikla_id','<>',-1)->where('active',1)->where('prikaz','!=',0)->orderBy('rbr','asc')->get(); ?>

        @if(count($tipovi) > 0) 
            @foreach($tipovi as $tip)
                @if(All::provera_tipa($tip->tip_artikla_id))        
                <div class="JSshift_right_type">
                    <div class="h2-container row">
                        <h2><span class="section-title JSInlineShort" data-target='{"action":"type","id":"{{$tip->tip_artikla_id}}"}'>{{ Language::trans($tip->naziv) }}</span></h2>
                    </div>
                    <div class="JSproducts_slick row"> 
                        @foreach(B2bCommon::artikli_b2b_tip($tip->tip_artikla_id,20) as $row)
                            @include('b2b.partials/product_on_grid')
                        @endforeach
                    </div>     
                </div>
                @endif
            @endforeach
        @endif
                 

     </div>   
</div>

<script src="{{ B2bOptions::base_url()}}js/slick.min.js" type="text/javascript"></script> 
@endsection
   

 