@extends('b2b.templates.login')
@section('content')  
 
<div class="wrapper">
    <div class="wrapper-inner">
        <div class="row flex"> 
            <div class="col-md-5 col-sm-5 col-xs-12 text-center"> 
                <a href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}">
                    <img class="logo" src="{{ B2bOptions::base_url()}}{{ B2bOptions::company_logo()}}" alt="{{ B2bOptions::company_name()}}">
                </a>
            </div>  

            <div class="col-md-7 col-sm-7 col-xs-12">  
                <div class="bg-fff">
                    <br>
                    @if(Session::has('wings_message'))
                    <div>Uskoro će vam stići email poruka o potvrdi registracije.</div>
                    @endif 
                    <form action="{{route('b2b.registration.store')}}" method="post">  
                        <div class="box-gap">
                            <label class="label" for="naziv">Naziv firme*</label>
                            <input id="naziv" name="naziv" type="text" class="form-control" value="{{ Input::old('naziv') }}">
                            <div class="error">{{ $errors->first('naziv') }}</div>
                        </div> 

                        <div class="box-gap">
                            <label class="label" for="pib">PIB*</label>
                            <input id="pib" name="pib" type="text" class="form-control" value="{{ Input::old('pib') }}">
                            <div class="error">{{ $errors->first('pib') }}</div>
                        </div> 

                        <div class="box-gap">
                            <label class="label" for="broj_maticni">Matični broj</label>
                            <input id="broj_maticni" name="broj_maticni" class="form-control" type="text" value="{{ Input::old('broj_maticni') }}">
                            <div class="error">{{ $errors->first('broj_maticni') }}</div>
                        </div> 

                        <div class="box-gap">
                            <label class="label" for="adresa">Adresa*</label>
                            <input id="adresa" name="adresa" type="text" class="form-control" value="{{ Input::old('adresa') }}">
                            <div class="error">{{ $errors->first('adresa') }}</div>
                        </div> 

                        <div class="box-gap">
                            <label class="label" for="mesto">Mesto*</label>
                            <input id="mesto" name="mesto" type="text" class="form-control" value="{{ Input::old('mesto') }}">
                            <div class="error">{{ $errors->first('mesto') }}</div>
                        </div>
                        
                        <hr>

                        <p class="title">Kontakt osoba</p>

                        <div class="box-gap">
                            <label class="label" for="kontakt_osoba">Ime i prezime*</label>
                            <input id="kontakt_osoba" name="kontakt_osoba" type="text" class="form-control" value="{{ Input::old('kontakt_osoba') }}">
                            <div class="error">{{ $errors->first('kontakt_osoba') }}</div>
                        </div> 

                        <div class="box-gap">
                            <label class="label" for="telefon">Telefon*</label>
                            <input id="telefon" name="telefon" type="text" class="form-control" value="{{ Input::old('telefon') }}">
                            <div class="error">{{ $errors->first('telefon') }}</div>
                        </div> 

                        <div class="box-gap">
                            <label class="label" for="mail">Email*</label>
                            <input id="mail" name="mail" type="text" class="form-control" value="{{ Input::old('mail') }}">
                            <div class="error">{{ $errors->first('mail') }}</div>
                        </div>

                        <div class="box-gap capcha"> 
                            {{ Captcha::img(5, 160, 50) }}<br>
                            <span>{{ Language::trans('Unesite kod sa slike') }}</span>
                            <input type="text" name="captcha-string" tabindex="10" autocomplete="off" {{ $errors->first('captcha') ? 'style="border: 1px solid red;"' : '' }}>
                        </div>

                        <div class="box-gap">
                            <button type="submit" class="sign-in-btn">REGISTRUJ SE</button> 
                            <div class="text-right"> 
                                Imate nalog?
                                <a class="new-psw-btn" href="login">Prijavite se</a>
                            </div>
                        </div>  
                    </form>   
                </div>
            </div>  
        </div>
        
        <div class="text-right"> 
            <a class="logo-selltico" href="https://www.selltico.com/" target="_blank"> 
                B2B ENGINE BY: 
                <img src="{{ Options::base_url()}}images/logo-selltico.png">    
            </a>
        </div>  
    </div>
</div>

<script type="text/javascript">
    @if(Session::has('registration_message'))
    bootbox.alert({
        message: "Uspešno ste uneli podateke. Uskoro ćete dobiti pristupne podatke."
    });
    @endif
</script>
@endsection