@extends('b2b.templates.main')

@section('content')
<?php B2bCommon::articleViewB2B($roba_id); ?>  

<ul class="breadcrumb">
    {{ B2bArticle::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
</ul>

<div class="row">
    <div class="col-xs-12">

        <div itemscope itemtype="http://schema.org/Product">

            <div class="row">
                <!-- PRODUCT PREVIEW IMAGE -->
                <div class="product-preview-image col-md-6 col-sm-6 col-xs-12 no-padding">

                    <div class="product-view">   
                        <img id="JSarticle-img" itemprop="image" class="img-responsive" src="{{ B2bOptions::base_url() }}{{ B2bArticle::web_slika_big($roba_id) }}" 
                        alt="{{ B2bArticle::seo_title($roba_id)}}"/>

                        <div id="JSarticle-modal-container" class="article-modal-container">  
                            <div class="article-modal-container-inner text-center"> 
                                <span class="JSarticle-modal-close">&times;</span>
                                <div class="modal-img-container"> 
                                    <img id="JSarticle-modal-img" class="img-responsive article-modal-img" src="{{ B2bOptions::base_url() }}{{ B2bArticle::web_slika_big($roba_id) }}" alt="{{ B2bArticle::seo_title($roba_id)}}"/>
                                </div>
                                <div class="modal-galery-container"> {{ B2bArticle::get_list_images($roba_id) }} </div>
                            </div>
                        </div>
                    </div>

                    <div class="JSImgLoop"> 
                        {{ B2bArticle::get_list_images($roba_id) }}  
                    </div>
                </div>
             
                <div class="col-md-6 col-sm-6 col-xs-12 sm-nopadd"> 
                    <!-- PRODUCT PREVIEW INFO -->
                    <?php
                    $lager = B2bArticle::lagerObj($roba_id);
                    $cartAmount = B2bBasket::getB2bQuantityItem($roba_id);
                    $dost_kol = $lager->kolicina - ($lager->rezervisano + $cartAmount);
                    ?>

                    @if( Product::stiker_levo($roba_id) != null )
                        <a class="article-sticker-img">
                            <img src="{{ Options::domain() }}{{product::stiker_levo($roba_id) }}"  />
                        </a>
                    @endif 
                    
                    @if( Product::stiker_desno($roba_id) != null )
                        <a class="article-sticker-img text-right">
                            <img src="{{ Options::domain() }}{{product::stiker_desno($roba_id) }}"  />
                        </a>
                    @endif   

                    @if(!empty(B2bArticle::get_labela($roba_id)))
                    <div class="custom-label inline-block relative">
                        <i class="fa fa-info-circle"></i>
                        {{B2bArticle::get_labela($roba_id)}} 
                    </div>
                    @endif   

                    <h1 class="article-heading" itemprop="name">{{ B2bArticle::seo_title($roba_id)}}</h1>

                    <div>Šifra: 
                        @if(AdminOptions::sifra_view_web()==1)
                        {{Product::sifra($roba_id)}}
                        @elseif(AdminOptions::sifra_view_web()==4)                       
                        {{Product::sifra_d($roba_id)}}
                        @elseif(AdminOptions::sifra_view_web()==3)                       
                        {{Product::sku($roba_id)}}
                        @elseif(AdminOptions::sifra_view_web()==2)                       
                        {{Product::sifra_is($roba_id)}}
                        @endif
                    </div>

                    <ul> 
                        <li>Proizvod iz grupe:{{ B2bArticle::get_grupaB2b($roba_id) }}</li>
                        <li>Proizvodjač: {{ B2bArticle::get_proizvodjac($roba_id) }}</li>

                        @if(B2bOptions::vodjenje_lageraB2B() == 1)
                            <li>Lager {{$dost_kol}} </li>
                            <li>Rezervisano {{($lager->rezervisano + $cartAmount )}} </li>
                        @endif
                    </ul>

                    <!-- CAS Number -->
                    @if(!empty($cas_number))
                        <div>CAS: {{ $cas_number }}</div>
                        <div class="legal-person-text">Artikli iz grupe Hemikalije su dostupni za poručivanje samo pravnim licima.</div>
                    @endif

                    @if(B2bArticle::getStatusArticle($roba_id) == 1)
                    <div class="product-preview-price">
                        <span itemprop="price">{{ B2bBasket::cena(B2bArticle::b2bRabatCene($roba_id)->cena_sa_rabatom) }}</span>
                    </div>
                    @endif

                    <div class="add-to-cart-area">
                        @if(B2bArticle::getStatusArticle($roba_id) == 1)
                        @if($dost_kol>0)
                        <div class="inline-block quantity-change"> 
                            <a class="JSProductListCartLess" href="javascript:void(0)"><i class="fas fa-minus"></i></a>
                            <input class="JSProductListCartAmount add-amount" data-max-quantity="{{$dost_kol}}" type="text" value="1">
                            <a class="JSProductListCartMore" href="javascript:void(0)"><i class="fas fa-plus"></i></a> 
                        </div> 

                        <button class="{{ $grupa_pr_id != 53 ? 'button add-to-cart add-to-cart-btn' : 'button add-to-cart add-to-cart-btn button-color'}}" data-roba-id="{{$roba_id}}">
                             Dodaj u korpu
                        </button>
                        @if(!is_null(B2bPartner::dokumentiUser()))
                        <button class="{{ $grupa_pr_id != 53 ? 'button add-to-offer JSadd-to-offer-product' : 'button add-to-offer JSadd-to-offer-product button-color'}}" data-roba-id="{{$roba_id}}">Dodaj u ponudu</button>
                        @endif

                        <!-- <a class="add-amount-less"  href="javascript:void(0)"><</a> -->
                        <!-- <a class="add-amount-more" data-max-quantity="{{$dost_kol}}" href="javascript:void(0)">></a> -->
                        @else

                        <button class="{{ $grupa_pr_id != 53 ? 'button not-available' : 'button not-available button-color'}}">Nije dostupno</button>

                        @endif
                        @else
                        <button class="{{ $grupa_pr_id != 53 ? 'button add-to-cart' : 'button add-to-cart button-color'}}" >{{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($roba_id),'naziv') }}</button>
                        @endif
                    </div>

                @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                    @if(Session::has('b2c_admin'.B2bOptions::server()))
                        <a class="article-edit-btn" target="_blank" href="{{ B2bOptions::base_url() }}admin/product/{{ $roba_id }}">IZMENI ARTIKAL</a>
                    @endif
                @endif
 
            </div>
        </div>

    <!-- PRODUCT PREVIEW TABS-->
        <div class="product-preview-tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Opis</a></li>
                <li><a data-toggle="tab" href="#tech_docs">Sadržaji</a></li> 
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    {{ B2bArticle::get_opis($roba_id) }} 
                    {{ B2bArticle::get_karakteristike($roba_id) }}
                </div>

                <div id="tech_docs" class="tab-pane fade">

                    @if(B2bOptions::web_options(120) && count($fajlovi) > 0) 
                    @foreach($fajlovi as $row)
                    <div class="files-list-item">
                        <a class="files-link" href="{{ $row->putanja != null ? Options::domain().$row->putanja : $row->putanja_etaz }}" target="_blank">
                            <img src="{{ Options::domain() }}images/file-icon.png" alt="{{ $row->naziv }}">

                            <div class="files-list-item">
                                <div class="files-name">{{ $row->naziv }}</div> 
                            </div>
                        </a>
                    </div>
                    @endforeach 
                    @endif
                </div> 
            </div>
        </div> 

        <br>

        <!-- TAGS -->
    <div class="tags"> 
        @if(B2bArticle::tags_count($roba_id)>1)
            {{ B2bArticle::get_tags($roba_id) }} 
        @endif
    </div>
        <!-- RELATED PRODUCTS -->
        @if(B2bArticle::getVezani($roba_id)>0) 
        <h2 class="slickTitle"><span class="section-title">Vezani artikli</span></h2>
        <div class="JSproducts_slick">
            @foreach(B2bArticle::getVezani($roba_id) as $row)
            @include('b2b.partials/product_on_grid') 
            @endforeach
        </div>
        @endif

        <!-- Table View -->

        <?php $query_products = B2bArticle::getSrodni($roba_id); ?>
        @include('b2b/partials/product_on_list_srodni')

        <!-- End of Table View -->


        </div>   
    </div> <!-- COL-XS-12 -->
</div>

<script src="{{ B2bOptions::base_url()}}js/slick.min.js" type="text/javascript" ></script> 

<script>
    $(document).ready(function () {
        var id = {{$roba_id}};
// $('.add-amount-less').click(function (){
//    var quantity = $('.add-amount').val();
//    if(quantity <= 1){
//        $('.info-popup').fadeIn().delay(2000).fadeOut();

//        $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
//    }
//    else {
//        $('.add-amount').val(quantity-1);
//    }
// });

//  $('.add-amount-more').click(function (){
//      var quantity = $('.add-amount').val();
//      var max = $(this).data('max-quantity');
//      if(quantity == max){
//          $('.info-popup').fadeIn().delay(2000).fadeOut();

//          $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
//      }
//      else {
//          $('.add-amount').val(Number(quantity)+1);
//      }
//  });

//  $('.add-amount').change(function() {
//     var quantity = $(this).val();
//     var max = $(this).data('max-quantity');
//      if(quantity <= 1){
//          $('.info-popup').fadeIn().delay(2000).fadeOut();

//          $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
//          $(this).val(1);
//      }
//      else if(quantity >= max){
//          $('.info-popup').fadeIn().delay(2000).fadeOut();

//          $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
//          $(this).val(max);
//      }

//  });
$('.add-to-cart-btn').click(function (){
    var quantity = $('.add-amount').val();
    var max = $('.add-amount').data('max-quantity');
    if(quantity < 1){
        $('.info-popup').fadeIn().delay(1200).fadeOut();
        $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
        $(this).val(1);
    }
    else if(quantity > max){
        $('.info-popup').fadeIn().delay(1200).fadeOut();
        $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
        $(this).val(max);
    }
    else {
        var _this = $(this);
        $.ajax({
            type: "POST",
            url:'{{route('b2b.cart_add')}}',
            cache: false,
            data:{roba_id:id, status:2, quantity:quantity},  
            success:function(res){
                $('.info-popup').fadeIn().delay(1200).fadeOut();
                $('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
                $('#broj_cart').text(res.countItems);
                $('#header-cart-content').html(res.cartContent);
                $('.add-amount').data('max-quantity', res.cartAvailable);
                $('.add-amount-more').data('max-quantity', res.cartAvailable);
                location.reload();
            }
        });
    }
});
// $('.addCart').click(function(){
//    var roba_id = $(this).data('product-id');
//    var max = $(this).data('max-quantity');
//     if(max <=0 ){
//         $('.info-popup').fadeIn().delay(800).fadeOut();
//         $('.info-popup .popup-inner').html("<p class='p-info'>Za dati arikal dodali ste maksimalnu količinu u korpu.</p>");
//     }
//     else {
//         var _this = $(this);
//         $.ajax({
//             type: "POST",
//             url:'{{route('b2b.cart_add')}}',
//             cache: false,
//             data:{roba_id:roba_id, status:2, quantity:1},
//             success:function(res){
//                 $('.info-popup').fadeIn().delay(800).fadeOut();
//                 $('.info-popup .popup-inner').html("<p class='p-info'>Artikal je dodat u korpu.</p>");
//                 location.reload();
//             }
//         })
//     }
// });
});

</script>
@endsection