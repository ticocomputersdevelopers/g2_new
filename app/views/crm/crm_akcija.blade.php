@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	
	</script>
@endif
<section id="main-content">
	<div class="row m-subnav">
		<div class="large-2 medium-3 small-12 columns ">
			<a class="m-subnav__link" href="{{AdminOptions::base_url()}}crm/crm_home" >
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">{{ AdminLanguage::transAdmin('Nazad') }}</div>
			</a>
		</div>
	</div>
	<br> 

	<div class="row"> 
		<div class="columns large-9 medium-8 anly-box">
			<div class="row article-edit-box crm-edit-box">
				<div class="columns large-10 medium-9 small-8"> 
					<h1>{{ AdminLanguage::transAdmin('Partner : ') }}{{ AdminPartneri::partner_naziv( AdminCrm::getPartnerID($crm_id)) }} </h1>
				</div>

				<div class="columns large-2 medium-3 small-4 text-right margin-h-10">
					<a href="{{AdminOptions::base_url()}}admin/kupci_partneri/partner/{{AdminCrm::getPartnerID($crm_id) }}" target="_blank" class="btn btn-secondary btn-small">
					{{ AdminLanguage::transAdmin('Vidi partnera') }} </a>
				</div>

				<div class="columns medium-12"> 	
					<div class="table-scroll">			
						<table> 
							<thead>
								<tr> 
								<th>{{ AdminLanguage::transAdmin('Akcija') }}</th>
								<th>{{ AdminLanguage::transAdmin('Naziv akcije') }}</th>  
								<th>{{ AdminLanguage::transAdmin('Komentar') }}</th> 
								<th>{{ AdminLanguage::transAdmin('Datum pocetka') }}</th> 
								<th>{{ AdminLanguage::transAdmin('Datum zavrsetka') }}</th> 
								<th></th> 
								</tr>
							</thead>

							<tbody>
								@foreach($akcije as $row) 
								<tr @if($row->flag_zavrseno==1) class="end_flag" @endif>
									<form method="POST" action="{{ AdminOptions::base_url() }}crm/crm_home/crm_akcija/akcija_edit/{{$row->crm_akcija_id}}" > 
										<input type="hidden" value="{{$row->crm_akcija_id}}" name= "crm_akcija_id">
										@if(preg_match("/mail/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			
											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-envelope-o" style="color:#242118"></i></td>
										@elseif(preg_match("/telefon/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			
											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-phone" style="color:#242118"></i>  </td>  
										@elseif(preg_match("/sastanak/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			
											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-users" style="color:#242118"></i>  </td>
										@elseif(preg_match("/prezentov/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			
											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-bullhorn" style="color:#242118"></i></td>
										@elseif(preg_match("/skype/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			
											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-skype" style="color:#242118"></i>  </td>
										@elseif(preg_match("/video/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			
											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-video-camera" style="color:#242118"></i></td>
										@elseif(preg_match("/zoom/i", AdminCrm::getAkcija($row->crm_akcija_tip_id)))			
											<td class="tooltipz" aria-label="{{ AdminLanguage::transAdmin(AdminCrm::getAkcija($row->crm_akcija_tip_id)) }}"><i class="fa fa-bullseye" style="color:#242118"></i></td>
										@else 
											<td><div class="crm-ellipsis-text">{{AdminCrm::getAkcija($row->crm_akcija_tip_id)}} </div></td> 
										@endif

											<td>
												<select class="select-width" name="crm_akcija_tip_id">
												@foreach(DB::table('crm_akcija_tip')->orderBy('crm_akcija_tip_id','asc')->get() as $akcija)
													<option value="{{ $akcija->crm_akcija_tip_id }}" @if($row->crm_akcija_tip_id == $akcija->crm_akcija_tip_id) {{ 'selected' }} @endif>{{ $akcija->naziv }}</option>	 			
												@endforeach
												</select>
											</td>
											<td>
												<textarea style="min-height: auto;" class="min-width-300" type="text" rows="2"  name='opis' aria-label="{{Input::old('opis') ? Input::old('opis') : $row->opis }}">{{Input::old('opis') ? Input::old('opis') : $row->opis }} </textarea>
											</td>				
											<td>@if($row->datum != NULL)
												<input type='text' class="JSdatepickerAction" 
												value="{{date($row->datum )}}"  name='datum'>
												@else
												<input type='text' class="JSdatepickerAction"  name='datum'>
												@endif
											</td> 
											<td>@if($row->datum_zavrsetka != NULL)
												<input type='text' class="JSdatepickerAction" value="{{date($row->datum_zavrsetka)}}" name='datum_zavrsetka'>
												@else
												<input type='text' class="JSdatepickerAction" name='datum_zavrsetka'>
												@endif
											</td> 
																	
											<td>
												<button type="submit" class="button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-save"></i></button>&nbsp;&nbsp;  
													<a href="{{ AdminOptions::base_url() }}crm/crm_home/{{$row->crm_akcija_id}}/akcija_delete" onclick="return confirm('Jeste li sigurni da želite da obrišete akciju?')" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Obriši akciju') }}"><i class="fa fa-times red"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;

													@if($row->flag_zavrseno)  
													<a href="{{ AdminOptions::base_url() }}crm/crm_akcija_vrati/{{ $row->crm_akcija_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Vrati akciju u aktivne') }}"><i class="fa fa-play"></i></a>&nbsp;&nbsp;  

													@else

													<a href="{{ AdminOptions::base_url() }}crm/crm_akcija_zavrsena/{{ $row->crm_akcija_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Završi akciju') }}"><i class="fa fa-power-off"></i></a> 
													@endif
											</td>
									</form>							 
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div> 		
			</div>
		</div>


		<div class="columns large-3 medium-4">
		 	<div class="order-status-p row">

				<div class="column medium-12"> <h1>{{ AdminLanguage::transAdmin('Dodaj novu akciju : ') }}</h1> </div>
		
				
				<form action="{{AdminOptions::base_url()}}crm/crm_home/akcija_create/{$crm_id}" method="post"  autocomplete="false" class="column medium-12">
					<input type="hidden" name="crm_id" value="{{$crm_id}}">
					<input type="hidden" name="crm_akcija_id" value="">
					<div>				
						<div>
							<div class="field-group">		
								<label>{{ AdminLanguage::transAdmin('Crm akcija') }} </label>
								<select name="crm_akcija_tip_id" id="JSNewTypeCheck" class="seo-button-wrapper" onchange="getSelectedValue();">
								@foreach(DB::table('crm_akcija_tip')->orderBy('crm_akcija_tip_id','asc')->get() as $akcija)
								<option value="{{ $akcija->crm_akcija_tip_id }}" selected>{{ $akcija->naziv }}</option>												
								@endforeach
								</select>
							</div> 
						</div>
						<div >
							<label>{{ AdminLanguage::transAdmin('Datum početka') }}</label>
							<input name="datum_kreiranja" value="{{date('Y-m-d')}}" type="date">
							<input name="vreme_kreiranja"  value="10:00:00"  type="time">
						</div>
						<div >
							<label>{{ AdminLanguage::transAdmin('Datum završetka') }}</label>
							<input name="datum_zavrsetka" value="" type="date">
							<input name="vreme_zavrsetka"  value="18:00:00"  type="time">
						</div>			
						<div > 
							<!-- <label>&nbsp;</label> -->
							<label>
								<input type="checkbox" name="flag_zavrseno" value="1">{{ AdminLanguage::transAdmin('Zavrseno') }}
							</label> 
						</div>
									
						<div > 
							<label>{{ AdminLanguage::transAdmin('Napomena') }}</label>
							<textarea name="opis" value=""></textarea>
						</div>		 
		 				<div >
							<div class="btn-container center">
								<button type="submit"  class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button> 
								<a href="{{AdminOptions::base_url()}}crm/crm_home/crm_akcija/{{$crm_id}}"  class="btn btn-danger">{{ AdminLanguage::transAdmin('Poništi izmene') }}</a>		
							</div>
				  		</div>
					</div>
				</form>	
			</div>
		</div>
	</div>
</section>