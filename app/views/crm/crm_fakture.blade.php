@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
<section id="main-content">
	@include('crm/partials/crm_tabs')
	<div class='row'>
				<div class="columns medium-3"> 
					<!-- SEARCH -->
					<div class="m-input-and-button">
						<input type="text" name="search" value="{{ urldecode($search) }}" placeholder={{ AdminLanguage::transAdmin('Pretraga...') }} >

						<button id='JSCRMfakture' class="btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Pretraga') }}</button>

						<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}crm/crm_fakture">{{ AdminLanguage::transAdmin('Poništi') }}</a>
					</div>  
				</div> 
				<!-- <div class="columns medium-1">
					<input name="datum_pocetka" type="text" value="{{ $datum_pocetka }}" autocomplete="on" placeholder="Datum od">
				</div>

				<div class="columns medium-1">
					<input name="datum_kraja" type="text" value="{{ $datum_kraja }}" autocomplete="on" placeholder="Datum do">
				</div>
				<button aria-label="Poništi" class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz">
							<a href="{{AdminOptions::base_url()}}crm/crm_fakture">
								<i class="fa fa-times"  aria-hidden="true"></i>
							</a>
						</button>

 -->
					 <div class="columns medium-2">
					
						<select name="crm_tip_id">
							<option value="" selected>{{ AdminLanguage::transAdmin('Izaberi paket') }}</option>
							<option>{{ AdminCrm::tipoviSelect($tip_ids) }}</option>
						</select>
						<button id="JSTipoviSearch" class="btn btn-primary btn-abs-right">
							<i class="fa fa-check" area-hidden="true"></i>
						</button>
					</div>
					<button aria-label={{ AdminLanguage::transAdmin('Poništi') }} class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz">
							<a href="{{AdminOptions::base_url()}}crm/crm_fakture"><i class="fa fa-times" aria-hidden="true"></i>
							</a>
						</button>
		</div>
				<a href="#" class="btn-edit-simple right JSExportFaktura">{{'Export'}} </a>
				<!-- export faktura modal -->
			<div id="JSselectField" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<a class="close-reveal-modal" aria-label="Close">&#215;</a>
				<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Export') }}</h4>
				<div class="row text-center">
					<h3 class="text-center">{{ AdminLanguage::transAdmin('Izaberite željene kolone') }}:</h3> 
					<div class="columns medium-2"> 
						<input type="checkbox" data-name="partner" class="JSChooseExport" data-status="partner">{{ AdminLanguage::transAdmin('Partner') }} 	
					</div>
					<div class="columns medium-2"> 
						<input type="checkbox" data-name="tip" class="JSChooseExport" data-status="tip">{{ AdminLanguage::transAdmin('Tip') }} 
					</div>
					<div class="columns medium-2"> 
						<input type="checkbox" data-name="datum" class="JSChooseExport" data-status="datum">{{ AdminLanguage::transAdmin('Datum') }} 
					</div>
					<div class="columns medium-2"> 
						<input type="checkbox" data-name="opis" class="JSChooseExport" data-status="opis">{{ AdminLanguage::transAdmin('Opis') }}
					</div> 
					<div class="columns medium-2"> 
						<input type="checkbox" data-name="vrednost" class="JSChooseExport" data-status="vrednost">{{ AdminLanguage::transAdmin('Vrednost') }}
					</div> 
					<div class="columns medium-2"> 
						<input type="checkbox" data-name="valuta" class="JSChooseExport" data-status="valuta">{{ AdminLanguage::transAdmin('Valuta') }}
					</div> 
				</div> 
				<div class="text-center"><br>
					<button data-kind="xls" class="btn btn-primary m-input-and-button__btn JSExportFak" >{{ AdminLanguage::transAdmin('Export XLS') }}</button>
				</div>
			</div>

				<div class="">
					<label>{{ AdminLanguage::transAdmin('Ukupno') }}: <b>{{count($fakture)}}</b> </label> 
					<table class="fixed-table-header" id="JSmyTable">
						<thead class="table-head">
							<tr>@if(count($cekirane)==count($sve))
								<th><a href="{{ AdminOptions::base_url() }}crm/faktura_cekiraj"class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Deselektuj sve') }}"><i class="fa fa-check"></i></a></th>
								@else
								<th><a href="{{ AdminOptions::base_url() }}crm/faktura_cekiraj"class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Selektuj sve') }}"><i class="fa fa-square-o"></i></a></th>
								@endif
								
								<th>{{ AdminLanguage::transAdmin('Partner') }}</th>
								<th> </th>
								<th>{{ AdminLanguage::transAdmin('Vrednost') }}<br>{{ AdminLanguage::transAdmin('bez PDVa') }}</th>  
								<th>{{ AdminLanguage::transAdmin('Paket') }}</th> 		
								<th>{{ AdminLanguage::transAdmin('Fakturisati (na početku ili na kraju meseca)') }}<br>{{ AdminLanguage::transAdmin(' + kurs') }}</th>
								<th>{{ AdminLanguage::transAdmin('Početak')}} <br> {{'fakturisanja'}}</th>
								<th>{{ AdminLanguage::transAdmin('Popust') }}</th>  
								<th>{{ AdminLanguage::transAdmin('Mail za fakturisanje') }}</th>  
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</thead>

						<tbody> 

							@foreach($fakture as $row)

							<tr id={{$row->crm_fakture_id}}> 
								<!-- @if(AdminCrm::trebaFakturisati($row->crm_fakture_id)) class="accounting" @endif -->
								<td>@if($row->flag_poslato)
									<a href="{{ AdminOptions::base_url() }}crm/faktura_nije_poslata/{{$row->crm_fakture_id}}"class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Poslato za mesec') }}"><i class="fa fa-check"></i></a>
									@else
									<a href="{{ AdminOptions::base_url() }}crm/faktura_poslata/{{$row->crm_fakture_id}}"class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Treba da se posalje') }}"><i class="fa fa-square-o"></i></a>
									@endif
									
									
								</td> 
								<form action="{{AdminOptions::base_url()}}crm/crm_fakture_save/{{$row->crm_fakture_id}}"  method="POST" autocomplete="false">
									<input type='hidden' value="{{$row->crm_fakture_id}}" name='crm_fakture_id'>
									<td><input type='hidden' value="{{$row->partner_id}}" name='partner_id'>{{$row->ime}} </td>
									<td><a href="#" class="open-accountNew" data-id="{{$row->partner_id}}"><i class="fa fa-file-text-o" style="color:green"></i></a>
									</td>
									<td style="width: 100px;">
										<div class="relative">
											<input id='valueField' type='number' name='vrednost' value="{{Input::old('vrednost') ? Input::old('vrednost') : $row->vrednost }}">

											@if($row->kursna_lista_id)
											
											<a href="{{ AdminOptions::base_url() }}crm/crm_fakture/{{ $row->crm_fakture_id }}/dinari" class="tooltipz btn btn-abs-right" aria-label="{{ AdminLanguage::transAdmin('Preračunaj u evre')}}" style='background: #FFFFFF' id="dinary">{{ AdminLanguage::transAdmin('RSD')}}</a></td>
											@else
										
											<a href="{{ AdminOptions::base_url() }}crm/crm_fakture/{{ $row->crm_fakture_id }}/preracunaj" class="tooltipz btn btn-abs-right" aria-label="{{ AdminLanguage::transAdmin('Preračunaj u dinare')}}" style='background: #FFFFFF' id="evry">{{ AdminLanguage::transAdmin('EUR')}}</a> </td>
											
											@endif
										</div>
									<td>
										<select name="crm_tip_id" class="select-width">
											@foreach(DB::table('crm_tip')->orderBy('crm_tip_id','asc')->get() as $tip)	
											<option value="{{ $tip->crm_tip_id  }}"@if($row->crm_tip_id == $tip->crm_tip_id) selected @endif>{{ AdminCrm::getTipNaziv($tip->crm_tip_id) }}</option>
											@endforeach
										</select>
									</td>

									<td><input type='text' value="{{$row->opis}}" name='opis'> </td>
									<td class="datepicker-col__box"><input type='text' class="JSdatepickerAction" value="{{ date('d-m-Y',strtotime($row->datum_fakturisanja)) }}" name='datum_fakturisanja'>
									</td>	
									<td style="width: 250px;"><input type='text' value="{{$row->popust}}" name='popust'> </td>
									<td>
										<input type='email' value="{{AdminCrm::mailZaFakturisanje($row->partner_id)}}" name="email_fakture">
									</td>
									
									
									<td><button type="submit" class="button-option tooltipz"aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-save"></i>
									</button></td>
									<td> 
										<a href="{{ AdminOptions::base_url() }}crm/crm_fakture_delete/{{ $row->crm_fakture_id }}" onclick="return confirm('Jeste li sigurni da želite da obrišete fakturu')" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Obriši fakturu') }}"><i class="fa fa-times red"></i></a>
									</td>
									<td>
										<a href="{{ AdminOptions::base_url() }}crm/crm_sertifikati/{{ $row->partner_id }}" class="tooltipz btn btn-small" aria-label="{{ AdminLanguage::transAdmin('Sertifikat') }}"><i class="fa fa-arrow-right"></i></a>
									</td> 
									

								</form>
							</tr>
							@endforeach

						</tbody>
					</table>
					<!-- modal dokumenta --> 
						<div id="accountNew" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
							<a class="close-reveal-modal" aria-label="Close">&#215;</a>

							<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Partner') }}</h4>

							<div id="JSAccountNew"></div>
						</div>
						  
				</div>
	
</section>
<script type="text/javascript">
         document.addEventListener("DOMContentLoaded", function(event) { 
            var scrollpos = localStorage.getItem('scrollpos');
            if (scrollpos) window.scrollTo(0, scrollpos);
        });

        window.onbeforeunload = function(e) {
            localStorage.setItem('scrollpos', window.scrollY);
        };
  </script>		