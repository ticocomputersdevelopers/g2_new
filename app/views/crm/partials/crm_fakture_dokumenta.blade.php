
						<div class="columns">
								<div class="relative"> 
								<h2 class="title-med">{{AdminCrm::getPartnerNaziv($partner_id)}}</h2>
								
								
							 	<table>
							 		<tr>
								 		<th>{{'Racun broj'}}</th>
								 		<th>{{'Datum'}}</th>
								 		<th>{{'Izmeni'}}</th>
								 		<th></th>
							 		</tr>
							 		@foreach($racuni as $row)
							 		<tr>
							 			
							 			<td>{{ $row->dokument_broj }} </td>
							 			<td>{{ $row->racun_datum }}</td>
							 			<td><a href="{{DokumentiOptions::base_url()}}dokumenti/racun/{{$row->racun_id}}">{{ AdminLanguage::transAdmin('Izmeni') }}</a></td>
							 			<td>
							 				<a href="mailto:{{AdminCrm::mailZaFakturisanje($row->id_partnera)}}?subject={{ 'Faktura' }}&body=Poštovani/na {{ $row->osoba }}," class="tooltipz btn btn-small" aria-label="{{ AdminLanguage::transAdmin('Posalji fakturu') }}"><i class="fa fa-paper-plane"></i></a> </td>

							 		</tr>
							 		@endforeach
							 	</table>
								
						</div>