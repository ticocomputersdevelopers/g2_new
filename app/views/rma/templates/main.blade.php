<!DOCTYPE html>
<html>
    <head>
        <title>{{$title}}</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="msapplication-tap-highlight" content="no"/> 

        <link rel="icon" type="image/png" href="{{ RmaOptions::base_url()}}favicon.ico">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href="{{ RmaOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ RmaOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{ RmaOptions::base_url()}}css/alertify.core.css">
        <link rel="stylesheet" href="{{ RmaOptions::base_url()}}css/alertify.default.css">
        <link href="{{ RmaOptions::base_url()}}css/rma/main.css" rel="stylesheet" type="text/css" />
        <script src="{{ RmaOptions::base_url()}}js/alertify.js" type="text/javascript"></script>


    </head>
<body>

    <div class="flex-wrapper">
       
        <!-- HEADER -->
        @include('rma.partials.header')
        <!-- MAIN MENU -->
        @yield('content')
    </div> <!-- end of flex-wrapper -->


    <input type="hidden" id="base_url" value="{{RmaOptions::base_url()}}" />
    <input type="hidden" id="live_base_url" value="{{AdminOptions::live_base_url()}}" />
    <script src="{{ RmaOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ RmaOptions::base_url()}}js/jquery-ui.min.js"></script>
    <script src="{{ RmaOptions::base_url()}}js/foundation.min.js" type="text/javascript"></script>
    <script src="{{ RmaOptions::base_url()}}js/rma/main.js" type="text/javascript"></script>
    <script src="{{ RmaOptions::base_url()}}js/rma/funkcije.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
   
    @if(in_array($strana,array('rma','radni_nalog_prijem','radni_nalog_opis_rada','radni_nalog_dokumenti','radni_nalog_predaja')))
        <link href="{{ RmaOptions::base_url()}}css/jquery.datetimepicker.min.css" rel="stylesheet">
        <script src="{{ RmaOptions::base_url()}}js/jquery.datetimepicker.full.min.js"></script>
        <script src="{{ RmaOptions::base_url()}}js/rma/rma.js" type="text/javascript"></script>
    @elseif($strana=='operacije')
        <script src="{{ RmaOptions::base_url()}}js/rma/operacije.js" type="text/javascript"></script>
    @elseif($strana=='serviseri')
        <script src="{{ RmaOptions::base_url()}}js/rma/serviseri.js" type="text/javascript"></script>
    @endif

    @if(Session::has('message'))
        <script>
            alertify.success('{{ Session::get('message') }}');
        </script>
    @endif
    @if(Session::has('error_message'))
        <script>
            alertify.error('{{ Session::get('error_message') }}');
        </script>
    @endif
</body>
</html>