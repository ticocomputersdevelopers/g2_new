
<div>
	<div class="row">
		<div class="medium-6 columns">
			<label for="">Uslužni servis</label>
			<select id="JSUsluzniServisId" >
				{{ RMA::usluzniServisSelect($radni_nalog_trosak->partner_id)}}
			</select>
		</div>
		<div class="medium-6 columns">
			<label for="">Broj računa</label>
			<input type="text" id="JSBrojRacuna" value="{{ $radni_nalog_trosak->broj_racuna }}">
		</div>
	</div>

	<div class="row">
		<div class="medium-6 columns">
			<label for="">Datum računa</label>
			<input type="text" id="JSDatumRacuna" value="{{ $radni_nalog_trosak->datum_racuna }}">
		</div>
		<div class="medium-6 columns">
			<label for="">Vrednost računa</label>
			<input type="text" id="JSVrednostRacuna" value="{{ $radni_nalog_trosak->vrednost_racuna }}">
		</div>
	</div>

	<div class="row">
		<div class="medium-12 columns">
			<label for="">Opis</label>
			<textarea id="JSTrosakOpis" rows="3">{{ $radni_nalog_trosak->opis }}</textarea>
		</div>
	</div>

	<div class="btn-container center"> <br>
		<button id="JSRNTrosakSubmit" class="save-it-btn btn btn-primary" data-radni_nalog_trosak_id="{{$radni_nalog_trosak->radni_nalog_trosak_id}}" data-radni_nalog_id="{{$radni_nalog->radni_nalog_id}}" >Sačuvaj</button>
	</div> 
</div>