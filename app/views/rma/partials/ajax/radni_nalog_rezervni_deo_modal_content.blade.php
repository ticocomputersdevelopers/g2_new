<div class="row">
	<div class="medium-6 columns">
		<label for="">Rezervni deo</label>
		<input type="hidden" id="JSRDRobaId" value="{{ $radni_nalog_rezervni_deo->roba_id }}">
		<div class="custom-select-arrow"> 
			<input type="text" id="JSRezervniDeoNaziv" value="{{ RMA::find($radni_nalog_rezervni_deo->roba_id,'naziv_web') }}">
		</div>
		<div id="JSSearchContent" class="rma-custom-select"></div>
	</div>
	<div class="medium-6 columns">
		<label for="">Kolicina</label>
		<input type="text" id="JSRDKolicina" value="{{ $radni_nalog_rezervni_deo->kolicina }}">
	</div>
</div>
@if(RmaOptions::gnrl_options(3043) == 1)
<div class="row">
	<div class="medium-6 columns">
		<label for="">Cena</label>
		<input type="text" id="JSRezervniDeoCena" value="{{ $radni_nalog_rezervni_deo->cena }}">
	</div>
	<div class="medium-6 columns">
		<label for="">Ukupno</label>
		<input type="text" id="JSRezervniDeoUkupno" value="{{ $radni_nalog_rezervni_deo->iznos }}">
	</div>
</div>
@endif

<div class="btn-container center"> <br>
	<button id="JSRNRezervniDeoSubmit" class="save-it-btn btn btn-primary" data-radni_nalog_rezervni_deo_id="{{$radni_nalog_rezervni_deo->radni_nalog_rezervni_deo_id}}" data-radni_nalog_id="{{$radni_nalog->radni_nalog_id}}" >Sačuvaj</button>
</div>  