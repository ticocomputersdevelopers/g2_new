<div id="main-content" class="article-edit">
	@if(Session::has('message_success'))
	<script>
		alertify.success('{{ Session::get('message_success') }}');
	</script>
	@endif

	<?php
	$old = count(Input::old()) ? 1 : 0;
	// echo '<pre>';
	// var_dump(Input::old()); die();
	?>
	
	@if($roba_id) 
	@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
	@include('admin/partials/product-tabs')
	@endif 
	@endif

	<div class="row">
		@if(Session::has('message'))
		<div class="columns erase-div"> 
			<div class="text-red">{{ AdminLanguage::transAdmin('Artikal ima istoriju') }}!</div> 
			<a class="btn btn-danger" href="{{ AdminOptions::base_url() }}admin/product-delete/{{ $roba_id }}/1">{{ AdminLanguage::transAdmin('Ipak obriši') }}!</a>
		</div>
		@endif

		@if(Session::has('limit_message'))
		<div class="columns erase-div"> 
			<div class="text-red">{{ AdminLanguage::transAdmin('Unošenje novih artikala nije dozvoljeno') }}!</div>
		</div>
		@endif

		<form method="POST" action="{{AdminOptions::base_url()}}admin/product-edit" id="JSArticleForm">
			<input type="hidden" name="roba_id" value="{{ $roba_id ? $roba_id : 0 }}">
			<input type="hidden" name="clone_id" value="{{ $clone_id ? $clone_id : 0 }}">


			<div class="columns medium-8 cut-padding">
				<div class="row flat-box padding-v-8">
					
					<div class="columns medium-3 small-12">
						<div class="row">
							<div class="columns small-6"> 
								<label>{{ AdminLanguage::transAdmin('Vrsta') }}</label>

								@if($old)
								@if(Input::old('flag_usluga'))
								<div> 
									<input type="radio" name="flag_usluga" value="0" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label for="">{{ AdminLanguage::transAdmin('Roba') }}</label>
								</div>
								<div>	
									<input type="radio" name="flag_usluga" value="1" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label for="">{{ AdminLanguage::transAdmin('Usluga') }}</label>
								</div>
								@else
								<div>	
									<input type="radio" name="flag_usluga" value="0" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label for="">{{ AdminLanguage::transAdmin('Roba') }}</label>
								</div>
								<div>
									<input type="radio" name="flag_usluga" value="1" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label for="">{{ AdminLanguage::transAdmin('Usluga') }}</label>		
								</div>			
								@endif
								@else
								@if($flag_usluga)
								<div>
									<input type="radio" name="flag_usluga" value="0" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label for="">{{ AdminLanguage::transAdmin('Roba') }}</label>
								</div>
								<div>
									<input type="radio" name="flag_usluga" value="1" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label for="">{{ AdminLanguage::transAdmin('Usluga') }}</label>
								</div>
								@else
								<div>
									<input type="radio" name="flag_usluga" value="0" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label for="">{{ AdminLanguage::transAdmin('Roba') }}</label>
								</div>
								<div>
									<input type="radio" name="flag_usluga" value="1" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label for="">{{ AdminLanguage::transAdmin('Usluga') }}</label>		
								</div>			
								@endif
								@endif
							</div>

							<div class="columns small-6">
								<label>{{ AdminLanguage::transAdmin('SKU') }}</label>
								<input type="text" name="sku" value="{{ htmlentities($old ? Input::old('sku') : $sku) }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('sku') }}</div>
							</div>

						</div>
					</div> 

					<div class="columns medium-9">
						<div class="row"> 
							<div class="columns medium-8"> 
								<label>{{ AdminLanguage::transAdmin('Vrsta karakteristika') }}</label>
								@if($old)
								@if(Input::old('web_flag_karakteristike')==0)
								<input type="radio" name="web_flag_karakteristike" value="0" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('HTML') }}</label>
								<input type="radio" name="web_flag_karakteristike" value="1" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Generisane') }}</label>
								@if(Admin_model::check_admin(array('WEB_IMPORT')) && Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
								<input type="radio" name="web_flag_karakteristike" value="2" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Od dobavljača') }}</label>	
								@endif
								@elseif(Input::old('web_flag_karakteristike')==1)
								<input type="radio" name="web_flag_karakteristike" value="0" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('HTML') }}</label>
								<input type="radio" name="web_flag_karakteristike" value="1" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Generisane') }}</label>

								@if(Admin_model::check_admin(array('WEB_IMPORT')) && Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
								<input type="radio" name="web_flag_karakteristike" value="2" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Od dobavljača') }}</label>
								@endif

								@else
								<input type="radio" name="web_flag_karakteristike" value="0" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('HTML') }}</label>
								<input type="radio" name="web_flag_karakteristike" value="1" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Generisane') }}</label>
								@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
								<input type="radio" name="web_flag_karakteristike" value="2" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Od dobavljača') }}</label>
								@endif
								@endif
								@else
								@if($web_flag_karakteristike==0)
								<input type="radio" name="web_flag_karakteristike" value="0" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('HTML') }}</label>
								<input type="radio" name="web_flag_karakteristike" value="1" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Generisane') }}</label>
								@if(Admin_model::check_admin(array('WEB_IMPORT')) && Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
								<input type="radio" name="web_flag_karakteristike" value="2" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Od dobavljača') }}</label>
								@endif
								@elseif($web_flag_karakteristike==1)
								<input type="radio" name="web_flag_karakteristike" value="0" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('HTML') }}</label>
								<input type="radio" name="web_flag_karakteristike" value="1" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Generisane') }}</label>
								@if(Admin_model::check_admin(array('WEB_IMPORT')) && Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
								<input type="radio" name="web_flag_karakteristike" value="2" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Od dobavljača') }}</label>
								@endif
								@else
								<input type="radio" name="web_flag_karakteristike" value="0" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('HTML') }}</label>
								<input type="radio" name="web_flag_karakteristike" value="1" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Generisane') }}</label>
								@if(Admin_model::check_admin(array('WEB_IMPORT')) && Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
								<input type="radio" name="web_flag_karakteristike" value="2" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}><label>{{ AdminLanguage::transAdmin('Od dobavljača') }}</label>
								@endif
								@endif
								@endif 
							</div>

							<div class="columns medium-2 small-6"> 
								<label>{{ AdminLanguage::transAdmin('Redni broj') }}</label>
								<input type="text" name="rbr" value="{{ htmlentities($old ? Input::old('rbr') : $rbr) }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('rbr') }}</div> 
							</div>

							<div class="columns medium-2 small-6"> 
								<label>{{ AdminLanguage::transAdmin('Osobine') }}</label>
								<select name="osobine" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									@if($old)
									@if(Input::old('osobine'))
									<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
									@else
									<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
									@endif
									@else
									@if($osobine)
									<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
									@else
									<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
									<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
									@endif
									@endif
								</select> 
							</div>
						</div>
					</div>

					<div class="columns medium-12">
						<div class="naziv-web-big">
							<label>{{ AdminLanguage::transAdmin('Naziv na web-u') }}</label>
							<input type="text" name="naziv_web" value="{{ $old ? Input::old('naziv_web') : $naziv_web }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('naziv_web') }}</div>
						</div>
					</div>
				</div>

				<div class="flat-box padding-v-8">
					<div class="row"> 
						<div class="columns medium-6 small-6"> 
							<label>{{ AdminLanguage::transAdmin('Grupa') }}</label>
							<div class="custom-select-arrow"> 
								<input type="text" name="grupa_pr_grupa" value="{{Input::old('grupa_pr_grupa') ? Input::old('grupa_pr_grupa') : $grupa_pr_grupa }}" autocomplete="off" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>
							<div class="short-group-container"> 
								{{ AdminSupport::listGroups() }}
							</div>
							<div class="error">{{ $errors->first('grupa_pr_grupa') }}</div> 
						</div>

						<div class="columns medium-6 small-6"> 
							<label>{{ AdminLanguage::transAdmin('Proizvođač') }}</label>
							<div class="custom-select-arrow"> 
								<input type="text" name="proizvodjac" value="{{Input::old('proizvodjac') ? Input::old('proizvodjac') : $proizvodjac }}" autocomplete="off" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>
							<div class="short-group-container">
								<ul id="JSListaProizvodjaca" hidden="hidden">
									@foreach(AdminSupport::getProizvodjaci() as $row)
									<li class="JSListaProizvodjac" data-proizvodjac="{{ $row->naziv }}">{{ $row->naziv }}</li>
									@endforeach
								</ul>
							</div>
							<div class="error">{{ $errors->first('proizvodjac') }}</div> 
						</div>
					</div>

					<div class="row"> 
						<div class="columns medium-6"> 
							<label>{{ AdminLanguage::transAdmin('Dodatne grupe') }}</label>
							<select id="roba_grupe" name="roba_grupe[]" multiple="multiple" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@if($old)
								{{ AdminSupport::select2Groups(Input::old('roba_grupe') ? Input::old('roba_grupe') : array()) }}
								@else
								{{ AdminSupport::select2Groups($roba_grupe) }}
								@endif
							</select> 
						</div>

						<div class="columns medium-6"> 
							<label>{{ AdminLanguage::transAdmin('Poreska grupa') }}</label>
							<div class="row"> 
								<div class="columns medium-4"> 
									<select name="tarifna_grupa_id" id="tarifna_grupa_id" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
										@foreach(AdminSupport::getPoreskeGrupe() as $row) 
										@if($old)
										@if(Input::old('tarifna_grupa_id') == $row->tarifna_grupa_id))
										<option value="{{ $row->tarifna_grupa_id }}" selected>{{ $row->naziv }}</option>
										@else
										<option value="{{ $row->tarifna_grupa_id }}">{{ $row->naziv }}</option>
										@endif
										@else
										@if($tarifna_grupa_id === $row->tarifna_grupa_id)
										<option value="{{ $row->tarifna_grupa_id }}" selected>{{ $row->naziv }}</option>
										@else
										<option value="{{ $row->tarifna_grupa_id }}">{{ $row->naziv }}</option>
										@endif
										@endif
										@endforeach
									</select>
								</div>
								<div class="columns medium-8">  
									<label class="no-margin"> 
										<input type="checkbox" id="JSIWebCena"  {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
										{{ AdminLanguage::transAdmin('Promeni i web cenu') }}
									</label>
								</div> 
							</div>
						</div>
					</div>

					<div class="row"> 
						<div class="columns medium-3 small-6"> 
							<label>{{ AdminLanguage::transAdmin('Jedinica mere') }}</label>
							<div class="custom-select-arrow"> 
								<input type="text" name="jedinica_mere" value="{{Input::old('jedinica_mere') ? Input::old('jedinica_mere') : $jedinica_mere }}" autocomplete="off" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>
							<div class="short-group-container">
								<ul id="JSListaJedinicaMera" hidden="hidden">
									@foreach(AdminSupport::getJediniceMere() as $row)
									<li class="JSListaJedinicaMere" data-jedinica_mere="{{ $row->naziv }}">{{ $row->naziv }}</li>
									@endforeach
								</ul>
							</div>
							<div class="error">{{ $errors->first('jedinica_mere') }}</div> 
						</div>

						<div class="columns medium-3 small-6"> 
							<label>{{ AdminLanguage::transAdmin('Težina (gr.)') }}</label>
							<input type="text" name="tezinski_faktor" value="{{ htmlentities($old ? Input::old('tezinski_faktor') : $tezinski_faktor) }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('tezinski_faktor') }}</div> 
						</div>

						<div class="columns medium-2 small-6"> 
							<label>{{ AdminLanguage::transAdmin('Pakovanje') }} </label>
							<select name="flag_ambalaza"  {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@if($old)
								@if(Input::old('flag_ambalaza'))
								<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
								@else
								<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
								@endif
								@else
								@if($flag_ambalaza)
								<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
								@else
								<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
								@endif
								@endif
							</select> 
						</div>

						<div class="columns medium-4 small-6">
							<div class="{{ $errors->first('ambalaza') ? 'error' : '' }}">
								<label>{{ AdminLanguage::transAdmin('Broj artikala u pakovanju') }} </label>
								<input type="text" name="ambalaza" value="{{ htmlentities($old ? Input::old('ambalaza') : $ambalaza) }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('ambalaza') }}</div>
							</div>
						</div>
					</div> 
				</div>

				<div class="row flat-box padding-v-8">
					<div class="columns medium-6 small-6"> 
						<label>{{ AdminLanguage::transAdmin('Model') }}</label>
						<input type="text" name="model" value="{{ htmlentities($old ? Input::old('model') : $model) }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('model') }}</div> 
					</div>

					<div class="columns medium-3 small-6"> 
						<label>{{ AdminLanguage::transAdmin('Garancija (u mesecima)') }}</label>
						<input type="text" name="garancija" value="{{ htmlentities($old ? Input::old('garancija') : $garancija) }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('garancija') }}</div> 
					</div>

					<div class="columns medium-3 small-6"> 
						<label>{{ AdminLanguage::transAdmin('Produžena garancija') }}</label>
						<div class="columns medium-12 no-padd">
							<select name="produzena_garancija" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@if($old)
								@if(Input::old('produzena_garancija'))
								<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
								@else
								<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
								@endif
								@else
								@if($produzena_garancija)
								<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
								@else
								<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
								@endif
								@endif
							</select>
						</div> 
					</div>

					<div class="columns medium-6 small-6"> 
						<label>{{ AdminLanguage::transAdmin('Barkod') }}</label>
						<input type="text" name="barkod" value="{{ $old ? Input::old('barkod') : $barkod }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('barkod') }}</div> 
					</div>

					<div class="columns medium-6 small-6">	 
						<label>{{ AdminLanguage::transAdmin('Stanje artikla') }}</label>
						<div class="custom-select-arrow"> 
							<input type="text" name="roba_flag_cene" value="{{Input::old('roba_flag_cene') ? Input::old('roba_flag_cene') : $roba_flag_cene }}" autocomplete="off" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						<div class="short-group-container">
							<ul id="JSListaFlagCene" hidden="hidden">
								@foreach(AdminSupport::getFlagCene() as $row)
								<li class="JSListaFlagCena" data-roba_flag_cene="{{ $row->naziv }}">{{ $row->naziv }}</li>
								@endforeach
							</ul>
						</div>
						<div class="error">{{ $errors->first('roba_flag_cene') }}</div> 
					</div>

					<div class="columns medium-6 small-6"> 
						<label>{{ AdminLanguage::transAdmin('CAS Number') }}</label>
						<input type="text" name="cas_number" value="{{ $old ? Input::old('cas_number') : $cas_number }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('cas_number') }}</div> 
					</div>

					@if(Options::pitchprint_aktiv() == 1)  
					<div class="columns medium-3"> 
						<label>{{ AdminLanguage::transAdmin('PitchPrint-Design ID') }}</label>
						<input type="text" name="design_id" value="{{ $old ? Input::old('design_id') : $design_id }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('design_id') }}</div> 
					</div>
					@endif
				</div>
				
				<div class="row flat-box padding-v-8">
					<div class="columns large-6 medium-6 small-6">	 
						<label>{{ AdminLanguage::transAdmin('Magacin') }}</label>
						<select name="orgj_id" id="orgj_id" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@foreach(DB::table('orgj')->where('b2c',1)->orderBy('orgj_id','asc')->get() as $row)
							@if($old)
							@if(Input::old('orgj_id') == $row->orgj_id)
							<option value="{{ $row->orgj_id }}" selected>{{ $row->naziv }} ({{ $row->orgj_id }} {{ DB::table('imenik_magacin')->where('orgj_id',$row->orgj_id)->pluck('izabrani') == 1 ? 'def.' : '' }})</option>
							@else
							<option value="{{ $row->orgj_id }}">{{ $row->naziv }} ({{ $row->orgj_id }} {{ DB::table('imenik_magacin')->where('orgj_id',$row->orgj_id)->pluck('izabrani') == 1 ? 'def.' : '' }})</option>
							@endif
							@else
							@if($orgj_id == $row->orgj_id)
							<option value="{{ $row->orgj_id }}" selected>{{ $row->naziv }} ({{ $row->orgj_id }} {{ DB::table('imenik_magacin')->where('orgj_id',$row->orgj_id)->pluck('izabrani') == 1 ? 'def.' : '' }})</option>
							@else
							<option value="{{ $row->orgj_id }}">{{ $row->naziv }} ({{ $row->orgj_id }} {{ DB::table('imenik_magacin')->where('orgj_id',$row->orgj_id)->pluck('izabrani') == 1 ? 'def.' : '' }})</option>
							@endif
							@endif
							@endforeach
						</select> 
					</div>

					<div class="columns large-6 medium-6 small-6">	 
						<label>{{ AdminLanguage::transAdmin('Količina') }}</label>
						<input type="text" name="kolicina" value="{{ $old ? Input::old('kolicina') : $kolicina }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('kolicina') }}</div> 
					</div>
				</div>

				<div class="row flat-box padding-v-8">
					<div class="columns medium-12">
						<div class="btn-container center">
							@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
							@if(isset($clone_id))
							<input type="checkbox" name="slike_clone" checked {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>{{ AdminLanguage::transAdmin('Kloniraj slike') }}
							@endif
							<button type="submit" id="JSArticleSubmit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button> 

							@if($roba_id != 0)
							<a class="btn btn-danger" href="{{ AdminOptions::base_url() }}admin/product-delete/{{ $roba_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</a>
							@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
							<a class="btn-secondary btn" href="{{AdminOptions::base_url()}}admin/product/0/{{ $roba_id }}">{{ AdminLanguage::transAdmin('Kloniraj artikal') }}</a>
							@endif
							@endif

							<a href="#" id="JSArtikalRefresh" class="btn btn-primary">{{ AdminLanguage::transAdmin('Poništi izmene') }}</a>
							@endif
							@if(AdminOptions::gnrl_options(3029))
							<a href="{{AdminOptions::base_url()}}admin/product-short/{{$roba_id}}" class="btn btn-primary">{{ AdminLanguage::transAdmin('Brza izmena') }}</a>
							@endif
						</div>
					</div>
				</div> 
			</div> 

			
			<div class="columns medium-4 cut-padding">
				@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
				<div class="row flat-box padding-v-8">
					<div class="columns medium-4 small-6">  
						<label>{{ AdminLanguage::transAdmin('Šifra kod dobavljača') }}</label>
						<input type="text" name="sifra_d" value="{{ $old ? Input::old('sifra_d') : $sifra_d }}"  {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('sifra_d') }}</div> 
					</div>
					<div class="columns medium-4 small-6">  
						<label>{{ AdminLanguage::transAdmin('Šifra IS') }}</label>
						<input type="text" name="sifra_is" value="{{ $old ? Input::old('sifra_is') : $sifra_is }}"  {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('sifra_is') }}</div> 
					</div>
					<div class="columns medium-4 small-6"> 
						<label>{{ AdminLanguage::transAdmin('Dobavljač') }}</label>
						<select name="dobavljac_id" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
							<option value=""></option>
							@foreach(AdminSupport::getDobavljaci() as $row)
							@if($old)
							@if(Input::old('dobavljac_id') != '' && Input::old('dobavljac_id') == $row->partner_id)
							<option value="{{ $row->partner_id }}" selected>{{ $row->naziv }}</option>
							@else
							<option value="{{ $row->partner_id }}">{{ $row->naziv }}</option>
							@endif
							@else
							@if($dobavljac_id === $row->partner_id)
							<option value="{{ $row->partner_id }}" selected>{{ $row->naziv }}</option>
							@else
							<option value="{{ $row->partner_id }}">{{ $row->naziv }}</option>
							@endif
							@endif
							@endforeach
						</select> 
					</div>
				</div>
				@endif
				

				<div class="row flat-box padding-v-8">
					<div class="columns medium-4 small-6"> 
						<label>{{ AdminLanguage::transAdmin('Na Web-u') }} </label>
						<select name="flag_prikazi_u_cenovniku"  {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@if($old)
							@if(Input::old('flag_prikazi_u_cenovniku'))
							<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
							@else
							<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
							@endif
							@else
							@if($flag_prikazi_u_cenovniku)
							<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
							@else
							<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
							@endif
							@endif
						</select>  
					</div>

					<div class="columns medium-4 small-6"> 
						<label>{{ AdminLanguage::transAdmin('Aktivan') }}</label>
						<select name="flag_aktivan" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@if($old)
							@if(Input::old('flag_aktivan'))
							<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
							@else
							<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
							@endif
							@else
							@if($flag_aktivan)
							<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
							@else
							<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
							@endif
							@endif
						</select> 
					</div>

					<div class="columns medium-4 small-6"> 
						<label>{{ AdminLanguage::transAdmin('Zaključan') }}</label>
						<select name="flag_zakljucan" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@if($old)
							@if(Input::old('flag_zakljucan')=="true")
							<option value="true" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="false" >{{ AdminLanguage::transAdmin('NE') }}</option>
							@else
							<option value="true" >{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="false" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
							@endif
							@else
							@if($flag_zakljucan)
							<option value="true" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="false" >{{ AdminLanguage::transAdmin('NE') }}</option>
							@else
							<option value="true" >{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="false" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
							@endif
							@endif
						</select> 
					</div>

					<div class="columns medium-12 small-6"> 
						<label>{{ AdminLanguage::transAdmin('Tip artikla') }}</label>
						<div class="custom-select-arrow"> 
							<input type="text" name="tip_artikla" value="{{Input::old('tip_artikla') ? Input::old('tip_artikla') : $tip_artikla }}" autocomplete="off" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						<div class="short-group-container"> 
							<ul id="JSListaTipaArtikla" hidden="hidden">
								<li class="JSListaTipArtikla" data-tip_artikla="">{{ AdminLanguage::transAdmin('Bez tipa') }}</li>
								@foreach(AdminSupport::getTipovi() as $row)
								<li class="JSListaTipArtikla" data-tip_artikla="{{ $row->naziv }}">{{ $row->naziv }}</li>
								@endforeach
							</ul>
						</div>
						<div class="error">{{ $errors->first('tip_artikla') }}</div> 
					</div> 
				</div>


				<div class="flat-box padding-v-8">
					<div class="row">
						<div class="columns medium-5"> 
							<label>{{ AdminLanguage::transAdmin('Nabavna cena') }}</label>
							<input type="text" name="racunska_cena_nc" id="racunska_cena_nc" value="{{ $old ? Input::old('racunska_cena_nc') : $racunska_cena_nc }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('racunska_cena_nc') }}</div> 
						</div>
					</div> 

					<div class="row">	
						<div class="columns medium-5 small-5"> 
							<label>{{ AdminLanguage::transAdmin('End cena') }}</label>
							<input type="text" name="racunska_cena_end" value="{{ $old ? Input::old('racunska_cena_end') : $racunska_cena_end }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('racunska_cena_end') }}</div> 
						</div>

						<div class="columns medium-5 small-5"> 
							<label>{{ AdminLanguage::transAdmin('End marža') }} (%)</label>
							<input type="text" name="end_marza" id="end_marza" value="{{ $old ? Input::old('end_marza') : $end_marza }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('end_marza') }}</div> 
						</div>

						<div class="columns medium-2 small-2 no-padd"> 
							<label>{{ AdminLanguage::transAdmin('Primeni') }}</label>
							<input type="checkbox" id="end_marza_check" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}> 
						</div>
					</div> 

					<div class="row">	
						<div class="columns medium-5 small-5"> 
							<label>{{ AdminLanguage::transAdmin('Maloprodajna cena') }}</label>
							<input type="text" name="mpcena" value="{{ $old ? Input::old('mpcena') : $mpcena }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('mpcena') }}</div> 
						</div>

						<div class="columns medium-5 small-5"> 
							<label>{{ AdminLanguage::transAdmin('MP marža') }} (%)</label>
							<input type="text" name="mp_marza" id="mp_marza" value="{{ $old ? Input::old('mp_marza') : $mp_marza }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('mp_marza') }}</div> 
						</div>

						<div class="columns medium-2 small-2 no-padd"> 
							<label>{{ AdminLanguage::transAdmin('Primeni') }}</label>
							<input type="checkbox" id="mp_marza_check" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}> 
						</div>
					</div>

					<div class="row"> 
						<div class="columns medium-5 small-5"> 
							<label>{{ AdminLanguage::transAdmin('Web cena') }}</label>
							<input type="text" name="web_cena" class="JSCenaChange" value="{{ $old ? Input::old('web_cena') : $web_cena }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('web_cena') }}</div> 
						</div>

						<div class="columns medium-5 small-5"> 
							<label>{{ AdminLanguage::transAdmin('Web marža') }} (%)</label>
							<input type="text" name="web_marza" id="web_marza" value="{{ $old ? Input::old('web_marza') : $web_marza }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('web_marza') }}</div> 
						</div>

						<div class="columns medium-2 small-2 no-padd"> 
							<label>{{ AdminLanguage::transAdmin('Primeni') }}</label>
							<input type="checkbox" id="web_marza_check" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}> 
						</div>
					</div>	
				</div>
			</div>  	
		</form>
	</div> <!-- end of .row -->	
</div>


