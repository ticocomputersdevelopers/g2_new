<div class="m-tabs clearfix">
	@if(Admin_model::check_admin(array('KUPCI_I_PARTNERI')) && Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')))
	<div class="m-tabs__tab{{ $strana=='kupci' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/kupci_partneri/kupci">{{ AdminLanguage::transAdmin('Kupci') }}</a></div>
	@endif
	@if(Admin_model::check_admin(array('KUPCI_I_PARTNERI')) && Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')))
	<div class="m-tabs__tab{{ $strana=='partneri' ? ' m-tabs__tab--active' : '' }}"><a href="{{AdminOptions::base_url()}}admin/kupci_partneri/partneri">{{ AdminLanguage::transAdmin('Partneri') }}</a></div>
	@endif
</div>