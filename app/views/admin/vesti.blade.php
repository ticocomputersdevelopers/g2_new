<div id="main-content">
 
	<div> 
		<h1 class="big inline-block">{{ AdminLanguage::transAdmin('Vesti') }}</h1>
		
		&nbsp;

		@if(Admin_model::check_admin(array('VESTI_AZURIRANJE')))
		<a href="{{ AdminOptions::base_url() }}admin/vest/0" class="btn btn-create btn-sm">{{ AdminLanguage::transAdmin('Dodaj novu') }}</a>
		@endif
		<a href="#" class="video-manual" data-reveal-id="news-manual">{{ AdminLanguage::transAdmin('Uputstvo') }} <i class="fa fa-film"></i></a>
		
		<!-- ====================== -->
		<div id="news-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<div class="video-manual-container"> 
				<p><span class="video-manual-title">{{ AdminLanguage::transAdmin('Vesti') }}</span></p>
				<iframe src="https://player.vimeo.com/video/271254902" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			</div>
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		</div>
		<!-- ====================== -->
		
		@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
		@endif
	</div> 

	<div class="flat-box">
		<div class="news-filter">
			<a href="{{ AdminOptions::base_url() }}admin/vesti/">{{ AdminLanguage::transAdmin('Sve') }} ({{ $count_sve }})</a>
			<a href="{{ AdminOptions::base_url() }}admin/vesti/1">Aktivne ({{ $count_aktivne }})</a>
			<a href="{{ AdminOptions::base_url() }}admin/vesti/0">Neaktivne ({{ $count_neaktivne }})</a>
		</div>

		<div class="table-scroll"> 
			<table class="news-table">
				<tr>
					<th>&nbsp;{{ AdminLanguage::transAdmin('Naslov') }}</th>
					<th class="text-center">{{ AdminLanguage::transAdmin('Status') }}</th>
					<th>{{ AdminLanguage::transAdmin('Datum') }}</th>
				</tr>
				@foreach($vesti as $row)
				<tr>
					<td>
						<a href="{{ AdminOptions::base_url() }}admin/vest/{{ $row->web_vest_b2c_id }}" class="title">{{ AdminVesti::findTitle($row->web_vest_b2c_id) }}</a>
						<div class="news-action">
							<a href="{{ AdminVesti::news_link($row->web_vest_b2c_id) }}" target="_blank">
								<i class="fa fa-eye" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Vidi') }}
							</a>
							@if(Admin_model::check_admin(array('KUPCI_I_PARTNERI_AZURIRANJE')))
							<a href="{{ AdminOptions::base_url() }}admin/vest/{{ $row->web_vest_b2c_id }}">
								<i class="fa fa-pencil" aria-hidden="true"></i>{{ AdminLanguage::transAdmin('Izmeni') }}
							</a>
							<a class="text-alert JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/vesti/{{ $row->web_vest_b2c_id }}/delete"><i class="fa fa-trash" aria-hidden="true"></i>{{ AdminLanguage::transAdmin('Obriši') }}</a>
							@endif

						</div>
					</td>
					@if($row->aktuelno == 1)
					<td class="text-center">
						<span class="active">{{ AdminLanguage::transAdmin('Aktivna') }}</span>
					</td>
					@else
					<td class="text-center">
						<span class="inactive">{{ AdminLanguage::transAdmin('Neaktivna') }}</span>
					</td>
					@endif
					<td>{{ $row->datum }}</td>
				</tr>

				@endforeach
				<div> {{ $vesti->links() }}</div>
			</table>
		</div>
	</div>  
</div>