<div id="main-content" class="">
	@include('admin/partials/product-tabs')
	@include('admin/partials/karak-tabs')

	<input id="roba_id_obj" type="hidden" value="{{ $roba_id }}">

	<div class="row"> 
		<div class="columns medium-8 medium-centered">
			<div class="flat-box">
				<h2 class="title-med">{{ AdminLanguage::transAdmin('Karakteristike') }}</h2>
				@if(count($grupe_karakteristika))
				<div class="table-scroll"> 
					<table role="grid" class="karakteristike-tabela">
						<tbody @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) id="sortable_grupa" @endif>
							@foreach($grupe_karakteristika as $karakteristika_grupa)
							<tr class="JSGrupaNazivData" grupa="{{$karakteristika_grupa}}">
								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<i class="fa fa-arrows-v sortableKar tooltipz" aria-label="{{ AdminLanguage::transAdmin('Prevucite da bi sortirali') }}"></i>
									@endif
								</td>

								<td><input type="text" class="JSGrupaNaziv min-width-200" value="{{ $karakteristika_grupa }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>

								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<button class="JSGrupa name-ul__li__save button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}" data-old="{{ $karakteristika_grupa }}"><i class="fa fa-floppy-o"></i></button>
									@endif

									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<button class="JSGrupaDelete name-ul__li__remove button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-old="{{ $karakteristika_grupa }}">&times</button>
									@endif
								</td>

								<td>
									<table>
										<tbody @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="JSSortableNaziv" @endif grupa="{{$karakteristika_grupa}}">
											@foreach(AdminSupport::getGrupeNazivKarak($roba_id,$karakteristika_grupa) as $row2)
											<tr class="JSGrupaKarakData " grupa="$karakteristika_grupa" naziv="{{$row2->karakteristika_naziv}}" vrednost="{{ $row2->karakteristika_vrednost }}"> 
												<td>
													@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
													<i class="fa fa-arrows-v sortableKar tooltipz" aria-label="{{ AdminLanguage::transAdmin('Prevucite da bi sortirali') }}"></i>
													@endif
												</td>

												<td><input type="text" class="JSKarakNaziv min-width-200" value="{{ $row2->karakteristika_naziv }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
												<td><input type="text" class="JSKarakVrednost min-width-200" value="{{ $row2->karakteristika_vrednost }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>

												<td>
													@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
													<button class="JSKarak name-ul__li__save button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}" data-id="{{ $row2->dobavljac_cenovnik_karakteristike_id }}"><i class="fa fa-floppy-o"></i></button>
													@endif
												</td>

												<td>
													@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
													<button class="JSKarakDelete name-ul__li__remove button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-id="{{ $row2->dobavljac_cenovnik_karakteristike_id }}">&times</button>
													@endif
												</td> 
											</tr>
											@endforeach
										</tbody>
										<tr>
											@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
											<td><i class="fa fa-arrows-v sortableKar tooltipz" aria-label="{{ AdminLanguage::transAdmin('Prevucite da bi sortirali') }}"></i></td>
											<td><input type="text" class="JSKarakNaziv" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
											<td><input type="text" class="JSKarakVrednost" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
											<td>
												<button class="JSKarak name-ul__li__save button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}" data-id="new"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
											</td>
											<td></td>
											@endif
										</tr>
									</table>
								</td>
							</tr>
							@endforeach

							<tr>
								@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
								<td><i class="fa fa-arrows-v sortableKar tooltipz" aria-label="{{ AdminLanguage::transAdmin('Prevucite da bi sortirali') }}"></i></td>
								<td></td>
								<td><input type="text" class="JSGrupaNaziv" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
								<td>
									<button class="JSGrupa name-ul__li__save button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}" data-old="new"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
								</td>
								<td></td>
								<td></td> 
								@endif
							</tr>
						</tbody>
					</table>
				</div>
				@else
				<div class="table-scroll">
					<table role="grid" class="karakteristike-tabela">
						<tbody id="sortable_naziv">
							@foreach(AdminSupport::getDobavljKarak($roba_id) as $row)
							<tr grupa="$karakteristika_grupa" naziv="{{$row->karakteristika_naziv}}" vrednost="{{ $row->karakteristika_vrednost }}">
								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<i class="fa fa-arrows-v sortableKar tooltipz" aria-label="{{ AdminLanguage::transAdmin('Prevucite da bi sortirali') }}"></i>
									@endif
								</td>

								<td><input type="text" class="JSKarakNaziv" value="{{ $row->karakteristika_naziv }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
								<td><input type="text" class="JSKarakVrednost" value="{{ $row->karakteristika_vrednost }}" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>

								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<button class="JSKarak name-ul__li__save button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}" data-id="{{ $row->dobavljac_cenovnik_karakteristike_id }}"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
									@endif
								</td>

								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<button class="JSKarakDelete name-ul__li__remove button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-id="{{ $row->dobavljac_cenovnik_karakteristike_id }}">&times</button>
									@endif
								</td>
							</tr>
							@endforeach
							<tr>
								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<i class="fa fa-arrows-v sortableKar tooltipz" aria-label="{{ AdminLanguage::transAdmin('Prevucite da bi sortirali') }}"></i>
									@endif
								</td>
								<td><input type="text" class="JSKarakNaziv" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
								<td><input type="text" class="JSKarakVrednost" {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<button class="JSKarak name-ul__li__save button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}" data-id="new"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
									@endif
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				@endif
			</div>	
		</div>
	</div>
</div>