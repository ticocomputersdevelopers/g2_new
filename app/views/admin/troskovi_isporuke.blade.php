<section id="main-content">

	@include('admin/partials/tabs')
	<?php
	$messages = array();
	if($errors->first('tezina_gr')){ $messages[] = $errors->first('tezina_gr'); }
	if($errors->first('cena_do')){ $messages[] = $errors->first('cena_do'); }
	if($errors->first('cena')){ $messages[] = $errors->first('cena'); }
	?>
	@if(count($messages)>0)
	<script>
		alertify.error('{{ $messages[0] }}');
	</script>
	@elseif(Session::has('success'))
	<script>
		alertify.success('{{ AdminLanguage::transAdmin('Uspešno ste sačuvali podatak') }}');
	</script>
	@elseif(Session::has('success-delete'))
	<script>
		alertify.success('{{ AdminLanguage::transAdmin('Uspešno ste obrisali vrednost') }}.');
	</script>
	@endif
	
	@if(AdminOptions::web_options(150)==1 OR AdminOptions::web_options(133)==1)
	<div class="row">
		<section class="medium-5 medium-centered columns">
			<div class="flat-box">
				<table>
					<thead>
						<tr>
							@if(AdminOptions::web_options(133)==1)
							<th>{{ AdminLanguage::transAdmin('Težina') }}</th>
							@endif
							@if(AdminOptions::web_options(150)==1)
							<th>{{ AdminLanguage::transAdmin('Cena') }}</th>
							@endif
							<th>{{ AdminLanguage::transAdmin('Cena isporuke') }}</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($troskovi_isporuke as $row)
						<tr>
							<form method="POST" action="{{AdminOptions::base_url()}}admin/troskovi-isporuke-save">
								<input type="hidden" name="web_troskovi_isporuke_id" value="{{ $row->web_troskovi_isporuke_id }}">
							
								@if(AdminOptions::web_options(133)==1)
								<td><input type="text" name="tezina_gr" value="{{ $row->tezina_gr }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
								@else
								<input type="hidden" name="tezina_gr" value="0.00">
								@endif
							
								@if(AdminOptions::web_options(150)==1)
								<td><input type="text" name="cena_do" value="{{ $row->cena_do }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
								@else
								<input type="hidden" name="cena_do" value="0.00">
								@endif
							
								<td><input type="text" name="cena" value="{{ $row->cena }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}></td>
								@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
								<td><input class="btn btn-small btn-secondary" type="submit" value="Sačuvaj"></td>
								
								<th>@if($row->web_troskovi_isporuke_id!=0)
									<a class="tooltipz JSbtn-delete" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-link="{{AdminOptions::base_url()}}admin/troskovi-isporuke-delete/{{$row->web_troskovi_isporuke_id}}"><i class="fa fa-times" style="color:red;" aria-hidden="true"></i>&nbsp;</a>@endif</th>
									@endif
								</form>
							</tr>
							@endforeach					
						</tbody>
					</table> 
				</div>
			</section>
		</div>
		@endif 
	</section>



