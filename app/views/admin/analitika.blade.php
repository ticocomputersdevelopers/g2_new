<section id="main-content" class="b2cAnalics">

	<h1 class="h1-title">{{ AdminLanguage::transAdmin('Analitika') }}</h1>

	<!-- analytics -->
	<div class="row small-gutter">
		<div class="column medium-4">
			<div class="flat-box">
				<ul class="anly-ul">
					<li class="anly-ul__li">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="head" {{ (in_array('nove',$statusi) AND in_array('prihvacene',$statusi) AND in_array('realizovane',$statusi) AND in_array('stornirane',$statusi)) ? 'checked' : '' }}>
					</li>
					@if(isset($nove))
					<li class="anly-ul__li">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="nove" {{ in_array('nove',$statusi) ? 'checked' : '' }}>
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Nove porudžbine') }}</span>
						<span class="anly-ul__li__count">{{ $nove }} </span>						
					</li>
					@else
					<li class="anly-ul__li">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="nove" {{ in_array('nove',$statusi) ? 'checked' : '' }}>
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Nove porudžbine') }}</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getNew() }}</span>
					</li>
					@endif

					@if(isset($prihvaceno))
					<li class="anly-ul__li">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="prihvacene" {{ in_array('prihvacene',$statusi) ? 'checked' : '' }}>
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Prihvaćene porudžbine') }}</span>
						<span class="anly-ul__li__count">{{ $prihvaceno }} </span>	
					</li>
					@else
					<li class="anly-ul__li">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="prihvacene" {{ in_array('prihvacene',$statusi) ? 'checked' : '' }}>
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Prihvaćene porudžbine') }}</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getPrihvacene() }}</span>
					</li>
					@endif

					@if(isset($realizovano))
					<li class="anly-ul__li">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="realizovane" {{ in_array('realizovane',$statusi) ? 'checked' : '' }}>
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Realizovane porudžbine') }}</span>
						<span class="anly-ul__li__count">{{ $realizovano }} </span>						
					</li>
					@else
					<li class="anly-ul__li">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="realizovane" {{ in_array('realizovane',$statusi) ? 'checked' : '' }}>
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Realizovane porudžbine') }}</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getRealizovane() }}</span>
					</li>
					@endif

					@if(isset($stornirano))
					<li class="anly-ul__li">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="stornirane" {{ in_array('stornirane',$statusi) ? 'checked' : '' }}>
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Stornirane porudžbine') }}</span>
						<span class="anly-ul__li__count">{{ $stornirano }} </span>	
					</li>
					@else
					<li class="anly-ul__li">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="stornirane" {{ in_array('stornirane',$statusi) ? 'checked' : '' }}>
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Stornirane porudžbine') }}</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getStornirane() }}</span>
					</li>
					@endif

					@if(isset($ukupno))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupno porudžbina') }}</span>
						<span class="anly-ul__li__count">{{ $ukupno }} </span>	
					</li>
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupno porudžbina') }}</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::getUkupnoPorudzbina() }}</span>
					</li>
					@endif 
				</ul>

				<div class="padding-v-8">	 
					<canvas height=140 id="graph5"></canvas> 
				</div>
			</div>
		</div>

		<div class="columns medium-4">
			<div class="flat-box">
				<ul class="anly-ul">
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Jučerašnji prihod') }}</span>
						<span class="anly-ul__li__count">{{ number_format(AdminAnalitika::jucerasnjiPrihod($where_status), 2, ',', ' ')  }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Proteklih mesec dana') }}</span>
						<span class="anly-ul__li__count">{{ number_format(AdminAnalitika::mesecniPrihod($where_status), 2, ',', ' ')  }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupan prihod') }}</span>
						<span class="anly-ul__li__count">{{ 
						number_format(AdminAnalitika::ukupanPrihod($where_status), 2, ',', ' ')  }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupna Razlika u ceni') }}</span>
						<span class="anly-ul__li__count">{{ 
						number_format(AdminAnalitika::ukupanRUC($where_status), 2, ',', ' ')  }} din</span>
					</li> 
				</ul>
			</div>
			

			<div class="flat-box">
				<ul class="anly-ul">
					<li class="anly-ul__li"> 
						<div class="column medium-5">
							<label for="">{{ AdminLanguage::transAdmin('Datum Od') }}</label>
							<input id="datum_od_din" class="datum-val has-tooltip" name="datum_od_din" type="text" value="{{$od}}">
						</div>
						<div class="column medium-5">
							<label for="">{{ AdminLanguage::transAdmin('Datum Do') }}</label>
							<input id="datum_do_din" class="datum-val has-tooltip" name="datum_do_din" type="text" value="{{$do}}">
						</div>
						<div class="column medium-2 no-padd">
							<label for="">&nbsp;</label>
							<a class="btn btn-danger fr" href="/admin/analitika">{{ AdminLanguage::transAdmin('Poništi') }}</a>
						</div> 
					</li>

					@if(isset($prihod))
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Prihod u odabranom periodu') }}: </span>
						<span class="anly-ul__li__count">{{ number_format($prihod, 2, ',', ' ') }} din</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text tooltipz" aria-label="Razlika u ceni">{{ AdminLanguage::transAdmin('RUC') }}</span>
						<span class="anly-ul__li__count">{{number_format($razlika, 2, ',', ' ')  }} din</span>						
					</li>
					
					@else
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Prihod u odabranom periodu') }}: </span>
						<span class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Nema rezultata') }}</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text tooltipz" aria-label="Razlika u ceni">{{ AdminLanguage::transAdmin('RUC') }}</span>
						<span class="anly-ul__li__count">{{ AdminLanguage::transAdmin('Nema rezultata') }}</span>
					</li>
					@endif
				</ul>
			</div>
		</div>

		<div class="columns medium-4">
			<div class="flat-box">
				<ul class="anly-ul">
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupno artikala u prodavnici') }}</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::ukupnoArtikala() }}</span>
					</li>
					<li class="anly-ul__li">
						<span class="anly-ul__li__text">{{ AdminLanguage::transAdmin('Ukupno korisnika registrovano') }}</span>
						<span class="anly-ul__li__count">{{ AdminAnalitika::ukupnoKorisnika() }}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row small-gutter">
		<div class="column medium-6">
			<div class="flat-box">
				<canvas height=140 id="graph"></canvas>
			</div>  
		</div>
		<div class="column medium-6">
			<div class="flat-box">
				<canvas height=140 id="graph2"></canvas>
			</div>
		</div>
	</div>

	<div class="row small-gutter">
		<div class="column medium-6">
			<div class="flat-box">
				<canvas height=140 id="graph3"></canvas>
			</div>  
		</div>
		<div class="column medium-6">
			<div class="flat-box">
				<canvas height=140 id="graph4"></canvas>
			</div>
		</div>
	</div>

	<div class="flat-box"> 
		<div class="row">
			<div class="column medium-6">
				<div class="table-scroll"> 
					<table class="analitics-table">
						<thead>
							<th>&nbsp;{{ AdminLanguage::transAdmin('Naziv') }}</th>
							<th>{{ AdminLanguage::transAdmin('Broj prodatih') }}</th>
						</thead>
						<tbody>
							@foreach($artikli as $row)
							@if($row->count > 0)
							<tr>
								<td class="first-td"> {{ AdminCommon::analitika_title($row->roba_id) }} </td>
								<td> {{ $row->count + 0 }} </td> 
							</tr>
							@endif
							@endforeach
						</tbody>
					</table>
				</div>
			</div>

			<div class="column medium-6">
				<div class="table-scroll">  
					<table class="analitics-table">
						<tr>
							<th>&nbsp; {{ AdminLanguage::transAdmin('Naziv') }}</th>
							<th>{{ AdminLanguage::transAdmin('Broj pregleda') }}</th>
						</tr>
						@foreach(AdminCommon::mostPopularArticles() as $row)
						@if($row->pregledan_puta > 0)
						<tr>
							<td class="first-td"> {{ AdminCommon::analitika_title($row->roba_id) }} </td>
							<td> {{ $row->pregledan_puta }} </td>
						</tr>
						@endif
						@endforeach
					</table>
				</div>
			</div>

			<div class="columns medium-12">  
				<div class="table-scroll"> 

					<table class="analitics-table">
						<thead> 
							<tr>
								<th>&nbsp;{{ AdminLanguage::transAdmin('Naziv (Grupe / Artikla)') }}</th>
								<th>{{ AdminLanguage::transAdmin('Broj prodatih') }}</th>
								<th>{{ AdminLanguage::transAdmin('Ukupna cena') }}</th>
								<th>{{ AdminLanguage::transAdmin('RUC') }}</th>
							</tr>
						</thead>
						<tbody> 
							@foreach($analitikaGrupa as $grupa)
							<tr class="JSAnalitikaGrupa" data-id="{{ $grupa->grupa_pr_id }}" data-isopen="closed">
								<td class="first-td">
									<a href="#!">{{$grupa->grupa}}</a>
								</td>
								<td>
									{{round($grupa->sum)}}
								</td>
								<td>
									{{sprintf("%.2f",$grupa->ukupno)}}
								</td>
								<td>
									{{sprintf("%.2f",$grupa->razlika)}} ( {{round($grupa->nc_cena)}} )
								</td>
							</tr>
							<?php $suma=0; ?>
							@foreach(AdminAnalitika::prikazGrupa($grupa->grupa_pr_id) as $prikaz)
							@if(isset($prikaz->naziv))
							<tr class="JSAnalitikaArtikliGrupe sub-td" data-id="{{ $grupa->grupa_pr_id }}" hidden>
								<td class="colapsable-td">&nbsp;&nbsp; - {{$prikaz->naziv}} </td>
								<td> {{round($prikaz->sum)}} </td>
								<td> {{sprintf("%.2f",$prikaz->ukupno)}} </td>
								<td>
									@if($prikaz->racunska_cena_nc > 0)
									{{sprintf("%.2f",$prikaz->razlika)}}
									<?php $suma +=$prikaz->razlika ?>
									@endif
								</td>
							</tr>
							@endif
							@endforeach

							@endforeach
						</tbody>
					</table>
				</div>

				@if(false)
				<div class="column medium-6">
					<div class="table-scroll"> 
						<table class="analitics-table analitics-table-artical">
							<thead> 
								<tr>
									<th>&nbsp;{{ AdminLanguage::transAdmin('Naziv artikla') }}</th>
									<th>{{ AdminLanguage::transAdmin('Broj prodatih') }}</th>
									<th>{{ AdminLanguage::transAdmin('Ukupna cena') }}</th>
									<th>{{ AdminLanguage::transAdmin('Ruc') }}</th>
								</tr>
							</thead>
							<?php $suma=0; ?>
							@foreach(array() as $prikaz)
							@if(isset($prikaz->naziv))
							<tbody> 
								<tr>
									<td> {{$prikaz->naziv}} </td>
									<td> {{round($prikaz->sum)}} </td>
									<td> {{sprintf("%.2f",$prikaz->ukupno)}} </td>
									<td>
										@if($prikaz->racunska_cena_nc > 0)
										{{sprintf("%.2f",$prikaz->razlika)}}
										<?php $suma +=$prikaz->razlika ?>
										@endif
									</td>
								</tr>
								@endif
								@endforeach
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td><b> {{sprintf("%.2f",$suma)}} </b></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				@endif
			</div>

		</div>
	</div>
	
	<div class="flat-box"> 
		<div class="row">	 
			<div class="columns medium-12 no-padd"> 
				<div class="column medium-6">
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Najprodavanije grupe') }}</h3>
					<br>
					<canvas height=140 id="graph6"></canvas> 
				</div>
				<div class="column medium-6">
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Razlika u ceni (RUC)') }}</h3>
					<br>
					<canvas height=140 id="graph7"></canvas> 
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="flat-box"> 
			<h3 class="title-med">{{ AdminLanguage::transAdmin('Najposećenije grupe') }}</h3>

			<div class="column medium-6"> 
				<br>
				<canvas height=140 id="graph8"></canvas>

			</div>
			<div class="column medium-6">
				<table class="analitics-table">
					<thead> 
						<tr>
							<th>{{ AdminLanguage::transAdmin('Naziv grupe') }}</th>
							<th>{{ AdminLanguage::transAdmin('Broj poseta') }}</th>
						</tr>
					</thead>
					<tbody> 
						@foreach($analitikaGrupaPregled as $grupa)
						<tr>
							<td>
								@if($datum_od && $datum_do)
								{{$grupa->grupa1}}
								@else
								{{$grupa->grupa1}}
								@endif
							</td>
							<td>
								{{$grupa->sum}}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div> 
		</div>
	</div>


	<div class="flat-box"> 
		<div class="row" id="poll_position">
			<h3 class="title-med">{{ AdminLanguage::transAdmin('Ankete') }}</h3>

			<div class="column medium-4"> 
				<ul>
					@foreach(AdminAnkete::ankete(1) as $anketa)
					<li class="poll-analytics {{ ($anketa_id == $anketa->anketa_id) ? 'active' : '' }}"><a href="/admin/analitika?anketa_id={{$anketa->anketa_id}}#poll_position">{{ $anketa->naziv }}</a></li>
					@endforeach
				</ul>
				<hr>

				@if($anketa_id)

				<div class="field-group"> 
					<div class="row">
						<div class="column medium-5 no-padd">					
							<h2>{{ AdminLanguage::transAdmin('Kupci') }}</h2>
							<select id="JSAnketaKupacId">
								<option value="0">{{ AdminLanguage::transAdmin('Svi') }}</option>
								@foreach($anketaKupci as $anketaKupac)
								<option value="{{ $anketaKupac->web_kupac_id }}" {{($web_kupac_id == $anketaKupac->web_kupac_id) ? 'selected' : '' }}>{{ $anketaKupac->kupac }} {{ isset($anketaKupac->datum) ? '('.$anketaKupac->datum.')' : '' }}</option>
								@endforeach
							</select>
						</div>
						<div class="column medium-5">
							@if(count($anketaNarudzbine) > 0)
							<h2>{{ AdminLanguage::transAdmin('Narudžbine') }}</h2>
							<select id="JSAnketaNarudzbinaId">
								<option value="0">{{ AdminLanguage::transAdmin('Sve') }}</option>
								@foreach($anketaNarudzbine as $anketaNarudzbina)
								<option value="{{ $anketaNarudzbina->web_b2c_narudzbina_id }}" {{($web_b2c_narudzbina_id == $anketaNarudzbina->web_b2c_narudzbina_id) ? 'selected' : '' }}><b>{{ $anketaNarudzbina->broj_dokumenta }}</b> ({{ $anketaNarudzbina->datum }})</option>
								@endforeach
							</select>
							@endif
						</div>
					</div>

					<div class="row">
						<div class="column medium-5 small-5 no-padd">
							<label for="">{{ AdminLanguage::transAdmin('Datum Od') }}</label>
							<input id="anketa_datum_od" class="datum-val has-tooltip" name="anketa_datum_od" type="text" value="{{$anketa_datum_od}}">
						</div>

						<div class="column medium-5 small-5">
							<label for="">{{ AdminLanguage::transAdmin('Datum Do') }}</label>
							<input id="anketa_datum_do" class="datum-val has-tooltip" name="anketa_datum_do" type="text" value="{{$anketa_datum_do}}">
						</div>

						<div class="column medium-2 small-2 no-padd">
							<label class="no-margin">&nbsp;</label>
							<a href="#" class="btn btn-danger fr" id="JSAnketeDatumPonisti">{{ AdminLanguage::transAdmin('Poništi') }}</a>
						</div> 
					</div>
				</div>
				@endif
			</div>

			<div class="column medium-8">
				<div class="table-scroll"> 
					<table class="analitics-table">
						<thead> 
							<tr>
								<th>{{ AdminLanguage::transAdmin('Pitanje') }}</th>
								<th>{{ AdminLanguage::transAdmin('Odgovori') }}</th>
							</tr>
						</thead>

						<tbody>
							@if($anketa_id)
							<tr>
								<td><b>{{ AdminLanguage::transAdmin('Check pitanja') }}</b></td>
								<td></td>
							</tr>

							<?php $pitenjeNaziv = ''; ?>
							@foreach($anketaPitanjaCheckAnalitika as $pitenje)
							<?php $anketaOdgovoriUkupno = AdminAnkete::anketaOdgovoriUkupno($pitenje->anketa_pitanje_id,$web_kupac_id,$anketa_datum_od,$anketa_datum_do,$web_b2c_narudzbina_id); ?>
							<tr>
								<td>{{ ($pitenjeNaziv != $pitenje->pitanje) ? ($pitenjeNaziv = $pitenje->pitanje) : '' }}</td>
								<td><span>{{ $pitenje->odgovor }}</span> - <span>{{ $pitenje->count }}/{{ $anketaOdgovoriUkupno }}</span> <span>({{ round(($pitenje->count*100 / $anketaOdgovoriUkupno)) }} %)</span></td>
							</tr>
							@endforeach

							<tr>
								<td><b>{{ AdminLanguage::transAdmin('Tekst pitanja') }}</b></td>
								<td></td>
							</tr>

							@foreach($anketaPitanjaTextAnalitika as $pitenje)
							<tr>
								<td>{{ ($pitenjeNaziv != $pitenje->pitanje) ? ($pitenjeNaziv = $pitenje->pitanje) : '' }}</td>
								<td><span>{{ !is_null($pitenje->kupac) ? $pitenje->kupac : $pitenje->ip }}</span> - <span>{{ $pitenje->anketa_odgovor_text }}</span></td>
							</tr>
							@endforeach

							@endif

						</tbody>
					</table>
				</div>
			</div> 
		</div>
	</div>



</section> <!-- end of #main-content -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.bundle.js"></script>

<script>

	var ctx = document.getElementById("graph");
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: [
			@foreach(AdminAnalitika::orderYears($od,$do) as $key => $year_name)
			@foreach(array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec") as $month_name)
			"{{ $month_name.' ('.$year_name.')' }}",
			@endforeach
			@endforeach
			],
			datasets: [{
				lineTension: 0.3,
				label: ' {{ AdminLanguage::transAdmin('Narudžbine') }}',
				data: [ @foreach(AdminAnalitika::orderYears($od,$do) as $year)
				@foreach(range(1,12) as $month)
				{{ AdminAnalitika::mesecnaAnalitika($month,$year,$od,$do)}},
				@endforeach
				@endforeach
				],
				backgroundColor: [
				'rgba(118, 186, 108, 0.46)'
				],
				borderColor: [
				'rgba(118, 186, 108, 1)'
				],
				borderWidth: 1
			}]
		},
		options: {
		// responsive: false,
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		}
	}
});

	var ctx2 = document.getElementById("graph2");
	var myChart2 = new Chart(ctx2, {
		type: 'line',
		data: {
			labels: [@foreach(AdminAnalitika::orderYears($od,$do) as $key => $year_name)
			@foreach(array("Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec") as $month_name)
			"{{ $month_name.' ('.$year_name.')' }}",
			@endforeach
			@endforeach
			],
			datasets: [{
				lineTension: 0.3,
				label: ' {{ AdminLanguage::transAdmin('Prihod') }}',
				data: [ @foreach(AdminAnalitika::orderYears($od,$do) as $year)
				@foreach(range(1,12) as $month)
				{{ AdminAnalitika::mesecnaAnalitika2($month,$year,$od,$do)}},
				@endforeach
				@endforeach
				],
				backgroundColor: [
				'rgba(110, 151, 207, 0.58)'
				],
				borderColor: [
				'rgba(110, 151, 207, 1)'
				],
				borderWidth: 1
			}]
		},
		options: {
		// responsive: false,
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}]
		}
	}
});

	var ctx3 = document.getElementById("graph3");
	var myChart3 = new Chart(ctx3, {
		type: 'bar',
		data: {
			labels: [
			@foreach($artikli as $row)
			<?php echo '"'.AdminCommon::analitika_title($row->roba_id) .'",' ?>
			@endforeach
			],
			datasets: [{

				label: ' {{ AdminLanguage::transAdmin('Najprodavaniji artikli') }}',
				data: [ 
				@foreach($artikli as $row)
				{{ "$row->count" }},
				@endforeach
				],
				backgroundColor: [
				@foreach($artikli as $row)
				'rgba(255, 254, 15, 0.55)',
				@endforeach
				
				],
				borderColor: [
				@foreach($artikli as $row)
				'#ffab00',
				@endforeach
				],
				borderWidth: 1
			}]
		},
		options: {
		// responsive: false,
		
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}],
			xAxes: [{display:false}]
		}
	}
});

	var ctx4 = document.getElementById("graph4");
	var myChart4 = new Chart(ctx4, {
		type: 'bar',
		data: {
			labels: [
			@foreach(AdminCommon::mostPopularArticles() as $row)
			<?php echo "'".AdminCommon::analitika_title($row->roba_id) ."'," ?>
			@endforeach
			],
			datasets: [{
				label: ' {{ AdminLanguage::transAdmin('Artikli sa najvećim brojem pregleda') }}',
				data: [ @foreach(AdminCommon::mostPopularArticles() as $row)
				{{ "$row->pregledan_puta" }},
				@endforeach
				],
				backgroundColor: [
				'rgba(240, 65, 36, 0.6)',
				'rgba(240, 65, 36, 0.5)',
				'rgba(240, 65, 36, 0.4)',
				'rgba(240, 65, 36, 0.3)'
				],
				borderColor: [
				'rgba(240, 65, 36, 1)',
				'rgba(240, 65, 36, 1)',
				'rgba(240, 65, 36, 1)',
				'rgba(240, 65, 36, 1)'
				],
				borderWidth: 1
			}]
		},
		options: {
		// responsive: false,
		scales: {
			yAxes: [{
				ticks: {
					beginAtZero:true
				}
			}],
			xAxes: [{display:false}]
		}
	}
});


	var ctx5 = document.getElementById("graph5");
	var myChart5 = new Chart(ctx5, {
		type: 'doughnut',
		data: {
			labels: [
			"{{ AdminLanguage::transAdmin('Nove') }}",
			"{{ AdminLanguage::transAdmin('Prihvaćene porudžbine') }}",
			"{{ AdminLanguage::transAdmin('Realizovane porudžbine') }}",
			"{{ AdminLanguage::transAdmin('Stornirane') }}"

			],
			datasets: [{
				label: '',
				data: [
				{{ $ukupno1 }},
				{{ $ukupno4 }},
				{{ $ukupno3 }}, 
				{{ $ukupno2 }} 
				],
				backgroundColor: [
				"#FF6384", 
				"#59cd74",
				"#a6a6a6",
				"#a370c2"
				],
				borderColor: [
				"#FF6384",
				"#59cd74",
				"#a6a6a6",
				"#a370c2"
				],
				borderWidth: 1
			}]
		},
		options: {
			legend: {
				display: false
			}
		}
	});

	var ctx6 = document.getElementById("graph6");
	var myChart6 = new Chart(ctx6, {
		type: 'doughnut',
		data: {
			labels: [
			@foreach($analitikaSkraceno as $grupa)
			'{{ $grupa->grupa }}',
			@endforeach
			'{{ AdminLanguage::transAdmin('Ostalo') }}'
			],
			datasets: [{
				label: '',
				data:[
				@foreach($analitikaSkraceno as $grupa_count)
				'{{ sprintf("%.2f",$grupa_count->ukupno) }}',
				@endforeach
				'{{ sprintf("%.2f",$ostalo) }}'
				],
				backgroundColor: [
				"#59cd74",
				"#FFCE56",
				"#FF6384",
				"#a6a6a6",
				"#009933",
				"#8080ff"
				],
				borderColor: [
				"#59cd74",
				"#FFCE56",
				"#FF6384",
				"#a6a6a6",
				"#009933",
				"#8080ff"
				],
				borderWidth: 1
			}]
		},
		options: {
			legend: {
				display: true,
				position: 'bottom'
			}
		}
	});

	var ctx7 = document.getElementById("graph7");
	var myChart7 = new Chart(ctx7, {
		type: 'bar',
		data: {
			labels: [
			@foreach($analitikaRuc as $ruc)
			'{{ $ruc->grupa }}',
			@endforeach
			
			],
			datasets: [{
				label: '',
				data:[
				<?php $s=0 ?>
				@foreach($analitikaRuc as $grupa_count)
				
				'{{ sprintf("%.2f",$grupa_count->razlika) }}',
				
				@endforeach
				@if ($ostalo1>0)
				'{{ sprintf("%.2f",$ostalo1) }}'
				@endif
				],
				backgroundColor: [
				"#59cd74",
				"#FFCE56",
				"#FF6384",
				"#a6a6a6",
				"#009933",
				"#8080ff"
				],
				borderColor: [
				"#59cd74",
				"#FFCE56",
				"#FF6384",
				"#a6a6a6",
				"#009933",
				"#8080ff"
				],
				borderWidth: 1
			}]
		},
		options: {
			legend: {
				display: false,
				position: 'bottom'
			}
		}
	});





	var ctx8 = document.getElementById("graph8");
	var myChart8 = new Chart(ctx8, {
		type: 'doughnut',
		data: {
			labels: [
			@foreach($analitikaGrupaPregled as $grupa)
			'{{ $grupa->grupa1 }}',
			@endforeach
			],
			datasets: 
			[{
				label: '',
				data:[
				@foreach($analitikaGrupaPregled as $grupa_sum)
				'{{ $grupa_sum->sum }}',
				@endforeach
				],
				backgroundColor: [
				"#59cd74",
				"#FFCE56",
				"#FF6384",
				"#a6a6a6",
				"#009933"

				],
				borderColor: [
				"#59cd74",
				"#FFCE56",
				"#FF6384",
				"#a6a6a6",
				"#009933"
				],
				borderWidth: 1
			}]
		},
		options: {
			legend: {
				display: true,
				position: 'bottom'
			}
		}
	});

</script>

