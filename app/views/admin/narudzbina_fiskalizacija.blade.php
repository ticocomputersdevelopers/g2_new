
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
@if(Session::has('error_message'))
	<script>
		alertify.error('{{ Session::get('error_message') }}');
	</script>
@endif


<section id="main-content">

		<div class="row m-subnav">
		<div class="large-2 medium-3 small-12 columns ">
			 <a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/narudzbina/{{$narudzbina->web_b2c_narudzbina_id}}">
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					{{ AdminLanguage::transAdmin('Nazad') }}
				</div>
			</a>
		</div>
	</div>


	<div class="row"> 
	  	<div class="columns medium-12 small-12"> 
			<form method="POST" action="{{AdminOptions::base_url()}}admin/narudzbina-fiskalizacija-send/{{$narudzbina->web_b2c_narudzbina_id}}">

				<div class="flat-box"> 
					<div class="field-group">
						<div class="row">

							<div class="columns medium-2">
								<label>{{ AdminLanguage::transAdmin('Kasir') }}:</label>
								<input type="text" name="cashier" value="{{ Session::get('b2c_admin'.AdminOptions::server()) }}" disabled> 
								<div class="error red-dot-error">{{ $errors->first('cashier') ? 'Maksimalno je dozvoljeno 50 karaktera' : '' }}</div>
							</div>

							<div class="columns medium-1">
								<label>{{ AdminLanguage::transAdmin('ID kupca') }}:</label>
								<input type="text" name="buyerId" value="{{ Input::old('buyerId') ? Input::old('buyerId') : AdminFiskalizacija::getBuyerPib($narudzbina->web_b2c_narudzbina_id) }}"> 
								<div class="error red-dot-error">{{ $errors->first('buyerId') ? 'Neispravno popunjeno polje' : '' }}</div>
							</div>


							<div class="columns medium-2">
								<label>{{ AdminLanguage::transAdmin('Opciono polje kupca') }}:</label>
								<input type="text" name="buyerCostCenterId" value="{{ Input::old('buyerCostCenterId') ? Input::old('buyerCostCenterId') : '' }}"> 
							</div>
							
							<div class="columns medium-1"> 
								<label>{{ AdminLanguage::transAdmin('Tip fakture') }}:*</label>
								<select name="invoiceType" id="JSinvoiceType">
									@if(!$ref_racun)
										<option value="Normal" {{ Input::old('invoiceType') == 'Normal' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Promet') }}</option>

										<option value="ProForma" {{ Input::old('invoiceType') == 'ProForma' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Predračun') }}</option>

										<!-- <option value="Copy" {{ Input::old('invoiceType') == 'Copy' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Kopija') }}</option> -->

										<option value="Training" {{ Input::old('invoiceType') == 'Training' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Obuka') }}</option>

										<option value="Advance" {{ Input::old('invoiceType') == 'Advance' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Avans') }}</option>
									@else
										<option value="{{$ref_racun->tip_racuna}}">{{ AdminLanguage::transAdmin(AdminFiskalizacija::mappedVrstaRacuna($ref_racun->tip_racuna)) }}</option>
									@endif
								</select> 
								<div class="error red-dot-error">{{ $errors->first('invoiceType') ? ' Niste popunili polje' : '' }}</div>
							</div> 



							<div class="columns medium-2">
								<label>{{ AdminLanguage::transAdmin('Vrsta transakcije') }}:*</label>
								<select name="transactionType"  id="JStransactionType">
									@if(!$ref_racun)
										<option value="Sale" {{ Input::old('transactionType') == 'Sale' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Prodaja') }}</option>
									@endif
									<option value="Refund" {{ Input::old('transactionType') == 'Refund' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Refundacija') }}</option>
								</select>
								<div class="error red-dot-error">{{ $errors->first('transactionType') ? ' Niste popunili polje' : '' }}</div>
							</div>

							<div class="columns medium-2 JSreferentDocumentNumber">
								<label>{{ AdminLanguage::transAdmin('Referentni broj') }}:</label>
								@if($ref_racun)
									<input type="text" name="referentDocumentNumber" placeholder="xxxxxxxx-xxxxxxxx-xxx" value="{{ $ref_racun->broj_dokumenta }}">
								@elseif($ref_racun_avans)
									<input type="text" name="referentDocumentNumber" placeholder="xxxxxxxx-xxxxxxxx-xxx" value="{{ $ref_racun_avans->broj_dokumenta }}">
								@else
									<input type="text" name="referentDocumentNumber" placeholder="xxxxxxxx-xxxxxxxx-xxx" value="{{ Input::old('referentDocumentNumber') ? Input::old('referentDocumentNumber') : '' }}">
								@endif 
								<div class="error red-dot-error">{{ $errors->first('referentDocumentNumber') ? 'Niste uneli odgovarajući format' : '' }}</div>
							</div>

							<div class="columns medium-2">
								<label>{{ AdminLanguage::transAdmin('Referentno pfr vreme') }}:</label>
								@if($ref_racun)
									<input type="text" name="referentDocumentDT"  placeholder="yyyy-MM-dd HH:mm:ss" value="{{ $ref_racun->datum_racuna }}">
								@elseif($ref_racun_avans)
									<input type="text" name="referentDocumentDT"  placeholder="yyyy-MM-dd HH:mm:ss" value="{{ $ref_racun_avans->datum_racuna }}">
								@else
									<input type="text" name="referentDocumentDT"  placeholder="yyyy-MM-dd HH:mm:ss" value="{{ Input::old('referentDocumentDT') ? Input::old('referentDocumentDT') : '' }}">
								@endif 
								<div class="error red-dot-error">{{ $errors->first('referentDocumentDT') ? ' Niste uneli odgovarajući format datuma' : '' }}</div>
							</div>


						</div>
					</div>
				</div>


				<div class="flat-box"> 
					<div class="field-group JSfiscalContainer">
						<!-- <h5 class="title-med">{{  AdminLanguage::transAdmin('Stavke') }}</h5> <button type="button" class="btn JSfiscalNewProduct">NOVI</button> -->
								
						@foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$narudzbina->web_b2c_narudzbina_id)->get() as $key =>$narudzbina_stavka)
						  
						<div class="row JSfiscalProduct" {{ empty(Input::old()) ? '' : (isset(Input::old('items')[$key]['name']) ? '' : 'disabled') }}> 

								<div hidden>	
									<input type="text" name="items[{{$key}}][roba_id]" value="{{ $narudzbina_stavka->roba_id }}">
								</div>

								<div class="columns medium-1">
						  			<label>{{ AdminLanguage::transAdmin('GTIN') }}:</label>
						                	<input {{ empty(Input::old()) ? '' : (isset(Input::old('items')[$key]['name']) ? '' : 'disabled') }} type="text" name="items[{{$key}}][gtin]" value="{{ isset(Input::old('items')[$key]['gtin']) ? Input::old('items')[$key]['gtin']  : '' }}">
						            <div class="error red-dot-error">{{ $errors->first('items.'.$key.'.gtin') ? 'Ovo polje zahteva od 8 do 14 karaktera' : '' }}</div>
						  		</div>
						  		<div class="columns medium-3">
						  			<label>{{ AdminLanguage::transAdmin('Naziv') }}:*</label>
						  			<input {{ empty(Input::old()) ? '' : (isset(Input::old('items')[$key]['name']) ? '' : 'disabled') }} type="text" name="items[{{$key}}][name]" value="{{ isset(Input::old('items')[$key]['name']) ? htmlentities(Input::old('items')[$key]['name'])  : htmlentities(AdminArticles::find($narudzbina_stavka->roba_id,'naziv_web')) }}">
						  			<div class="error red-dot-error">{{ $errors->first('items.'.$key.'.name') ? ' Neispravno popunjeno polje' : '' }}</div>
						  		</div>
						  		<div class="columns medium-1">
						  			<label>{{ AdminLanguage::transAdmin('Jedinica mere') }}:*</label>
						  			<input {{ empty(Input::old()) ? '' : (isset(Input::old('items')[$key]['name']) ? '' : 'disabled') }} type="text" name="items[{{$key}}][unit]" value="{{ isset(Input::old('items')[$key]['unit']) ? Input::old('items')[$key]['unit']  : AdminFiskalizacija::getUnit($narudzbina_stavka->roba_id) }}">
						  			<div class="error red-dot-error">{{ $errors->first('items.'.$key.'.unit') ? 'Obavezno polje' : '' }}</div>
						  		</div>
						  		<div class="columns medium-1">
						  			<label>{{ AdminLanguage::transAdmin('Količina') }}:*</label>
						  			<input {{ empty(Input::old()) ? '' : (isset(Input::old('items')[$key]['name']) ? '' : 'disabled') }} class="JSfiscalUnitQuant" type="text" name="items[{{$key}}][quantity]" value="{{ isset(Input::old('items')[$key]['quantity']) ? Input::old('items')[$key]['quantity'] : number_format($narudzbina_stavka->kolicina, 3, '.', ' ') }}">
						  			<div class="error red-dot-error">{{ $errors->first('items.'.$key.'.quantity') ? ' Neispravno popunjeno polje' : '' }}</div> 
						  		</div>
						  		<div class="columns medium-2">
						  			<label>{{ AdminLanguage::transAdmin('Cena po jedinici') }}:*</label>
						  			<input {{ empty(Input::old()) ? '' : (isset(Input::old('items')[$key]['name']) ? '' : 'disabled') }} class="JSfiscalUnitPrice" type="text" name="items[{{$key}}][unitPrice]" value="{{ isset(Input::old('items')[$key]['unitPrice']) ? Input::old('items')[$key]['unitPrice'] : number_format($narudzbina_stavka->jm_cena, 2, '.', '') }}"> 
						  			<div class="error red-dot-error">{{ $errors->first('items.'.$key.'.unitPrice') ? 'Neispravno popunjeno polje' : '' }}</div>
						  		</div>

						  		<div class="columns medium-2">
						  			<label>{{ AdminLanguage::transAdmin('Poreska stopa') }}:*</label>
						  			
						  			<select name="items[{{$key}}][labels][]" {{ empty(Input::old()) ? '' : (isset(Input::old('items')[$key]['name']) ? '' : 'disabled') }}>
						  				<option value=""></option>
						  			@foreach(AdminFiskalizacija::vcsdServiceGetTaxes() as $tax)
						  				@foreach($tax->taxRates as $taxVal)
						  				<option name="items[{{$key}}][labels][]" value="{{ $taxVal->label }}" {{ (isset(Input::old('items')[$key]['labels']) && in_array($taxVal->label,Input::old('items')[$key]['labels'])) ? 'selected' : AdminFiskalizacija::getTexValue($narudzbina_stavka->roba_id) == $taxVal->rate ? 'selected' : ''  }}>Poreska oznaka {{ $taxVal->label }}  {{ $tax->name }} ( {{ $taxVal->rate }} % )</option>
						  				@endforeach
						  			@endforeach
									</select>

						  			<div class="error red-dot-error">{{ $errors->first('items.'.$key.'.labels.0') ? 'Izaberite barem jednu poresku oznaku' : '' }}</div>
						  		</div>
						  		<div class="columns medium-2">
						  			<label>{{ AdminLanguage::transAdmin('Ukupan iznos') }}*</label>
						  			<input class="JSfiscalTotalAmount" type="text" value="{{ isset(Input::old('items')[$key]['unitPrice']) && isset(Input::old('items')[$key]['quantity']) ? number_format(Input::old('items')[$key]['unitPrice']*Input::old('items')[$key]['quantity'], 2, '.', '') : number_format($narudzbina_stavka->jm_cena*$narudzbina_stavka->kolicina, 2, '.', '') }}" disabled>
						  		</div>

						  		@if(sizeof(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$narudzbina->web_b2c_narudzbina_id)->get()) > 1)
							  		<div class="columns medium-1">
							  			<div class="JStaxDisable">
							  				<input class="JSfiscalProductDisable" type="checkbox" {{ empty(Input::old()) ? '' : (isset(Input::old('items')[$key]['name']) ? '' : 'checked') }}><label>Isključi</label>
							  				<!-- <button type="button" class="btn JSfiscalProductRemove">X</button> -->
							  			</div>
							  		</div>
						  		@endif
								

						  </div>
						  		@endforeach
					</div>
				</div>
				<div class="error red-dot-error text-center">{{ $errors->first('items') ? 'Morate imati barem jedan artikal na računu!' : '' }}</div>
				
				<div class="columns medium-2 JSpaymentDropdown-check-list">
		  			<label class="paymentAnchor">{{ AdminLanguage::transAdmin('Placanja') }}:*</label>
		  				
		  			<ul class="JSpaymentDrop">
		  				<li><input type="checkbox" id="placanje0" name="placanje0" value="Other" {{ (is_null(Input::old('payment.0.paymentType')) && is_null(Input::old('payment.0.amount'))) ? (($fiskalni_naziv) == 'Other' && is_null(Input::old('payment')) ? 'checked' : '') : 'checked' }}>Drugo</li>
		  				<li><input type="checkbox" id="placanje1" name="placanje1" value="Cash" {{ (is_null(Input::old('payment.1.paymentType')) && is_null(Input::old('payment.1.amount'))) ? (($fiskalni_naziv) == 'Cash' && is_null(Input::old('payment')) ? 'checked' : '') : 'checked' }}>Gotovina</li>
		  				<li><input type="checkbox" id="placanje2" name="placanje2" value="Card" {{ (is_null(Input::old('payment.2.paymentType')) && is_null(Input::old('payment.2.amount'))) ? (($fiskalni_naziv) == 'Card' && is_null(Input::old('payment')) ? 'checked' : '') : 'checked' }}>Platna kartica</li>
		  				<li><input type="checkbox" id="placanje3" name="placanje3" value="Check" {{ (is_null(Input::old('payment.3.paymentType')) && is_null(Input::old('payment.3.amount'))) ? (($fiskalni_naziv) == 'Check' && is_null(Input::old('payment')) ? 'checked' : '') : 'checked' }}>Ček</li>
		  				<li><input type="checkbox" id="placanje4" name="placanje4" value="WireTransfer" {{ (is_null(Input::old('payment.4.paymentType')) && is_null(Input::old('payment.4.amount'))) ? (($fiskalni_naziv) == 'WireTransfer' && is_null(Input::old('payment')) ? 'checked' : '') : 'checked' }}>Prenos na račun</li>
		  				<li><input type="checkbox" id="placanje5" name="placanje5" value="Voucher" {{ (is_null(Input::old('payment.5.paymentType')) && is_null(Input::old('payment.5.amount'))) ? (($fiskalni_naziv) == 'Voucher' && is_null(Input::old('payment')) ? 'checked' : '') : 'checked' }}>Vaučer</li>
		  				<li><input type="checkbox" id="placanje6" name="placanje6" value="MobileMoney" {{ (is_null(Input::old('payment.6.paymentType')) && is_null(Input::old('payment.6.amount'))) ? (($fiskalni_naziv) == 'MobileMoney' && is_null(Input::old('payment')) ? 'checked' : '') : 'checked' }}>Instant plaćanje</li>
		  				
		  			</ul>
		  		</div>

				<div class="flat-box"> 
					<div class="field-group payment-field-group">
						
						<h5 class="title-med">{{  AdminLanguage::transAdmin('Placanje') }}</h5>
						<div id="paymentOther" class="row {{ (is_null(Input::old('payment.0.paymentType')) && is_null(Input::old('payment.0.amount'))) ? (($fiskalni_naziv) == 'Other' && is_null(Input::old('payment')) ? '' : 'hidden') : '' }}">
							 <div class="columns medium-6">

								<label for="">Drugo</label>
								<input id="paymentTypeOther" type="hidden" name="payment[0][paymentType]" value="Other" {{ (is_null(Input::old('payment.0.paymentType')) && is_null(Input::old('payment.0.amount'))) ? (($fiskalni_naziv) == 'Other' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}>
								<div class="error red-dot-error">{{ $errors->first('payment.0.paymentType') ? 'Niste popunili polje' : '' }}</div>
						  	</div>

						  	<div class="columns medium-6">
						  			<label>{{ AdminLanguage::transAdmin('Iznos uplate') }}:*</label>
						  			<input id="paymentAmountOther" type="text" name="payment[0][amount]" value="{{ (is_null(Input::old('payment.0.paymentType')) && is_null(Input::old('payment.0.amount'))) ? (($fiskalni_naziv) == 'Other' && is_null(Input::old('payment')) ? $iznos_narudzbine : '') : Input::old('payment.0.amount') }}" {{ (is_null(Input::old('payment.0.paymentType')) && is_null(Input::old('payment.0.amount'))) ? (($fiskalni_naziv) == 'Other' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}>
						  			<div class="error red-dot-error">{{ $errors->first('payment.0.amount') ? 'Neispravno popunjeno polje' : '' }}</div>
						  	</div>
						</div>

						<div id="paymentCash" class="row {{ (is_null(Input::old('payment.1.paymentType')) && is_null(Input::old('payment.1.amount'))) ? (($fiskalni_naziv) == 'Cash' && is_null(Input::old('payment')) ? '' : 'hidden') : '' }}">
							 <div class="columns medium-6">

								<label for="">Gotovina</label>
								<input id="paymentTypeCash" type="hidden" name="payment[1][paymentType]" value="Cash" {{ (is_null(Input::old('payment.1.paymentType')) && is_null(Input::old('payment.1.amount'))) ? (($fiskalni_naziv) == 'Cash' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}>
								<div class="error red-dot-error">{{ $errors->first('payment.1.paymentType') ? 'Niste popunili polje' : '' }}</div>
						  	</div>
						  	<div class="columns medium-6">
						  			<label>{{ AdminLanguage::transAdmin('Iznos uplate') }}:*</label>
						  			<input id="paymentAmountCash" type="text" name="payment[1][amount]" value="{{ (is_null(Input::old('payment.1.paymentType')) && is_null(Input::old('payment.1.amount'))) ? (($fiskalni_naziv) == 'Cash' && is_null(Input::old('payment')) ? $iznos_narudzbine : '') : Input::old('payment.1.amount') }}" {{ (is_null(Input::old('payment.1.paymentType')) && is_null(Input::old('payment.1.amount'))) ? (($fiskalni_naziv) == 'Cash' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}> 
						  			<div class="error red-dot-error">{{ $errors->first('payment.1.amount') ? 'Neispravno popunjeno polje' : '' }}</div>
						  	</div>
						</div>

						<div id="paymentCard" class="row {{ (is_null(Input::old('payment.2.paymentType')) && is_null(Input::old('payment.2.amount'))) ? (($fiskalni_naziv) == 'Card' && is_null(Input::old('payment')) ? '' : 'hidden') : '' }}">
							 <div class="columns medium-6">

								<label for="">Platna kartica</label>
								<input id="paymentTypeCard" type="hidden" name="payment[2][paymentType]" value="Card" {{ (is_null(Input::old('payment.2.paymentType')) && is_null(Input::old('payment.2.amount'))) ? (($fiskalni_naziv) == 'Card' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}>
								<div class="error red-dot-error">{{ $errors->first('payment.2.paymentType') ? 'Niste popunili polje' : '' }}</div>
						  	</div>
						  	<div class="columns medium-6">
						  			<label>{{ AdminLanguage::transAdmin('Iznos uplate') }}:*</label>
						  			<input id="paymentAmountCard" type="text" name="payment[2][amount]" value="{{ (is_null(Input::old('payment.2.paymentType')) && is_null(Input::old('payment.2.amount'))) ? (($fiskalni_naziv) == 'Card' && is_null(Input::old('payment')) ? $iznos_narudzbine : '') : Input::old('payment.2.amount') }}" {{ (is_null(Input::old('payment.2.paymentType')) && is_null(Input::old('payment.2.amount'))) ? (($fiskalni_naziv) == 'Card' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}> 
						  			<div class="error red-dot-error">{{ $errors->first('payment.2.amount') ? 'Neispravno popunjeno polje' : '' }}</div>
						  	</div>
						</div>

						<div id="paymentCheck" class="row {{ (is_null(Input::old('payment.3.paymentType')) && is_null(Input::old('payment.3.amount'))) ? (($fiskalni_naziv) == 'Check' && is_null(Input::old('payment')) ? '' : 'hidden') : '' }}">
							 <div class="columns medium-6">

								<label for="">Ček</label>
								<input id="paymentTypeCheck" type="hidden" name="payment[3][paymentType]" value="Check" {{ (is_null(Input::old('payment.3.paymentType')) && is_null(Input::old('payment.3.amount'))) ? (($fiskalni_naziv) == 'Check' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}>
								<div class="error red-dot-error">{{ $errors->first('payment.3.paymentType') ? 'Niste popunili polje' : '' }}</div>
						  	</div>
						  	<div class="columns medium-6">
						  			<label>{{ AdminLanguage::transAdmin('Iznos uplate') }}:*</label>
						  			<input id="paymentAmountCheck" type="text" name="payment[3][amount]" value="{{ (is_null(Input::old('payment.3.paymentType')) && is_null(Input::old('payment.3.amount'))) ? (($fiskalni_naziv) == 'Check' && is_null(Input::old('payment')) ? $iznos_narudzbine : '') : Input::old('payment.3.amount') }}" {{ (is_null(Input::old('payment.3.paymentType')) && is_null(Input::old('payment.3.amount'))) ? (($fiskalni_naziv) == 'Check' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}> 
						  			<div class="error red-dot-error">{{ $errors->first('payment.3.amount') ? 'Neispravno popunjeno polje' : '' }}</div>
						  	</div>
						</div>

						<div id="paymentWireTransfer" class="row {{ (is_null(Input::old('payment.4.paymentType')) && is_null(Input::old('payment.4.amount'))) ? (($fiskalni_naziv) == 'WireTransfer' && is_null(Input::old('payment')) ? '' : 'hidden') : '' }}">
							 <div class="columns medium-6">

								<label for="">Prenos na račun</label>
								<input id="paymentTypeWireTransfer" type="hidden" name="payment[4][paymentType]" value="WireTransfer" {{ (is_null(Input::old('payment.4.paymentType')) && is_null(Input::old('payment.4.amount'))) ? (($fiskalni_naziv) == 'WireTransfer' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}>
								<div class="error red-dot-error">{{ $errors->first('payment.4.paymentType') ? 'Niste popunili polje' : '' }}</div>
						  	</div>
						  	<div class="columns medium-6">
						  			<label>{{ AdminLanguage::transAdmin('Iznos uplate') }}:*</label>
						  			<input id="paymentAmountWireTransfer" type="text" name="payment[4][amount]" value="{{ (is_null(Input::old('payment.4.paymentType')) && is_null(Input::old('payment.4.amount'))) ? (($fiskalni_naziv) == 'WireTransfer' && is_null(Input::old('payment')) ? $iznos_narudzbine : '') : Input::old('payment.4.amount') }}" {{ (is_null(Input::old('payment.4.paymentType')) && is_null(Input::old('payment.4.amount'))) ? (($fiskalni_naziv) == 'WireTransfer' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}>
						  			<div class="error red-dot-error">{{ $errors->first('payment.4.amount') ? 'Neispravno popunjeno polje' : '' }}</div> 
						  	</div>
						</div>

						<div id="paymentVoucher" class="row {{ (is_null(Input::old('payment.5.paymentType')) && is_null(Input::old('payment.5.amount'))) ? (($fiskalni_naziv) == 'Voucher' && is_null(Input::old('payment')) ? '' : 'hidden') : '' }}">
							 <div class="columns medium-6">

								<label for="">Vaučer</label>
								<input id="paymentTypeVoucher" type="hidden" name="payment[5][paymentType]" value="Voucher" {{ (is_null(Input::old('payment.5.paymentType')) && is_null(Input::old('payment.5.amount'))) ? (($fiskalni_naziv) == 'Voucher' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}>
								<div class="error red-dot-error">{{ $errors->first('payment.5.paymentType') ? 'Niste popunili polje' : '' }}</div>
						  	</div>
						  	<div class="columns medium-6">
						  			<label>{{ AdminLanguage::transAdmin('Iznos uplate') }}:*</label>
						  			<input id="paymentAmountVoucher" type="text" name="payment[5][amount]" value="{{ (is_null(Input::old('payment.5.paymentType')) && is_null(Input::old('payment.5.amount'))) ? (($fiskalni_naziv) == 'Voucher' && is_null(Input::old('payment')) ? $iznos_narudzbine : '') : Input::old('payment.5.amount') }}" {{ (is_null(Input::old('payment.5.paymentType')) && is_null(Input::old('payment.5.amount'))) ? (($fiskalni_naziv) == 'Voucher' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}> 
						  			<div class="error red-dot-error">{{ $errors->first('payment.5.amount') ? 'Neispravno popunjeno polje' : '' }}</div> 
						  	</div>
						</div>

						<div id="paymentMobileMoney" class="row {{ (is_null(Input::old('payment.6.paymentType')) && is_null(Input::old('payment.6.amount'))) ? (($fiskalni_naziv) == 'MobileMoney' && is_null(Input::old('payment')) ? '' : 'hidden') : '' }}">
							 <div class="columns medium-6">

								<label for="">Instant plaćanje</label>
								<input id="paymentTypeMobileMoney" type="hidden" name="payment[6][paymentType]" value="MobileMoney" {{ (is_null(Input::old('payment.6.paymentType')) && is_null(Input::old('payment.6.amount'))) ? (($fiskalni_naziv) == 'MobileMoney' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}>
								<div class="error red-dot-error">{{ $errors->first('payment.6.paymentType') ? 'Niste popunili polje' : '' }}</div>
						  	</div>
						  	<div class="columns medium-6">
						  			<label>{{ AdminLanguage::transAdmin('Iznos uplate') }}:*</label>
						  			<input id="paymentAmountMobileMoney" type="text" name="payment[6][amount]" value="{{ (is_null(Input::old('payment.6.paymentType')) && is_null(Input::old('payment.6.amount'))) ? (($fiskalni_naziv) == 'MobileMoney' && is_null(Input::old('payment')) ? $iznos_narudzbine : '') : Input::old('payment.6.amount') }}" {{ (is_null(Input::old('payment.6.paymentType')) && is_null(Input::old('payment.6.amount'))) ? (($fiskalni_naziv) == 'MobileMoney' && is_null(Input::old('payment')) ? '' : 'disabled="disabled"') : '' }}> 
						  			<div class="error red-dot-error">{{ $errors->first('payment.6.amount') ? 'Neispravno popunjeno polje' : '' }}</div>
						  	</div>
						</div>
					</div>
				</div>

				<div class="error red-dot-error text-center">{{ $errors->first('payment') ? 'Morate imati barem jedno izabrano plaćanje!' : '' }}</div>

				<div class="row">
					
					<div class="columns medium-6 {{ !is_null(Input::old('invoiceType')) && Input::old('invoiceType') == 'Advance' &&  !is_null(Input::old('transactionType')) &&  Input::old('transactionType') == 'Sale' ? '' : 'hidden' }}" id="JSReklama">
						<label>{{ AdminLanguage::transAdmin('Reklama') }}: <i class="fa fa-info-circle tooltipz" aria-hidden="true" aria-label='Unesite nazive i cene artikala:
							<br>Ispred naziva artikala stavite *
							<br>Nazive artikala i cene odvajajte -- Primer:<br>
							*Naziv artika 1--cena 1<br>
							*Naziv artika 2--cena 2<br><br>
							Ovo polje nije obavezno i unosi se po zahtevu kupca.
							'></i></label>

						<textarea name="reklama" id="JSReklamaTextarea" {{ !is_null(Input::old('invoiceType')) && Input::old('invoiceType') == 'Advance' &&  !is_null(Input::old('transactionType')) &&  Input::old('transactionType') == 'Sale' ? '' : 'disabled="disabled"' }}>{{ Input::old('reklama') ? Input::old('reklama') : ''}}</textarea>
					</div>
					<div class="columns medium-6 medium-offset-6">
						<label>{{ AdminLanguage::transAdmin('Za uplatu') }}:</label>
						<input type="text" class="JSfiscalSumTotal" disabled="disabled">
					</div>
					

					<div class="columns medium-12 text-right margin-h-10">
					  	<input class="btn inline-block send_order_fiskal" type="submit" value="Pošalji">
					  	<a class="btn inline-block" href="{{ AdminOptions::base_url().'admin/narudzbina-fiskalizacija/'.$narudzbina->web_b2c_narudzbina_id }}"> {{ AdminLanguage::transAdmin('Obriši sve') }} </a>
					</div>
				</div>
			</form>
		</div>
	</div>



<div class="text-center">Fiskalizacija © {{date('Y')}}. Sva prava zadržana. - Izrada Selltico - verzija softvera SellPos 889 1.0.</div>






</section>