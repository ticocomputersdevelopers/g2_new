@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
@if(Session::has('error_message'))
	<script>
		alertify.error('{{ Session::get('error_message') }}');
	</script>
@endif

<section id="main-content" class="billing-info-page">

	<div class="row m-subnav">
		<div class="large-3 medium-3 small-12 columns ">
			 <a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/narudzbina/{{$web_b2c_narudzbina_id}}">
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					{{ AdminLanguage::transAdmin('Nazad') }}
				</div>
			</a>
		</div>
	</div>

	<div class="row">
		<h3 class="title-med">{{ AdminLanguage::transAdmin('Detalji računa') }}</h3>
	</div>
	<div class="flat-box">
		<h3 class="no-margin">{{  AdminLanguage::transAdmin('Racun') }}: {{ $racun->broj_dokumenta }}</h3>
	</div>

	<div class="flat-box text-center">
		<a href="{{ $racun->verifikacija_url }}" target="_blank"><button class="btn btn-primary"> {{  AdminLanguage::transAdmin('Verifikacija') }} </button></a>
		<a href="{{ AdminOptions::base_url().'admin/fiskalizacija_isecak/'.$racun->racun_id }}" target="_blank"><button class="btn btn-primary"> {{  AdminLanguage::transAdmin('Isečak') }} </button></a>
		<a href="{{ AdminOptions::base_url().'admin/fiskalizacija_racun/'.$racun->racun_id }}" target="_blank"><button class="btn btn-primary"> {{  AdminLanguage::transAdmin('Račun') }} </button></a>

		@if($racun->tip_racuna != 'Copy')
			<a class="btn btn-danger JSFiscalDetails-CopySend" >{{ AdminLanguage::transAdmin('Napravi kopiju') }}</a>
		
			<div class="JSFiscalCopyAlert">
				<label>{{ AdminLanguage::transAdmin('Da li ste sigurni da želite napraviti kopiju računa?') }}</label>
				<form method="post" action="{{ AdminOptions::base_url().'admin/fiskalizacija-detalji-send/'.$web_b2c_narudzbina_id.'/'.$racun->racun_id }}">
				<input type="hidden" name="action" value="Copy">
				<button>U redu</button>
				<button type="button" class="JSArticleDetails-CopySend-no">Otkaži</button>
				</form>
			</div>
		@endif


		@if($racun->vrsta_transakcije != 'Refund' && !AdminFiskalizacija::checkRefund($racun->racun_id))
		<a class="btn btn-danger JSFiscalDetails-RefundSend">{{  AdminLanguage::transAdmin('Refundiraj ceo iznos') }}</a>

		<div class="JSFiscalRefundAlert">
			<label>{{ AdminLanguage::transAdmin('Da li ste sigurni da želite refundirati račun?') }}</label>
			<form method="post" action="{{ AdminOptions::base_url().'admin/fiskalizacija-detalji-send/'.$web_b2c_narudzbina_id.'/'.$racun->racun_id }}">
			<input type="hidden" name="action" value="Refund">
			@if(empty($racun->kupac_id) || !AdminFiskalizacija::checkBuyerId($racun->kupac_id))
			<label class="fiscalizationBuyerIdInput">
				<label>{{ AdminLanguage::transAdmin('ID kupca je obavezan za račun refundacije i mora biti u odgovarajućem formatu. ') }}</label><br>
				<input type="text" name="buyerId" value="">
			</label>
			@else
			<label class="fiscalizationBuyerIdInput">
				<label>{{ AdminLanguage::transAdmin('ID kupca je obavezan za račun refundacije i mora biti u odgovarajućem formatu. ') }}</label><br>
				<input type="text" name="buyerId" value="{{ $racun->kupac_id }}">
			</label>
			@endif
			<button>U redu</button>
			<button type="button" class="JSArticleDetails-RefundSend-no">Otkaži</button>
			</form>
		</div>
		@endif

		@if($racun->vrsta_transakcije != 'Refund' && !AdminFiskalizacija::checkRefund($racun->racun_id))
		<a class="btn btn-danger" href="{{ AdminOptions::base_url().'admin/narudzbina-fiskalizacija-refundacija/'.$web_b2c_narudzbina_id.'/'.$racun->racun_id }}">{{  AdminLanguage::transAdmin('Refundiraj deo iznosa') }}</a>
		@endif

		@if($racun->vrsta_transakcije == 'Refund' && $racun->tip_racuna == 'Advance' && !AdminFiskalizacija::checkNormalSale($racun->racun_id))
		<a class="btn btn-danger" href="{{ AdminOptions::base_url().'admin/narudzbina-fiskalizacija-zatvaranje-avansa/'.$web_b2c_narudzbina_id.'/'.$racun->racun_id }}">{{  AdminLanguage::transAdmin('Zatvori avans') }}</a>
		@endif

		<button class="btn btn-primary JSFiscalMailSend"> {{  AdminLanguage::transAdmin('Pošalji e-poštom') }} </button>
		<div class="JSFiscalMailAlert">
			<label>{{ AdminLanguage::transAdmin('Da li želite da pošaljete mail?') }}</label>
			<form method="post" action="{{ AdminOptions::base_url().'admin/fiskalizacija-detalji-send/'.$web_b2c_narudzbina_id.'/'.$racun->racun_id }}">
			<input type="hidden" name="action" value="mail-send">
			<label class="fiscalizationMailInput">
				<input type="text" name="email" value="{{ AdminFiskalizacija::getKupacMail($web_b2c_narudzbina_id) }}">
			</label>
			<button>U redu</button>
			<button type="button" class="JSFiscalMailSend-no">Otkaži</button>
			</form>
		</div>
	</div>

	<div class="flat-box text-center">

		<ul class="tabs border-btm" data-tab role="tablist">
		  <li class="tab-title active" role="presentation"><a href="#bill" role="tab" tabindex="0" aria-selected="true" aria-controls="bill">Račun</a></li>
		  <li class="tab-title" role="presentation"><a href="#status" role="tab" tabindex="0" aria-selected="false" aria-controls="status">Status računa</a></li>
		  <li class="tab-title" role="presentation"><a href="#signature" role="tab" tabindex="0" aria-selected="false" aria-controls="signature">Digitalni potpis | Interni podaci</a></li>
		  <li class="tab-title" role="presentation"><a href="#clipping" role="tab" tabindex="0" aria-selected="false" aria-controls="clipping">Žurnal</a></li>
		</ul>

		<div class="tabs-content text-left">

		<!-- RACUN -->
		  <section role="tabpanel" aria-hidden="false" class="content active" id="bill">

		  	<div class="row">

			  	<div class="columns medium-5 margin-h-10">
			  		<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('PIB') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right"> {{ $racun->pib }} </li>
				  	</ul>
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Dobavljač') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right"> {{ $racun->poslovno_ime }} </li>
				  	</ul>
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Mesto prodaje') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right"> {{ $racun->naziv_lokacije }} </li>
				  	</ul>
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Adresa') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right"> {{ $racun->adresa }} </li>
				  	</ul>
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Opština') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right"> {{ AdminFiskalizacija::cirilicToLatin($racun->okrug) }} </li>
				  	</ul>
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Kasir') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right"> {{ $racun->kasir }} </li>
				  	</ul>
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('ID kupca') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right"> {{ $racun->kupac_id }} </li>
				  	</ul>
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Opciono polje kupca') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right"> {{ $racun->kupac_mesto_placanja_id }} </li>
				  	</ul>
				  	@if(!empty($racun->esir))
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Esir broj') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right">{{ $racun->esir }}</li>
				  	</ul>
				  	@endif
				  	@if(!empty($racun->datum_prometa))
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Esir vreme') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right">{{ !empty($racun->datum_prometa) ? date('d.m.Y. H:i:s',strtotime($racun->datum_prometa)) : '' }}</li>
				  	</ul>
				  	@endif
				  	@if(!empty($racun->referentni_broj_dokumenta))
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Ref. broj') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right">{{ $racun->referentni_broj_dokumenta }}</li>
				  	</ul>
				  	<ul class="row border-btm">
				  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Ref. vreme') }}: </li>
				  		<li class="columns medium-6 padding-v-5 text-right">{{ date('d.m.Y. H:i:s',strtotime($racun->referentni_dokument_datum)) }}</li>
				  	</ul>
				  	@endif
				</div>

				<div class="columns medium-3 medium-offset-2 margin-h-10">
					<div> {{  AdminLanguage::transAdmin('Broj računa') }} </div>
				    <div class="success-saved text-left no-margin"> {{$racun->broj_dokumenta}} <i class="fa fa-check no-margin"> </i> </div>
				    <div> {{  AdminLanguage::transAdmin('Račun je proveren') }} </div>
				</div>

			  	<div class="columns medium-10 margin-h-10">
			  		<label> {{  AdminLanguage::transAdmin('Artikli') }} </label>
			  		<div class="table-scroll margin-bottom">
				  		<table>
							<thead>
								<tr>
									<th>{{ AdminLanguage::transAdmin('GTIN') }}</th>
									<th width="300px">{{ AdminLanguage::transAdmin('Ime') }}</th>
									<th>{{ AdminLanguage::transAdmin('Cena') }}</th>
									<th>{{ AdminLanguage::transAdmin('Količina') }}</th>
									<th>{{ AdminLanguage::transAdmin('Ukupna cena') }}</th>
								</tr>
							</thead>

							<tbody>
								@foreach($stavke as $stavka)
								<tr> 
									<td>{{ $stavka->gtin }}</td>
									<td><div class="max-width-450">{{ $stavka->naziv_stavke }} ({{ $stavka->etikete }})</div></td>
									<td>{{ str_replace(".",",",$stavka->pcena) }}</td>
									<td>{{ str_replace(".",",",$stavka->kolicina) }}</td>
									<td>{{ str_replace(".",",",$stavka->uk_cena) }}</td>
								</tr>	
								@endforeach
							</tbody>
						</table> 
					</div>


					<label>  {{  AdminLanguage::transAdmin('Poreske stope') }} </label>
					<div class="table-scroll margin-bottom">
				  		<table>
							<thead>
								<tr>
									<th>{{ AdminLanguage::transAdmin('Oznaka poreske stope') }}</th>
									<th width="300px">{{ AdminLanguage::transAdmin('Naziv poreza') }}</th>
									<th>{{ AdminLanguage::transAdmin('Stopa') }}</th>
									<th>{{ AdminLanguage::transAdmin('Iznos poreza') }}</th>
								</tr>
							</thead>

							<tbody>
								@foreach($porezi as $porez)
								<tr> 
									<td>{{ $porez->poreska_oznaka }}</td>
									<td>{{ $porez->naziv_kategorije }}</td>
									<td>{{ $porez->iznos_poreske_stope}} %</td>
									<td>{{ number_format($porez->iznos_poreza,2, ',', '') }}</td>
								</tr>	
								@endforeach
								<tr> 
									<td colspan="2">  </td>
									<td> <strong> Za uplatu </strong> </td>
									<td>{{ number_format($racun->iznos,2, ',', '') }}</td>
								</tr>	
								<tr> 
									<td colspan="2">  </td>
									<td> <strong> Porez ukupno </strong> </td>
									<td>{{ str_replace(".",",",$porez_ukupno) }}</td>
								</tr>
								<tr> 
									<td colspan="2">  </td>
									<td> <strong> Način plaćanja </strong> </td>
									<td>
										@foreach($placanja as $placanje)
										<div>{{ AdminFiskalizacija::getPlacanjeNaziv($placanje->racun_vrsta_placanja_naziv_id) }}: {{ str_replace(".",",",$placanje->iznos) }}</div>
										@endforeach
									</td>
								</tr>
								<tr> 
									<td colspan="2">  </td>
									<td> <strong> Vrsta računa </strong> </td>
									<td>{{ AdminFiskalizacija::mappedVrstaRacuna($racun->tip_racuna,$racun->vrsta_transakcije) }}</td>
								</tr>
								<tr> 
									<td colspan="2">  </td>
									<td> <strong> PFR vreme (vremenska zona servera) </strong> </td>
									<td>{{ date('d.m.Y. H:i:s',strtotime($racun->datum_racuna)) }}</td>
								</tr>
							</tbody>
						</table> 
					</div>
			  	</div>
			</div>
		  </section>

		  <!-- STATUS RACUNA -->
		  <section role="tabpanel" aria-hidden="true" class="content" id="status">
		    <div class="row">

		    	<div class="columns medium-5 margin-h-10">
		    		<div class="margin-bottom">
		    			<div class="heading-results"> <i class="fa fa-share"></i> {{ AdminLanguage::transAdmin('Zahtev za fiskalizaciju') }} </div>
				  		<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('PIB') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"> {{ $racun->pib }} </li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Zahtevao potpis') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"> {{ $racun->zatrazio }} </li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Esir datum i vreme') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"> {{ date('d.m.Y. H:i:s',strtotime($racun->datum_prometa))}} </li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Kasir') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"> {{ $racun->kasir }} </li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('ID kupca') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"> {{ $racun->kupac_id }} </li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Opciono polje kupca') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"> {{ $racun->kupac_mesto_placanja_id }} </li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Esir broj') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"></li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Način plaćanja') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5">
					  			@foreach($placanja as $placanje)
					  				<div> {{ AdminFiskalizacija::getPlacanjeNaziv($placanje->racun_vrsta_placanja_naziv_id) }} </div>   
					  			@endforeach
					  		</li>
					  	</ul>

					</div>

				  	<div class="margin-bottom">
				  		<div class="margin-top heading-results"> <i class="fa fa-reply"></i> {{ AdminLanguage::transAdmin('Rezultati fiskalizacije računa') }} </div>
				  		<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Zatražio - Potpisao - Brojač') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"> {{ $racun->broj_dokumenta }} </li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-7 padding-v-5"> {{ AdminLanguage::transAdmin('PFR vreme (vremenska zona servera)') }}: </li>
					  		<li class="columns medium-5 text-right padding-v-5"> {{ date('d.m.Y. H:i:s',strtotime($racun->datum_racuna)) }} </li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Brojač računa') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"> {{ $racun->brojac_racuna }} </li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Potpisao račun') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"> {{ $racun->potpisao }} </li>
					  	</ul>
					  	<ul class="row border-btm">
					  		<li class="columns medium-6 padding-v-5"> {{ AdminLanguage::transAdmin('Ukupan iznos') }}: </li>
					  		<li class="columns medium-6 text-right padding-v-5"> {{ str_replace(".",",",$racun->iznos) }} </li>
					  	</ul>
				  	</div>
				</div>

				<div class="columns medium-3 medium-offset-2 margin-h-10">
					<div> {{  AdminLanguage::transAdmin('Broj računa') }} </div>
				    <div class="success-saved text-left no-margin"> {{$racun->broj_dokumenta}} <i class="fa fa-check no-margin"> </i> </div>
				    <div> {{  AdminLanguage::transAdmin('Račun je proveren') }} </div>
				</div>
		    </div>
		  </section>

		  <!-- DIGITALNI POTPIS I PODACI -->
		  <section role="tabpanel" aria-hidden="true" class="content" id="signature">
		       <div class="row">
			    	<div class="columns medium-5 margin-h-10">
			    		<div>
				    		<div class="heading-results"> 
				    			{{ AdminLanguage::transAdmin('Digitalni potpis') }} 
				    		</div>

				    		<div class="margin-bottom margin-left digital-signature padding-v-8 padding-h-8">
						  		{{$racun->digitalni_potpis}}
							</div>
						</div>

						<div>
				    		<div class="margin-top heading-results"> 
				    			{{ AdminLanguage::transAdmin('Interni podaci') }} 
				    		</div>

				    		<div class="margin-bottom margin-left digital-signature padding-v-8 padding-h-8">
						  		{{$racun->interni_podaci}}
							</div>
						</div>
					</div>

					<div class="columns medium-3 medium-offset-2 margin-h-10">
						<div> {{  AdminLanguage::transAdmin('Broj računa') }} </div>
					    <div class="success-saved text-left no-margin"> {{$racun->broj_dokumenta}} <i class="fa fa-check no-margin"> </i> </div>
					    <div> {{  AdminLanguage::transAdmin('Račun je proveren') }} </div>
					</div>
			    </div>
		  </section>

		  <!-- FISKALNI ISECAK -->
		  <section role="tabpanel" aria-hidden="true" class="content" id="clipping">
		     <div class="row">
			    <div class="columns medium-5 margin-h-10 text-center">  


		<?php 
        $text_prikaz = explode("\r\n",$text);
          $text_prikaz_drugi_deo = array($text_prikaz[sizeof($text_prikaz)-2],$text_prikaz[sizeof($text_prikaz)-1]);
          unset($text_prikaz[sizeof($text_prikaz)-2]); unset($text_prikaz[sizeof($text_prikaz)-1]);
          $text_prikaz_prvi_deo = implode("\r\n",$text_prikaz);
          if($change >= 0 || $racun->tip_racuna == 'ProForma') {
          if($change < 0) {
            $change = '0,00';
          }  
          $text_prikaz = explode("========================================",$text_prikaz_prvi_deo);
          $text_prikaz[1] = $text_prikaz[1]."Повраћај:".str_repeat(" ",48-strlen("Повраћај:")-strlen($change)).$change."\r\n";
          $text_prikaz_prvi_deo = implode("========================================",$text_prikaz); 
          }

          if($racun->tip_racuna == 'Normal' && $racun->vrsta_transakcije = 'Sale' && !empty($racun->referentni_broj_dokumenta) && DB::table('racun')->where(array('broj_dokumenta' => $racun->referentni_broj_dokumenta,'tip_racuna' => 'Advance','vrsta_transakcije' => 'Refund'))->first()) {
            $text_prikaz = explode("----------------------------------------",$text_prikaz_prvi_deo);
            $text_prikaz_parrent = explode("\r\n",$text_prikaz[1]);
            $placeno_avansom = number_format(DB::table('racun')->where('broj_dokumenta',$racun->referentni_broj_dokumenta)->pluck('iznos'),2, ',', '.');
            $referentni_racun_id = DB::table('racun')->where('broj_dokumenta',$racun->referentni_broj_dokumenta)->pluck('racun_id');
            $pdv_na_avans = number_format(array_map('current',DB::select("SELECT sum (ROUND(iznos_poreza, 2)) from racun_porez where racun_id = ".$referentni_racun_id.""))[0],2, ',', '.');
            

            $amount_avans_change = $racun->iznos - DB::table('racun')->where('broj_dokumenta',$racun->referentni_broj_dokumenta)->pluck('iznos') - array_map('current',DB::select("SELECT sum(iznos) from racun_vrsta_placanja where racun_id = ".$racun->racun_id.""))[0];
            
            if($amount_avans_change  > 0) {
              $text_avans_change = 'Преостало за плаћање:';
            } else {
              $text_avans_change = 'Повраћај:';
            }

            $amount_avans_change = number_format(abs($amount_avans_change),2, ',', '.');

            $text_prikaz_parrent[1] = $text_prikaz_parrent[1]."\r\nПлаћено авансом:".str_repeat(" ",54-strlen('Плаћено авансом:')-strlen($placeno_avansom)).$placeno_avansom."\r\nПДВ на аванс:".str_repeat(" ",50-strlen('ПДВ на аванс:')-strlen($pdv_na_avans)).$pdv_na_avans;
            $text_prikaz[1] = implode("\r\n",$text_prikaz_parrent);

            $text_prikaz_parrent = explode("========================================",$text_prikaz[1]);
            if($text_avans_change != 'Повраћај:') {
              $text_prikaz_parrent[0] = $text_prikaz_parrent[0].$text_avans_change.str_repeat(" ",58-strlen($text_avans_change)-strlen($amount_avans_change)).$amount_avans_change."\r\n";
            } else {
              $text_prikaz_parrent[0] = $text_prikaz_parrent[0].$text_avans_change.str_repeat(" ",48-strlen($text_avans_change)-strlen($amount_avans_change)).$amount_avans_change."\r\n";
            }
            $text_prikaz[1] = implode("========================================",$text_prikaz_parrent);

            $text_prikaz_prvi_deo = implode("----------------------------------------",$text_prikaz);

          }
          $text_prikaz_drugi_deo = implode("\r\n",$text_prikaz_drugi_deo);
       ?>



		            <pre class="pre_bill_tag">{{ $text_prikaz_prvi_deo }}</pre>

		            <img style="max-width: 250px" src="{{ $qr_kod_gif }}">

		            @if($racun->tip_racuna == 'Copy' && $racun->vrsta_transakcije == 'Refund')
		              <br>
		              <pre class="pre_bill_tag">Потпис купца: _ _ _ _ _ _ _ _ _ _</pre>
		            @endif

		            <pre class="pre_bill_tag">{{ $text_prikaz_drugi_deo }}</pre>


		            @if($racun->tip_racuna == 'Normal' && $racun->vrsta_transakcije == 'Sale' && !empty($racun->referentni_broj_dokumenta) && DB::table('racun')->where(array('broj_dokumenta' => $racun->referentni_broj_dokumenta,'tip_racuna' => 'Advance','vrsta_transakcije' => 'Refund'))->first())

			            <?php
			              $number_advance= DB::table('racun')->where('broj_dokumenta',$racun->referentni_broj_dokumenta)->pluck('referentni_broj_dokumenta');
			              
			             $time_advance = DB::table('racun')->where(array('broj_dokumenta' => $number_advance))->pluck('datum_racuna'); 
			             ?>

		            <pre class="pre_bill_tag">*Последњи авансни рачун: <br> {{ $number_advance }} {{ date('d.m.Y.',strtotime($time_advance)) }}</pre>
		            @endif

	           </div>

	           <div class="columns medium-3 medium-offset-2 margin-h-10">
					<div> {{  AdminLanguage::transAdmin('Broj računa') }} </div>
				    <div class="success-saved text-left no-margin"> {{$racun->broj_dokumenta}} <i class="fa fa-check no-margin"> </i> </div>
				    <div> {{  AdminLanguage::transAdmin('Račun je proveren') }} </div>
				</div>
	        </div>
		  </section>

		</div>	<!-- END TABS CONTENT -->
	</div>

</section>