<section id="main-content">


	@if(Session::has('message'))
		<script>
			alertify.success('{{ Session::get('message') }}');
		</script>
	@endif
	@if(Session::has('error_message'))
		<script>
			alertify.error('{{ Session::get('error_message') }}');
		</script>
	@endif

	@include('admin/partials/tabs')
	
	<div class="row">
		<section class="medium-3 columns">
			<div class="flat-box">	
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Način plaćanja') }} <i class="fa fa-credit-card"></i></h3>
				<div class="row"> 
					<div class="columns medium-12 after-select-margin"> 
						<select name="" id="JSnacinPlacanja">
							<option data-id="0">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
							@foreach($nacini_placanja as $row)
							<option data-id="{{ $row->web_nacin_placanja_id }}" @if($row->web_nacin_placanja_id == $web_nacin_placanja_id) selected @endif>{{ $row->naziv }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</section>

		<section class="medium-5 columns">
			<div class="flat-box">
				<form action="{{ AdminOptions::base_url() }}admin/nacin_placanja/{{$web_nacin_placanja_id}}" method="POST">
					<h3 class="title-med">{{ AdminLanguage::transAdmin('Izmeni') }}</h3>
					
					<div class="row"> 
						<div class="columns medium-12 field-group {{ $errors->first('naziv') ? ' error' : '' }}">
							<input type="text" name="naziv" value="{{ $nacin_placanja->naziv }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns"> 
							<label class="inline-block"> 
								<input type="checkbox" name="aktivno" @if($nacin_placanja->selected == 1) : checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('Aktivno') }}
							</label>
							
							&nbsp;
							
							@if(AdminOptions::checkB2C())
							<label class="inline-block"> 
								<input type="checkbox" name="b2c_default" @if($nacin_placanja->b2c_default == 1) : checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('B2C Default') }}
							</label>
							@endif
							
							&nbsp;
							
							@if(AdminOptions::checkB2B())
							<label class="inline-block"> 
								<input type="checkbox" name="b2b_default" @if($nacin_placanja->b2b_default == 1) : checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ AdminLanguage::transAdmin('B2B Default') }}
							</label>
							@endif
						</div>

						@if(AdminOptions::gnrl_options(3065))
						<div class="columns medium-6">
						<label class="text-center">{{ AdminLanguage::transAdmin('Veza sa fiskalnom kasom') }}</label>
							<select name="racun_vrsta_placanja_naziv_id">
								<option value=""></option>
									<option value="0" {{ AdminNacinPlacanja::getFisalValue($web_nacin_placanja_id) == '0' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Drugo') }}</option>

									<option value="1" {{ AdminNacinPlacanja::getFisalValue($web_nacin_placanja_id) == '1' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Gotovina') }}</option>

									<option value="2" {{ AdminNacinPlacanja::getFisalValue($web_nacin_placanja_id) == '2' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Platna kartica') }}</option>

									<option value="3" {{ AdminNacinPlacanja::getFisalValue($web_nacin_placanja_id) == '3' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Ček') }}</option>

									<option value="4" {{ AdminNacinPlacanja::getFisalValue($web_nacin_placanja_id) == '4' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Prenos na račun') }}</option>

									<option value="5" {{ AdminNacinPlacanja::getFisalValue($web_nacin_placanja_id) == '5' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Vaučer') }}</option>

									<option value="6" {{ AdminNacinPlacanja::getFisalValue($web_nacin_placanja_id) == '6' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Instant plaćanje') }}</option>
							</select>
						</div>
						@endif

					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
					<div class="btn-container text-center"> 
						<button type="submit" class="btn btn-primary save-it-btn" class="btn btn-primary">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div>
					@endif

				</form>

				@if($web_nacin_placanja_id != 3)
				<form action="{{ AdminOptions::base_url() }}admin/nacin_placanja/{{$web_nacin_placanja_id}}/delete" method="POST">
					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
					<div class="row"> 
						<div class="columns medium-12 text-center">
							<input type="submit" value="Obriši" class="btn btn-danger">
						</div>
					</div>
					@endif
				</form>
				@endif

			</div>
		</section>
	</div> 
</section>