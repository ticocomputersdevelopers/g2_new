<div class="polls-page" id="main-content">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')

	<div class="row">

		<div class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi anketu') }} </h3>
				<select class="JSeditSupport">
					<option value="{{ AdminOptions::base_url() }}admin/ankete/0">{{ AdminLanguage::transAdmin('Dodaj novu') }}</option>
					@foreach($ankete as $row)
					<option value="{{ AdminOptions::base_url() }}admin/ankete/{{ $row->anketa_id }}" @if($row->anketa_id == $anketa_id) {{ 'selected' }} @endif >{{ $row->naziv }}
					</option>
					@endforeach					
				</select>
			</div>

			<div class="text-center"> 
				<a href="#" class="video-manual" data-reveal-id="encoders-manual">{{ AdminLanguage::transAdmin('Uputstvo') }}<i class="fa fa-film"></i></a>
			</div>
		</div>


		<div class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ $title }}</h3>
				<form method="POST" action="{{ AdminOptions::base_url() }}admin/anketa-edit" enctype="multipart/form-data">
					<div class="row"> 
						<div class="columns"> 
							<input type="hidden" name="survey_id" value="{{ $anketa_id }}" />

							<div class="field-group {{ $errors->first('name') ? ' error' : '' }}">
								<label for="">{{ AdminLanguage::transAdmin('Naziv ankete') }}</label>
								<input type="text" name="name" data-id="" value="{{ htmlentities(Input::old('name') ? Input::old('name') : $anketa) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }} />
							</div>

							<label> 
								<input {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }} type="checkbox" name="flag_active" {{ ((Input::old('flag_active') ? 1 : $aktivan) == 1) ? 'checked' : '' }}>
								{{ AdminLanguage::transAdmin('Aktivna') }}
							</label>

							<label> 
								<input {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }} type="checkbox" name="default" {{ ((Input::old('default') ? 1 : $izabran) == 1) ? 'checked' : '' }}>
								{{ AdminLanguage::transAdmin('Izabrana') }}
							</label>

							<label> 
								<input {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }} type="checkbox" name="order_default" {{ ((Input::old('order_default') ? 1 : $narudzbina_izabran) == 1) ? 'checked' : '' }}>
								{{ AdminLanguage::transAdmin('Izabrana (narudžbina)') }}
							</label>

							@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
							<div class="btn-container center">
								<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>

								@if($anketa_id != 0)
								<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/anketa-delete/{{ $anketa_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
								@endif 
							</div>
							@endif
						</div>
					</div>
				</form>
			</div> 
		</div>

 
		@if($anketa_id != 0)
		<div class="medium-3 columns">
			<div class="flat-box">

				<h3 class="title-med">{{ AdminLanguage::transAdmin('Pitanje') }} 	 
					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
					- <button class="JSadd_question btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Dodaj/Izmeni') }}</button>
					@endif
				</h3> 

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/ankete/pitanje-save" enctype="multipart/form-data" class="JSnew_poll_form hide">
					<input type="hidden" name="survey_id" value="{{ $anketa_id }}">
					<input type="hidden" name="survey_question_id" value="{{ $anketa_pitanje_id }}">
					
					<div class="row"> 
						<div class="columns"> 
							<label>{{ AdminLanguage::transAdmin('Novo pitanje') }}</label>
							<input name="question" type="text"  autocomplete="off"> 
						</div>
					</div>

					<div class="field-group"> 
						<div class="row"> 
							<div class="medium-6 columns"> 
								<label>{{ AdminLanguage::transAdmin('Tip') }}</label>
								<select name = "type" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									<option value="checkbox" selected>{{ AdminLanguage::transAdmin('checkbox') }}</option>
									<option value="text" >{{ AdminLanguage::transAdmin('text') }}</option>	
								</select> 
							</div>

							<div class="medium-6 columns text-right"> 
								<br>	
								<label class="inline-block"> 
									<input type="checkbox" name="flag_active" value='1' @if($row->flag_aktivan == 1) : checked @endif {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}> 
 									{{ AdminLanguage::transAdmin('Aktivno') }}
 								</label>

								<button type="submit" class="button-option tooltipz"aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-check"></i></button>

							</div>
						</div>
					</div> 
				</form>	 

				<div id="JSPollQuestionSortable">
				@foreach($pitanja as $row)

					<div class="poll relative @if($row->anketa_pitanje_id == $anketa_pitanje_id) active @endif JSPollQuestionSort" data-id="{{ $row->anketa_pitanje_id }}">

						<a class="question" href="{{ AdminOptions::base_url() }}admin/ankete/{{ $row->anketa_id }}/{{ $row->anketa_pitanje_id }}">{{ $row->pitanje }}</a> 

						<div class="poll-options">  
							<!-- ADD -->
							@if($row->tip != 'text')
								<a href="{{ AdminOptions::base_url() }}admin/ankete/{{ $row->anketa_id }}/{{ $row->anketa_pitanje_id }}" class="tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Dodaj odgovor') }}"><i class="fa fa-plus-square"></i></a> 
							@endif

							<!-- EDIT -->
							<a class="JSeditpoll tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Izmeni') }}">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</a>

						 	<!-- REMOVE -->
							<a class="JSbtn-delete text-red tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-link="{{ AdminOptions::base_url() }}admin/pitanje-delete/{{ $anketa_pitanje_id }}">
								<i class="fa fa-times" aria-hidden="true"></i>
							</a>
						</div> 


						<form method="POST" action="{{ AdminOptions::base_url() }}admin/ankete/pitanje-edit" enctype="multipart/form-data" class="hide">
							
							<br>

							<input type="hidden" name="survey_id" value="{{ $anketa_id }}">
							<input type="hidden" name="survey_question_id" value="{{ $row->anketa_pitanje_id }}">
						 
							<input name="question" type="text" value="{{ htmlentities($row->pitanje) }}" data-id="{{ $row->anketa_pitanje_id }}" autocomplete="off" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						 
							<div class="field-group"> 
								<div class="row"> 
									<div class="medium-6 columns no-padd"> 
										<label>{{ AdminLanguage::transAdmin('Tip') }}</label>
										<select name="type" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
											<option value="checkbox" selected>{{ AdminLanguage::transAdmin('checkbox') }}</option>
											<option value="text" >{{ AdminLanguage::transAdmin('text') }}</option>	
										</select> 
									</div>

									<div class="medium-6 columns no-padd text-right"> 
										<br>	
										<label class="inline-block"> 
											<input {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }} type="checkbox" name="flag_active" value='1'@if($row->flag_aktivan == 1) : checked @endif>
										    {{ AdminLanguage::transAdmin('Aktivan') }}
										</label>
 
										<button type="submit" class="button-option tooltipz"aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-check"></i></button>
									</div>   
								</div>    
							</div> 
						</form>		
					</div> 
				@endforeach 
				</div>
			</div>
		</div>

		@endif

		<!-- ANSWERS -->


		@if(isset($anketa_pitanje_id) && $anketa_pitanje_id != 0  && $tip !='text')
		<div class="medium-3 columns">

			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Odgovor') }} 	 
					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
					- <button class="JSadd_question btn btn-primary btn-small">{{ AdminLanguage::transAdmin('Dodaj/Izmeni') }}</button>
					@endif
				</h3>


				<form class="JSnew_poll_form hide" method="POST" action="{{ AdminOptions::base_url() }}admin/ankete/odgovor-save/{{$anketa_pitanje_id}}" enctype="multipart/form-data" >

					<input type="hidden" name="survey_answer_id"  value="{{$anketa_odgovor_id}}">
					<input type="hidden" name="survey_question_id" value="{{$anketa_pitanje_id}}">

					<div class="row">
						<div class="columns">
							<label>{{ AdminLanguage::transAdmin('Novi odgovor') }}</label>
							<input name="answer" type="text" value="" autocomplete="off"> 
						</div>
					</div>

					<div class="row"> 
						<div class="medium-6 columns">
							<label class="inline-block">
								<input {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }} type="checkbox" name="flag_active" value="1">
								{{ AdminLanguage::transAdmin('Aktivan') }}
							</label>
						</div>

						<div class="medium-6 columns text-right"> 
							<button type="submit" class="button-option tooltipz"aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-check"></i></button>
						</div>
					</div> 
				</form>		
 
				<div id="JSPollAnswerSortable">
					@foreach($odgovori as $row)
					<div id="{{ $row->anketa_pitanje_id }}" class="poll JSPollAnswerSort" data-id="{{ $row->anketa_odgovor_id }}">

						<form method="POST" action="{{ AdminOptions::base_url() }}admin/ankete/odgovor-edit/{{$anketa_odgovor_id}}" enctype="multipart/form-data" >

							<input type="hidden" name="survey_answer_id"  value="{{$row->anketa_odgovor_id}}">
							<input type="hidden" name="survey_question_id" value="{{$row->anketa_pitanje_id}}">
							
						 
							<input name="answer" type="text" value="{{$row->odgovor}}" autocomplete="off"> 
						 
							<div class="row"> 
								<div class="medium-6 columns no-padd">
									<label class="inline-block">
										<input class="vert-align" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }} value="1" type="checkbox" name="flag_active" @if($row->flag_aktivan == 1) : checked @endif> 
										
										{{ AdminLanguage::transAdmin('Aktivan') }}
									</label>
								</div>
		 
		 						<div class="medium-6 columns no-padd text-right">
									<button type="submit" class="button-option tooltipz"aria-label="{{ AdminLanguage::transAdmin('Sačuvaj') }}"><i class="fa fa-check"></i></button>
									 
									<a class="JSbtn-delete text-red tooltipz inline-block" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-link="{{ AdminOptions::base_url() }}admin/odgovor-delete/{{$row->anketa_odgovor_id}}">
										<i class="fa fa-times" aria-hidden="true"></i>
									</a>
								</div>
							</div>
						</form>		 
					</div>
					@endforeach
				</div> 
			</div> 
		</div> 
		@endif
	</div>
</div>