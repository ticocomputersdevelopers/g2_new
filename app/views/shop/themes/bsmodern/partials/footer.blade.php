<?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

<!-- FOOTER.blade -->
 <footer {{ Options::web_options(322, 'str_data') != '' ? 'style=background-color:' . Options::web_options(322, 'str_data') : '' }}>
 	<div class="container{{(Options::web_options(322)==1) ? '-fluid' : '' }}">
 		<div class="row JSfooter-cols">  
 			@foreach(All::footer_sections() as $footer_section)
 			@if($footer_section->naziv == 'slika')
 			<div class="col-md-4 col-sm-4 col-xs-12">		 
				@if(!is_null($footer_section->slika))
				<a href="{{ $footer_section->link }}" rel="nofollow" class="hidden">
					<img class="footer-logo img-responsive" src="{{ Options::domain() }}{{ $footer_section->slika }}" alt="{{Options::company_name()}}" />
				</a>
				<h4 class="text-gold text-uppercase text-bold">G-2 D.O.O.</h4>
				@else
				<a href="/" title="{{Options::company_name()}}" rel="nofollow" class="hidden">
					<img class="footer-logo img-responsive" src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
				</a>
				<h4 class="text-gold text-uppercase text-bold">G-2 D.O.O.</h4>
				@endif

				<div class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
					{{ $footer_section->sadrzaj }} 
				</div> 
 			</div>
 			@elseif($footer_section->naziv == 'text')
 			<div class="col-md-4 col-sm-3 col-xs-12"> 
				<h5 class="ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
					{{ $footer_section->naslov }}
				</h5>

				<div class="JSInlineFull" data-target='{"action":"footer_section_content","id":"{{$footer_section->futer_sekcija_id}}"}'>
					{{ $footer_section->sadrzaj }} 
				</div> 
 			</div>
 			@elseif($footer_section->naziv == 'linkovi')
 			<div class="col-md-4 col-sm-3 col-xs-12"> 
 				<h5 class="ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
 					{{ $footer_section->naslov }}
 				</h5>

 				<ul class="footer-links">
 					@foreach(All::footer_section_pages($footer_section->futer_sekcija_id) as $page)
 					<li>
 						<a href="{{ Options::base_url().Url_mod::page_slug($page->naziv_stranice)->slug }}">{{ Url_mod::page_slug($page->naziv_stranice)->naziv }}</a>
 					</li>
 					@endforeach
 				</ul>  
 			</div>
 			@elseif($footer_section->naziv == 'kontakt')
 			<div class="col-md-4 col-sm-4 col-xs-12"> 
 				<h5 class="ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
 					{{ $footer_section->naslov }}
 				</h5>

 				<ul>
 					@if(Options::company_adress() OR Options::company_city())
 					<li> 
 						{{ Options::company_adress() }} 
 						{{ Options::company_city() }}
 					</li>
 					@endif
 					@if(Options::company_phone())
 					<li>
 						<a href="tel:{{ Options::company_phone() }}">
 							<i class="fas fa-phone"></i> {{ Options::company_phone() }}
 						</a>
 					</li>
 					@endif
 					@if(Options::company_phone())
 					<li>
 						<a href="tel:{{ Options::company_fax() }}">
 						<i class="fas fa-fax"></i>	{{ Options::company_fax() }}
 						</a>
 					</li>
 					@endif
 					@if(Options::company_email())
 					<li>
					 <i class="fas fa-envelope"></i> <a href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
 					</li>
 					@endif
 				</ul>
 			</div>
 			@elseif($footer_section->naziv == 'drustvene_mreze')
 			<div class="col-md-3 col-sm-3 col-xs-12"> 
				<h5 class="ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
					{{ $footer_section->naslov }}
				</h5>
				<div class="social-icons">
					{{Options::social_icon()}}
				</div>  
 			</div>	
 			@elseif($footer_section->naziv == 'mapa' AND Options::company_map() != '' AND Options::company_map() != ';')
 			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="map-frame relative">

					<div class="map-info">
						<h5>{{ Options::company_name() }}</h5>
						<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
					</div>
					<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

				</div> 
 			</div>					
 			@endif
 			@endforeach 

			 <!-- Prodajni program -->
			  <!-- CATEGORIES LEVEL 1 -->

			    <div class="col-md-4 col-sm-4 col-xs-12">
					<h5 class="ft-section-title JSInlineShort" data-target='{"action":"footer_section_label","id":"{{$footer_section->futer_sekcija_id}}"}'>
						Prodajni program
					</h5>
				
			      <ul class="footer-level-1">
                        @if(Options::category_type()==1) 
                        @foreach ($query_category_first as $row1)
                        @if(Groups::broj_cerki($row1->grupa_pr_id) >0)

                        <li>
                           <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}" class="">
                                @if(Groups::check_image_grupa($row1->putanja_slika))
                                <span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
                                    <img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
                                </span>
                                @endif
								<i class="fas fa-chevron-right"></i> {{ Language::trans($row1->grupa)  }} 
                            </a>

                            <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                            <ul class="JSlevel-2 row">
                                @foreach ($query_category_second->get() as $row2)
                                <li class="col-md-6 col-sm-12 col-xs-12">  
            
                                    <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
                                        @if(Groups::check_image_grupa($row2->putanja_slika))
                                        <span class="lvl-2-img-cont inline-block hidden-sm hidden-xs">
                                            <img src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
                                        </span>
                                        @endif
                                        
                                        {{ Language::trans($row2->grupa) }}
                                        
                                    </a>
                    
                                    @if(Groups::broj_cerki($row2->grupa_pr_id) >0)
                                    <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                    
                                    <ul class="JSlevel-3">
                                        @foreach($query_category_third as $row3)
                                        
                                        <li>						 
                                            <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}</a>

            <!-- 								@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
                                            <span class="JSCategoryLinkExpend hidden-sm hidden-xs">
                                                <i class="fas fa-chevron-down" aria-hidden="true"></i>
                                            </span>
                                            @endif   -->

                                            @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
                                            <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

                                            <ul class="JSlevel-4">
                                                @foreach($query_category_forth as $row4)
                                                <li>
                                                    <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
                                                </li>
                                                @endforeach
                                            </ul>
                                            @endif		
                                        </li>					 	
                                        @endforeach
                                    </ul>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        </li>

                        @else

                        <li>		 
                            <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">		 
                                @if(Groups::check_image_grupa($row1->putanja_slika))
                                <span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
                                    <img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
                                </span>
                                @endif
                                {{ Language::trans($row1->grupa)  }}  				
                            </a>			 
                        </li>
                        @endif
                        @endforeach
                        @else
                        @foreach ($query_category_first as $row1)
                        

                        @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
                        <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa)  }}</a>
                        <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

            
                        @foreach ($query_category_second->get() as $row2)
                        <li>
                            <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
                                {{ Language::trans($row2->grupa) }}
                            </a>
                            @if(Groups::broj_cerki($row2->grupa_pr_id) >0)
                            <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                            <ul>
                                @foreach($query_category_third as $row3)
                                <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}
                                </a>
                                @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
                                <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

                                <ul>
                                    @foreach($query_category_forth as $row4)
                                    <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
                                    @endforeach
                                </ul>
                                @endif	
                                @endforeach
                            </ul>
                            @endif
                        </li>
                        @endforeach

                        @else
                            <li>
                                <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa) }}</a>
                            </li>
                        @endif 
                        @endforeach				
                        @endif

                        @if(Options::all_category()==1)
                        <li>
                            <a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sve-kategorije') }}">{{ Language::trans('Sve kategorije') }}</a>
                        </li>
                        @endif

                    </ul>			
				</div>		

			 <!-- End of prodajni program -->

 			<?php $newslatter_description = All::newslatter_description(); ?>
 			@if(Options::newsletter()==1)
 			<div class="col-md-3 col-sm-3 col-xs-12 hidden">
 				<h5 class="ft-section-title JSInlineShort" data-target='{"action":"newslatter_label"}'>
 					{{ $newslatter_description->naslov }}
 				</h5> 
 				<p class="JSInlineFull" data-target='{"action":"newslatter_content"}'>
 					{{ $newslatter_description->sadrzaj }}
 				</p>

 				<div class="newsletter relative">		 
 					<input type="text" placeholder="E-mail" id="newsletter" />
 			 
 					<button onclick="newsletter()" class="button">{{ Language::trans('Prijavi se') }}</button>
 				</div>
 			</div>
 			@endif 	 	 
 		</div> 

 		<br> 

 		<div class="row"> 
			<div class="text-center foot-note col-xs-12">
			 
				<div class="JSInlineFull" data-target='{"action":"front_admin_content","id":4}'>
					{{ Support::front_admin_content(4) }}
				</div>

			</div> 
		</div>
		
 	</div>

	 <!-- Below footer -->
	 <div class="below-footer">
			<div class="row">
			  <div class="container flex">
				<div class="col-md-6 col-md-12 col-xs-12">
					<p class="silver-text">{{ Options::company_name() }} &copy; {{ date('Y') }}. {{Language::trans('Sva prava zadržana')}}. - 
						<a href="https://www.selltico.com/">{{Language::trans('Izrada internet prodavnice')}}</a> - 
						<a href="https://www.selltico.com/"> Selltico. </a>
					</p>
				</div>
				<div class="col-md-6 col-md-12 col-xs-12 text-right sm-text-center">
					<div class="social-icons below-footer-icons">
						{{Options::social_icon()}}
					</div>  		
				</div>
				</div>
			</div>
	 	</div>



 <span class="JSscroll-top"><i class="fas fa-angle-up"></i></span>
 
</footer>

@if(Support::banca_intesa()) 
<div class="after-footer"> 
	<div class="container"> 
		<div class="banks flex {{(empty(Options::gnrl_options(3023,'str_data')) && empty(Options::gnrl_options(3024,'str_data'))) ? 'justify-between' : 'justify-center' }}">
 			<ul class="list-inline sm-text-center text-right">
 				<li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/master-card.png"></li>
 				<li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/maestro-card.png"></li>
 				<li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/visa-card.png"></li> 

				@if(!empty(Options::gnrl_options(3023,'str_data')) && !empty(Options::gnrl_options(3024,'str_data')))
	 			<li>
					<img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/dinacard.png">
	 			</li>
 				@endif			 				
 				
 				<li><img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/american-express.png"></li>
 			</ul>
 	
 			<ul class="list-inline sm-text-center">
 				<li>
 					@if(!empty(Options::gnrl_options(3023,'str_data')) && !empty(Options::gnrl_options(3024,'str_data')))
 					<a href="https://www.bancaintesa.rs" class="banc_main_logo" target="_blank" rel="nofollow">
 						<img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/banca-intesa.png">
	 				</a>
	 				@else
	 				<a href="http://www.e-services.rs/" target="_blank" rel="nofollow">
 						<img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/chipCard.jpg">
	 				</a> 
	 				@endif
	 			</li> 
 				<li>
 					<a href="https://rs.visa.com/pay-with-visa/security-and-assistance/protected-everywhere.html" target="_blank" rel="nofollow">
 						<img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/verified-by-visa.jpg">
	 				</a>
	 			</li>
 				<li>
 					<a href="https://www.mastercard.rs/sr-rs/consumers/find-card-products/credit-cards.html" target="_blank" rel="nofollow">
	 					<img alt="bank-logo" class="JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}images/cards/master-card-secure.gif">
	 				</a>
	 			</li>
 			</ul>
	 	</div>
	</div>
</div> 
@endif  
<!-- FOOTER.blade END -->

<!-- COOKIES -->
@if(Options::web_options(323) == 1)
<div class="JScookies-part">
	<div id="alertCookiePolicy" class="alert-cookie-policy">
	  	<div class="alert alert-secondary margin-auto clearfix" role="alert">
		  	<h4 class="text-bold"> {{ Language::trans('Upotreba kolačića') }} (eng. Cookies) </h4>
		    <div> 
		    	{{ Language::trans('Ovaj web sajt koristi kolačiće u cilju unapređenja pretrage, analize saobraćaja, personalizacije sadržaja i ciljanog marketinga, da pruži bolje korisničko iskustvo. Ako nastavite da koristite naše web stranice, saglasni ste sa korišćenjem naših kolačića.') }}  
		    	<!-- <a href="#" class="alert-link">{{ Language::trans('Saznaj više o kolačićima') }}</a> -->
			</div>
		    <button id="btnDeclineCookiePolicy" class="button" data-dismiss="alert" type="button" aria-label="Close">{{ Language::trans('Samo neophodni') }}</button>
		    <button id="btnAcceptCookiePolicy" class="button" data-dismiss="alert" type="button" aria-label="Close"> {{ Language::trans('Dozvoli sve') }} </button>
		    <div class="JScookiesInfo_btn inline-block relative"> <!-- CONTENT FROM JS --> </div>

		    <div class="row cookies_tabs">
			    <div class="col-md-3 col-sm-3 col-xs-4 no-padding">
				    <ul class="nav nav-tabs tab-titles">
			            <li class="active"> <a data-toggle="tab" href="#cookies-necessary" rel="nofollow" class="no-margin">{{Language::trans('Neophodni')}}</a> </li>
			            <li> <a data-toggle="tab" href="#cookies-permanent" rel="nofollow">{{Language::trans('Trajni')}}</a> </li>
			            <li> <a data-toggle="tab" href="#cookies-statistic" rel="nofollow">{{Language::trans('Statistika')}}</a> </li>
			            <li> <a data-toggle="tab" href="#cookies-marketing" rel="nofollow">{{Language::trans('Marketing')}}</a> </li>
			        </ul>
		    	</div>

		    	<div class="col-md-9 col-sm-9 col-xs-8">
		          <div class="tab-content"> 
		            <div id="cookies-necessary" class="tab-pane fade in active">
		            	{{Language::trans('Neophodni kolačići pomažu da sajt bude upotrebljiv omogućavajući osnovne funkcije kao što su navigacija na stranici i pristup bezbednim oblastima web sajta. Web sajt ne može da funkcioniše pravilno bez ovih kolačića.')}}
		            </div>
		            <div id="cookies-permanent" class="tab-pane fade">
			         	{{Language::trans('Trajne kolačiće koristimo za olakšanu funkcionalnost prijave korisnika, kao što je “Ostanite prijavljeni”. Takođe koristimo trajne kolačiće da bismo bolje razumeli navike korisnika, te kako bismo poboljšali web stranicu prema vašim navikama.')}}
			        </div>
			        <div id="cookies-statistic" class="tab-pane fade">
			         	{{Language::trans('Statistički kolačići pomažu vlasnicima web sajta da razumeju interakciju posetilaca sa web sajtom anonimnim sakupljanjem informacija i izveštavanjem.')}}
			        </div>
			        <div id="cookies-marketing" class="tab-pane fade">
			         	{{Language::trans('Marketing kolačići se koriste za praćenje posetioca na web sajtovima. Namera je da se prikažu reklame koje su relevantne i privlačne za pojedinačnog korisnika a time i od veće vrednosti za izdavače i treće strane oglašivače.')}}
			        </div>
			      
		          </div>
		        </div>
		    </div>
	 	</div>  
	</div>
</div>
@endif


