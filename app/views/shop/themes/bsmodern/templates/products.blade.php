<!DOCTYPE html>
<html lang="sr">
<head>
	@include('shop/themes/'.Support::theme_path().'partials/head')

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "WebPage",
			"name": "<?php echo $title; ?>",
			"description": "<?php echo $description; ?>",
			"url" : "<?php echo Options::base_url().$url; ?>",
			"publisher": {
			"@type": "Organization",
			"name": "<?php echo Options::company_name(); ?>"
		}
	}
</script>
</head>
<body id="product-page" 
@if($strana == All::get_page_start()) id="start-page"  @endif 
@if(Support::getBGimg() != null) 
style="background-image: url({{Options::domain()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
@endif
>


@include('shop/themes/'.Support::theme_path().'partials/menu_top')
 
@include('shop/themes/'.Support::theme_path().'partials/header')

<!-- PRODUCTS.blade -->
<main class="d-content JSmain relative"> 
	<div class="container"> 
		<div class="row flex-on-small">  
			
			@if($strana!='akcija' and $strana!='tip')
			<ul class="breadcrumb">
				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
				{{ Url_mod::breadcrumbs2()}}
				@elseif($strana == 'proizvodjac')
				<li>
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('brendovi') }}">{{ Language::trans('Brendovi') }}</a>  
					@if($grupa)
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac') }}/{{ Url_mod::slug_trans($proizvodjac) }}">{{ All::get_manofacture_name($proizvodjac_id) }}</a>
					{{ Groups::get_grupa_title($grupa) }}
					@else
					{{ All::get_manofacture_name($proizvodjac_id) }} 
					@endif
				</li>
				@else					
				<li><a href="{{ Options::base_url() }}">{{ All::get_title_page_start() }}</a></li>
				<li>{{$title}}</li>
				@endif
			</ul>
			@endif   

			<div class="col-md-3 col-sm-12 col-xs-12 no-padding order-1-sm">  

				@if($filter_prikazi == 0)
					@if($strana == 'proizvodjac')
						@include('shop/themes/'.Support::theme_path().'partials/categories/manufacturer_category')
					@elseif($strana == 'tip')
						@include('shop/themes/'.Support::theme_path().'partials/categories/tip_category')
					@elseif($strana == 'akcija')
						@include('shop/themes/'.Support::theme_path().'partials/categories/akcija_category')
					@endif
				@endif

				@if(Options::enable_filters()==1 AND $filter_prikazi)
					@include('shop/themes/'.Support::theme_path().'partials/products/filters')
				@endif

				<!-- Categories -->
				<div class="sideCategories">		
					  <div class="content-box">
						<div class="JScategories relative"> 
							<?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
							
							<h4 class="categories-title text-center text-white">{{ Language::trans('Kategorije') }}</h4> 

							<!-- CATEGORIES LEVEL 1 -->
							<ul class="products-categories">
								@if(Options::category_type()==1) 
								@foreach ($query_category_first as $row1)
								@if(Groups::broj_cerki($row1->grupa_pr_id) >0)

								<li>
									<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}" class="">
										@if(Groups::check_image_grupa($row1->putanja_slika))
										<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
											<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
										</span>
										@endif
										{{ Language::trans($row1->grupa)  }} 
									</a>

									<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

									<ul class="products-categories-2">
										@foreach ($query_category_second->get() as $row2)
										<li class="col-md-12 col-sm-12 col-xs-12">  

											<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
												@if(Groups::check_image_grupa($row2->putanja_slika))
												<span class="lvl-2-img-cont inline-block hidden-sm hidden-xs">
													<img src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
												</span>
												@endif
												
												{{ Language::trans($row2->grupa) }}
												
											</a>
							
											@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
											<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
											
											<ul class="JSlevel-3">
												@foreach($query_category_third as $row3)
												
												<li>						 
													<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}</a>

													<!-- @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
													<span class="JSCategoryLinkExpend hidden-sm hidden-xs">
														<i class="fas fa-chevron-down" aria-hidden="true"></i>
													</span>
													@endif   -->

													@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
													<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

													<ul class="JSlevel-4">
														@foreach($query_category_forth as $row4)
														<li>
															<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
														</li>
														@endforeach
													</ul>
													@endif		
												</li>					 	
												@endforeach
											</ul>
											@endif
										</li>
										@endforeach
									</ul>
								</li>

								@else

								<li>		 
									<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">		 
										@if(Groups::check_image_grupa($row1->putanja_slika))
										<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
											<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
										</span>
										@endif
										{{ Language::trans($row1->grupa)  }}  				
									</a>			 
								</li>
								@endif
								@endforeach
								@else
								@foreach ($query_category_first as $row1)
								

								@if(Groups::broj_cerki($row1->grupa_pr_id) >0)
								<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa)  }}</a>
								<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>


								@foreach ($query_category_second->get() as $row2)
								<li>
									<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
										{{ Language::trans($row2->grupa) }}
									</a>
									@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
									<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
									<ul>
										@foreach($query_category_third as $row3)
										<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}
										</a>
										@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
										<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

										<ul>
											@foreach($query_category_forth as $row4)
											<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
											@endforeach
										</ul>
										@endif	
										@endforeach
									</ul>
									@endif
								</li>
								@endforeach

								@else
									<li>
										<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa) }}</a>
									</li>
								@endif 
								@endforeach				
								@endif

								@if(Options::all_category()==1)
								<li>
									<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sve-kategorije') }}">{{ Language::trans('Sve kategorije') }}</a>
								</li>
								@endif
							</ul>
						</div>
					
				 </div>
	        	</div>
				<!-- End of Categories -->
				
				<div class="hiddne-sm hidden-xs">&nbsp; <!-- EMPTY SPACE --></div>
			</div>

 			<div class="col-md-9 col-sm-12 col-xs-12 product-page">
				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac' and $strana != 'tagovi')
					@if(isset($sub_cats) and !empty($sub_cats))
					<div class="row sub-group">

						@foreach($sub_cats as $row)
						<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
							<a class="flex" href="{{ Options::base_url()}}{{ $url }}/{{ Url_mod::slug_trans($row->grupa) }}">
								
								@if(isset($row->putanja_slika))
									<img src="{{ Options::domain() }}{{ $row->putanja_slika }}" alt="{{ $row->grupa }}" class=" img-responsive" />
						 		@endif
								
								<span>{{ $row->grupa }}</span>
							</a>
						</div>
						@endforeach

					</div>
					@endif
				@endif

				@yield('products_list')

			</div>

		</div>
	</div>
</main>
<!-- PRODUCTS.blade END -->


@include('shop/themes/'.Support::theme_path().'partials/footer')
 
@include('shop/themes/'.Support::theme_path().'partials/popups')


<!-- BASE REFACTORING -->
<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
<input type="hidden" id="in_stock" value="{{Options::vodjenje_lagera()}}" />
<input type="hidden" id="elasticsearch" value="{{ Options::gnrl_options(3055) }}" />

<!-- js includes -->
@if(Session::has('b2c_admin'.Options::server()) OR (Options::enable_filters()==1 AND $filter_prikazi))
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@endif
@if(Session::has('b2c_admin'.Options::server()))
<script src="{{Options::domain()}}js/tinymce_5.1.3/tinymce.min.js"></script> 
<script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
@endif

<script src="{{Options::domain()}}js/shop/translator.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>

@if(Options::header_type()==1)
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
@endif
</body>
</html>