@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<br><br>
		<h3>{{ Language::trans('Nakon potvrde vrši se preusmeravanje na sajt Banke, gde se na zaštićenoj stranici realizuje proces plaćanja') }}.</h3></br>

		<h4>{{ Language::trans('Podaci o korisniku') }}:</h4>
		<ul>
			@if($web_kupac->flag_vrsta_kupca == 1)
				<li>{{ Language::trans('Firma') }}: {{ $web_kupac->naziv }}</li>
			@else
				<li>{{ Language::trans('Ime') }}: {{ $web_kupac->ime.' '.$web_kupac->prezime }}</li>
			@endif
			<li>{{ Language::trans('Adresa') }}: {{ $web_kupac->adresa }}, {{ $web_kupac->mesto }}</li>
		</ul>
		</br>
		<h4>{{ Language::trans('Podaci o trgovcu') }}:</h4>
		<ul>
			<li>{{ $preduzece->naziv }}</li>
			<li>{{ Language::trans('Kontakt osoba') }}: {{ $preduzece->kontakt_osoba }}</li>
			@if($preduzece->matbr_registra != '')
			<li>{{ Language::trans('MBR') }}: {{ $preduzece->matbr_registra }}</li>
			@endif
			@if($preduzece->pib != '')
			<li>VAT Reg No: {{ $preduzece->pib }}</li>
			@endif
			<li>{{ $preduzece->adresa }}, {{ $preduzece->mesto }}</li>
			<li>{{ Language::trans('Telefon') }}: {{ $preduzece->telefon }}</li>
			<li><a href="mailto:{{ $preduzece->email }}">{{ $preduzece->email }}</a></li>
		</ul> 
		</br>

		<input type="checkbox" id="JSConditions" onclick="clickCheckbox()"> <a href="{{ Options::base_url() }}banka-uslovi" target="_blank">
		{{ Language::trans('Uslovi korišćenja') }}</a>

		<form method="GET" action="{{ $link }}">
	 		<button type="submit" id="JSIntesaSubmit" disabled>{{ Language::trans('Završi plaćanje karticom') }}</button>			
		</form>
		<script type="text/javascript">
			function clickCheckbox(){
				if(document.getElementById("JSConditions").checked){
					document.getElementById("JSIntesaSubmit").removeAttribute('disabled');
				}else{
					document.getElementById("JSIntesaSubmit").setAttribute('disabled','disabled');
				}
			}
		</script>
	</div>
@endsection