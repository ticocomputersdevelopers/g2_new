<footer {{ Options::web_options(322, 'str_data') != '' ? 'style=background-color:' . Options::web_options(322, 'str_data') : '' }}>
	<div class="container{{(Options::web_options(322)==1) ? '-fluid' : '' }}"> 
		<div class="row"> 

			@foreach(All::footer_sections() as $footer_section)
				@if($footer_section->naziv == 'slika')
				<div class="col-md-3 col-sm-3 col-xs-12">  
					@if(!is_null($footer_section->slika))
					<a class="logo center-block" href="{{ $footer_section->link }}">
						 <img src="{{ Options::domain() }}{{ $footer_section->slika }}" alt="{{Options::company_name()}}" />
					</a>
					@else
					<a class="logo center-block" href="/" title="{{Options::company_name()}}">
						 <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" />
					</a>
					@endif 
					{{ $footer_section->sadrzaj }}  
				</div>

				@elseif($footer_section->naziv == 'text')

				<div class="col-md-3 col-sm-3 col-xs-12">  
					<h2 class="footer-title">{{ $footer_section->naslov }}</h2>  
					{{ $footer_section->sadrzaj }}   
				</div>

				@elseif($footer_section->naziv == 'linkovi')
				<div class="col-md-3 col-sm-3 col-xs-12"> 

					<h2 class="footer-title">{{ $footer_section->naslov }}</h2>

					<ul>
						@foreach(All::footer_section_pages($footer_section->futer_sekcija_id) as $page)
			                @if($page->grupa_pr_id != -1 and $page->grupa_pr_id != 0)
			                <li>
			                    <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans(Groups::getGrupa($page->grupa_pr_id)) }}">{{ Language::trans($page->title) }}</a>
			                </li>
			                
							@elseif($page->tip_artikla_id == -1)
							<li>
								<a href="{{ Options::base_url().Url_mod::page_slug($page->naziv_stranice)->slug }}">{{ Url_mod::page_slug($page->naziv_stranice)->naziv }}</a>
							</li>
				            @else

				            @if($page->tip_artikla_id==0)
			                <li>
			                	<a href="{{ Options::base_url().Url_mod::slug_trans('akcija') }}">{{ Language::trans($page->title) }}</a>
			                </li>
				            @else
			                <li>
			                	<a href="{{ Options::base_url().Url_mod::slug_trans('tip').'/'.Url_mod::slug_trans(Support::tip_naziv($page->tip_artikla_id)) }}">
			                    {{ Language::trans($page->title) }}
			                	</a> 
			                </li>
				            @endif

				        	@endif

						@endforeach
					</ul>   
				</div>

				@elseif($footer_section->naziv == 'kontakt')
				<div class="col-md-3 col-sm-3 col-xs-12"> 
					<h2 class="footer-title">{{ $footer_section->naslov }}</h2> 
					
					<ul> 
						<li>{{ Options::company_adress() }}	{{ Options::company_city() }}</li>
						<li>{{ Options::company_phone() }}</li> 
						<li><a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a></li>  
					</ul>
	
				 </div>

				@elseif($footer_section->naziv == 'drustvene_mreze')
				<div class="col-md-3 col-sm-3 col-xs-12">
			    	<h2 class="footer-title">{{ $footer_section->naslov }}</h2>
					
					<div class="social-icons">
						{{Options::social_icon()}} 
				 	</div>
				</div>	
	  			@elseif($footer_section->naziv == 'mapa' AND Options::company_map() != '' AND Options::company_map() != ';')
	 			<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="map-frame relative">

						<div class="map-info">
							<h5>{{ Options::company_name() }}</h5>
							<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
						</div>
						<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

					</div> 
	 			</div>						
				@endif 

			@endforeach 
		</div> 
	</div> 

	<br>
	
	<div class="below-footer text-center"> 
		<p class="no-margin">{{ Options::company_name() }} &copy; {{ date('Y') }}. {{Language::trans('Sva prava zadržana')}}. - <a href="https://www.selltico.com/">{{ Language::trans('Izrada web prezentacija') }}</a> - 
			<a href="https://www.selltico.com/"> Selltico. </a>
		</p> 
	</div>
 	<a class="JSscroll-top" href="javascript:void(0)" ><i class="fa fa-chevron-up"></i></a>

</footer>

<!-- COOKIES -->
@if(Options::web_options(323) == 1)
<div class="JScookies-part">
	<div id="alertCookiePolicy" class="alert-cookie-policy">
	  	<div class="alert alert-secondary margin-auto clearfix" role="alert">
		  	<h4 class="text-bold"> {{ Language::trans('Upotreba kolačića') }} (eng. Cookies) </h4>
		    <div> 
		    	{{ Language::trans('Ovaj web sajt koristi kolačiće u cilju unapređenja pretrage, analize saobraćaja, personalizacije sadržaja i ciljanog marketinga, da pruži bolje korisničko iskustvo. Ako nastavite da koristite naše web stranice, saglasni ste sa korišćenjem naših kolačića.') }}  
		    	<!-- <a href="#" class="alert-link">{{ Language::trans('Saznaj više o kolačićima') }}</a> -->
			</div>
		    <button id="btnDeclineCookiePolicy" class="button" data-dismiss="alert" type="button" aria-label="Close">{{ Language::trans('Samo neophodni') }}</button>
		    <button id="btnAcceptCookiePolicy" class="button" data-dismiss="alert" type="button" aria-label="Close"> {{ Language::trans('Dozvoli sve') }} </button>
		    <div class="JScookiesInfo_btn inline-block relative"> <!-- CONTENT FROM JS --> </div>

		    <div class="row cookies_tabs">
			    <div class="col-md-3 col-sm-3 col-xs-4 no-padding">
				    <ul class="nav nav-tabs tab-titles">
			            <li class="active"> <a data-toggle="tab" href="#cookies-necessary" rel="nofollow" class="no-margin">{{Language::trans('Neophodni')}}</a> </li>
			            <li> <a data-toggle="tab" href="#cookies-permanent" rel="nofollow">{{Language::trans('Trajni')}}</a> </li>
			            <li> <a data-toggle="tab" href="#cookies-statistic" rel="nofollow">{{Language::trans('Statistika')}}</a> </li>
			            <li> <a data-toggle="tab" href="#cookies-marketing" rel="nofollow">{{Language::trans('Marketing')}}</a> </li>
			        </ul>
		    	</div>

		    	<div class="col-md-9 col-sm-9 col-xs-8">
		          <div class="tab-content"> 
		            <div id="cookies-necessary" class="tab-pane fade in active">
		            	{{Language::trans('Neophodni kolačići pomažu da sajt bude upotrebljiv omogućavajući osnovne funkcije kao što su navigacija na stranici i pristup bezbednim oblastima web sajta. Web sajt ne može da funkcioniše pravilno bez ovih kolačića.')}}
		            </div>
		            <div id="cookies-permanent" class="tab-pane fade">
			         	{{Language::trans('Trajne kolačiće koristimo za olakšanu funkcionalnost prijave korisnika, kao što je “Ostanite prijavljeni”. Takođe koristimo trajne kolačiće da bismo bolje razumeli navike korisnika, te kako bismo poboljšali web stranicu prema vašim navikama.')}}
			        </div>
			        <div id="cookies-statistic" class="tab-pane fade">
			         	{{Language::trans('Statistički kolačići pomažu vlasnicima web sajta da razumeju interakciju posetilaca sa web sajtom anonimnim sakupljanjem informacija i izveštavanjem.')}}
			        </div>
			        <div id="cookies-marketing" class="tab-pane fade">
			         	{{Language::trans('Marketing kolačići se koriste za praćenje posetioca na web sajtovima. Namera je da se prikažu reklame koje su relevantne i privlačne za pojedinačnog korisnika a time i od veće vrednosti za izdavače i treće strane oglašivače.')}}
			        </div>
			      
		          </div>
		        </div>
		    </div>
	 	</div>  
	</div>
</div>
@endif
