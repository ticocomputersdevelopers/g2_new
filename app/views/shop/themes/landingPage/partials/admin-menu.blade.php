

@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
    <div id="admin-menu"> 
        <div class="container text-right">
            <div class="col-md-12">
                <span>
                    {{ Language::trans('Prijavljeni ste kao administrator') }} 
                </span>
                | 
                <span class="ms admin-links">
                    <a target="_blank" href="{{ Options::base_url() }}admin">
                        {{ Language::trans('Admin Panel') }}
                    </a>
                </span>
                |
                <span class="ms admin-links">
                    <a href="{{ Options::domain() }}admin-logout">
                        {{ Language::trans('Odjavi se') }}
                    </a>
                </span>
            </div>
        </div>
    </div>  
@endif