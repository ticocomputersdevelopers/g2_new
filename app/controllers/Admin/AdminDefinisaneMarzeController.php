<?php

class AdminDefinisaneMarzeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tr_isp = DB::table('definisane_marze')->orderBy('definisane_marze_id','asc')->get();
		$tr_count = count($tr_isp);

		$definisane_marze = array();
		if($tr_count>0){		
			for($i=0;$i<$tr_count;$i++){
				if($i==0){
					$definisane_marze[$i] = (object) array('definisane_marze_id'=>$tr_isp[$i]->definisane_marze_id,'nc_od'=>0,'nc_do'=>$tr_isp[$i]->nc,'web_marza'=>$tr_isp[$i]->web_marza, 'mp_marza' =>$tr_isp[$i]->mp_marza);
				}else{
					$definisane_marze[$i] = (object) array('definisane_marze_id'=>$tr_isp[$i]->definisane_marze_id,'nc_od'=>$tr_isp[$i-1]->nc,'nc_do'=>$tr_isp[$i]->nc,'web_marza'=>$tr_isp[$i]->web_marza, 'mp_marza'=>$tr_isp[$i]->mp_marza);
				}
			}
			$definisane_marze[$tr_count] = (object) array('definisane_marze_id'=>0,'nc_od'=>$tr_isp[$tr_count-1]->nc,'nc_do'=>'','web_marza'=>'', 'mp_marza'=>'');
		}else{
			$definisane_marze[] = (object) array('definisane_marze_id'=>0,'nc_od'=>0,'nc_do'=>'','web_marza'=>'', 'mp_marza'=>'');
		}

		$data = array(
	                'strana'=>'definisane_marze',
	                'title'=> 'Definisane marže',
	                'web_marza'=>DB::table('definisane_marze')->get(),
	                'mp_marza'=>DB::table('definisane_marze')->get(),
	                'definisane_marze'=>$definisane_marze
	            );

		return View::make('admin/page', $data);
	}


	public function save()	
	{
		$data = Input::get();
		$max_id = DB::table('definisane_marze')->max('definisane_marze_id');

		$validator_messages = array('required'=>'Polje ne sme biti prazno!',
									'numeric'=>'Polje sme da sadrži samo brojeve!',
									'digits_between'=>'Dužina unetih cifara je predugačka!',
									'min'=>'Minimalna vrednost polja je neodgovarajuća!',
									'max'=>'Prekoračili ste maksimalnu vrednost polja!'
								);

		$validator_rules = array(
        		'web_marza' => 'required|numeric|digits_between:0,10',
        		'mp_marza' => 'numeric|digits_between:0,10'
        	);
		
		if($data['definisane_marze_id']!=0 && $data['definisane_marze_id'] < $max_id){
			$validator_rules['nc'] = 'required|numeric|digits_between:0,20|min:'. DB::table('definisane_marze')->where('definisane_marze_id', $data['definisane_marze_id']-1)->pluck('nc');

		}elseif($data['definisane_marze_id'] == 0){
        	$validator_rules['nc'] = 'required|numeric|digits_between:0,20|min:'. DB::table('definisane_marze')->where('definisane_marze_id', $max_id)->pluck('nc');
		}

		$validator = Validator::make($data, $validator_rules, $validator_messages);		
		if($validator->fails()){
	       	return Redirect::to(AdminOptions::base_url().'admin/definisane-marze')->withInput()->withErrors($validator->messages());
        }

        $data['mp_marza'] = !empty($data['mp_marza']) ? $data['mp_marza'] : 0;

		if($data['definisane_marze_id']==0){
        	$data['definisane_marze_id'] = $max_id+1;
        	DB::table('definisane_marze')->insert(array('definisane_marze_id'=>$data['definisane_marze_id'],'nc'=>$data['nc'],'web_marza'=>$data['web_marza'], 'mp_marza'=>$data['mp_marza']));
        	AdminSupport::saveLog('SIFARNIK_DEFINISANE_MARZE_DODAJ', array(DB::table('definisane_marze')->max('definisane_marze_id')));
        }else{
        	DB::table('definisane_marze')->where('definisane_marze_id',$data['definisane_marze_id'])->update(array('nc'=>$data['nc'],'web_marza'=>$data['web_marza'], 'mp_marza'=>$data['mp_marza']));
        	AdminSupport::saveLog('SIFARNIK_DEFINISANE_MARZE_IZMENI', array($data['definisane_marze_id']));
        }
        return Redirect::to(AdminOptions::base_url().'admin/definisane-marze')->with('success',true);
	}


	public function delete($id) {
		AdminSupport::saveLog('SIFARNIK_DEFINISANE_MARZE_OBRISI', array($id));
		DB::table('definisane_marze')->where('definisane_marze_id',$id)->delete();
		return Redirect::to(AdminOptions::base_url().'admin/definisane-marze')->with('success-delete',true);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
