<?php
use Import\Support;

class AdminFilesController extends Controller {
    function index(){
        if(AdminOptions::gnrl_options(3002)==0){
        AdminSupport::saveLog('WEB_IMPORT_FILE_UPLOAD');
            return Redirect::to(AdminOptions::base_url()."admin/web_import/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/nn-nn");
        }
        $data=array(
            "strana"=>'file_upload',
            "title"=>'Aploadovanje fajla',
        );
        return View::make('admin/page', $data);    	
    }

    function save(){
        $file = Input::file('import_file');
        $podrzan_import_id = Input::get('podrzan_import_id');
        $first = DB::table('podrzan_import')->where('podrzan_import_id',$podrzan_import_id)->first();

        $status = false;
        if($podrzan_import_id != 0 && Input::hasFile('import_file') && $file->isValid()){
            $path_info = Support::import_file_path($first);
            if($file->getClientOriginalExtension() == $first->file_type){
                $file->move($path_info->path,$path_info->name);
                $status = true;
            }
            elseif($file->getClientOriginalExtension() == 'xls' && $first->file_type=='xlsx'){
                if(Support::xls_to_xlsx($file->getPathName(),$path_info->path.$path_info->name)){
                    $status = true;
                }
            }

        }

        if($status){
            return Redirect::to(AdminOptions::base_url().'admin/file-upload')->with('message','Fajl je uspesno aploadovan!');
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/file-upload')->with('alert','Proverite strukturu fajla za dati import!');
        }
    }
     function uploadDocumentDelete($file){
        AdminSupport::saveLog('UCITANE_SLIKE_OBRISI');
        File::delete('files/documents/'.$file);
        // return Redirect::to(AdminOptions::base_url().'admin/upload-image-article');
    }
    
    function uploadDocumentAdd(){
        $data = Input::get();
        $files = Input::file('files');

        foreach($files as $file){
                $originalName = $file->getClientOriginalName();
                $name = $originalName;
                        
                $file->move('files/documents/', $name);

               AdminSupport::saveLog('UCITANI_FILE_DODAJ');

            }        
    }

    public function filesContent(){
        $files = scandir('files/documents');
        unset($files[0]);
        unset($files[1]);

        $search = Input::get('search') && !empty(Input::get('search')) ? trim(Input::get('search')) : null;
        $page = Input::get('page') ? Input::get('page') : 1;
        $limit = 20;
        $offset = ($page-1)*$limit;

        if(!is_null($search)){
            $files = array_filter($files,function($file) use ($search){
                return strpos(strtolower($file),strtolower($search)) !== false;
            });
        }
        $maxPages = intdiv(count($files),$limit) + (fmod(count($files),$limit) > 0 ? 1 : 0);
        $files = array_slice($files,$offset,$limit);

        $content = View::make('admin/partials/file_document',array('files'=>$files))->render();

        return Response::json(['content'=>$content, 'max_page'=>$maxPages],200);
    }

}