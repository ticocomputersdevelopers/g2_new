<?php 
use Service\TranslatorService;

class AdminVestiController extends Controller {


	public function vesti($active=null){

		$data=array(
			"strana"=>'vesti',
			"title"=> 'Vesti',
			"vesti" => AdminVesti::getVesti($active),
			"count_aktivne" => DB::table('web_vest_b2c')->where('aktuelno', 1)->count(),
			"count_neaktivne" => DB::table('web_vest_b2c')->where('aktuelno', 0)->count(),
			"count_sve" => DB::table('web_vest_b2c')->count()

			);
		return View::make('admin/page', $data);
	}

	public function vest($id,$jezik_id=null){
        if(is_null($jezik_id)){
            $jezik_id = DB::table('jezik')->where(array('aktivan'=>1,'izabrani'=>1))->pluck('jezik_id');
        }
        $vesti_jezik = null;  
        if($id != 0){
            $vesti_jezik=DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$id, 'jezik_id'=>$jezik_id))->first();
        	$seo = AdminSeo::vest($id,$jezik_id);
        }
		$data=array(
			"strana" =>'vest',
			"title" => 'Vest',
			"web_vest_b2c_id" => $id,
			"naslov" => $vesti_jezik ? $vesti_jezik->naslov : '',
			"rbr" => $id ? AdminVesti::find($id, 'rbr') : DB::table('web_vest_b2c')->max('rbr')+1,
			"tekst" =>  $vesti_jezik ? $vesti_jezik->sadrzaj : '',
			"aktuelno" => $id ? AdminVesti::find($id, 'aktuelno') : 1,
			"b2b_aktuelno" => $id ? AdminVesti::find($id, 'b2b_aktuelno') : 0,
			"slika" => $id ? AdminVesti::find($id, 'slika') : AdminOptions::base_url().'images/no-image.jpg',
			"jezik_id"=>$jezik_id,
            "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
            "seo_title" => $id != 0 ? $seo->title : '',
            "keywords" => $id != 0 ? $seo->keywords : '',
            "description" => $id != 0 ? $seo->description : ''
		);

		return View::make('admin/page', $data);
	}

	public function saveNews(){
		$data = Input::get();
		$naslov = $data['naslov'];
		$rbr = $data['rbr'];
		$tekst = $data['tekst'];
		$slika_link = $data['news_img_link'];
		$id = $data['id'];
		$jezik_id = $data['jezik_id'];

		if(isset($data['aktuelno'])) {
			$aktuelno = 1;
		} else {
			$aktuelno = 0;
		}
		if(isset($data['b2b_aktuelno'])) {
			$b2b_aktuelno = 1;
		} else {
			$b2b_aktuelno = 0;
		}

		$messages = array(
    		'required' => 'Niste popunili polje!',
    		'regex' => 'Uneli ste neodgovarajuče karaktere, molimo Vas da ih izbrišete!',
    		'digits_between' => 'Unesite maksimalno 10 brojeva!',
    		'numeric'=>'Unesite brojeve!',
    		'unique' => 'Ovaj naslov već postoji, molimo Vas da unesete drugi naslov!'
		);

		$rules = array(
			'naslov' => 'required|regex:'.AdminSupport::regex().'|max:200|unique:web_vest_b2c_jezik,naslov,'.$id.',web_vest_b2c_id,jezik_id,'.$jezik_id,
            'seo_title' => 'max:60',
            'description' => 'max:158',
            'keywords' => 'max:158',
			'rbr' => 'numeric|digits_between:1,10',
			'news_img_link' => 'max:300'
			);
       	$validator = Validator::make($data, $rules, $messages);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/vest'.'/'.$id)->withInput()->withErrors($validator->messages());
		}else{
			if($rbr == ''){
				$rbr = null;
			}

			if($id == 0) {
				DB::table('web_vest_b2c')->insert(['rbr' => $rbr, 'datum' => date('Y-m-d'), 'aktuelno' => $aktuelno, 'b2b_aktuelno' => $b2b_aktuelno]);
				$id = DB::select("SELECT MAX(web_vest_b2c_id) FROM web_vest_b2c")[0]->max;
			} else {
				DB::table('web_vest_b2c')->where('web_vest_b2c_id', $id)->update(['rbr' => $rbr, 'aktuelno' => $aktuelno, 'b2b_aktuelno' => $b2b_aktuelno]);
			}
			
			if(Input::hasFile('news_img')){		
				$slika = Input::file('news_img');
				$extension = $slika->getClientOriginalExtension();
				$slika->move('images/vesti/',$id.'.'.$extension);
				$slika_link = AdminOptions::base_url().'images/vesti/'.$id.'.'.$extension;
			}
			DB::table('web_vest_b2c')->where('web_vest_b2c_id', $id)->update(['slika' => $slika_link]);

            $query = DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$id, 'jezik_id'=>$jezik_id));
            $logId = DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$id, 'jezik_id'=>$jezik_id))->pluck('web_vest_b2c_id');
            $jezik_data = array('naslov'=>$naslov,'sadrzaj'=>$tekst,'title'=>$data['seo_title'],'description'=>$data['description'],'keywords'=>$data['keywords']);

            if(!is_null($query->first())){
                $query->update($jezik_data);
				AdminSupport::saveLog('VESTI_IZMENI', array($logId));
            }else{
                $jezik_data['web_vest_b2c_id'] = $id;
                $jezik_data['jezik_id'] = $jezik_id;
                DB::table('web_vest_b2c_jezik')->insert($jezik_data);
	            if($jezik_id==1){
	                foreach(DB::table('jezik')->where('aktivan',1)->where('jezik_id','!=',1)->orderBy('izabrani','desc')->get() as $jezik){
	                    $translator = new TranslatorService(DB::table('jezik')->where('jezik_id',1)->pluck('kod'),$jezik->kod);
						$jezik_data = array('web_vest_b2c_id' => $id, 'jezik_id' => $jezik->jezik_id, 'naslov'=>$translator->translate($naslov),'sadrzaj'=>$translator->translate(html_entity_decode($tekst)),'title'=>$translator->translate($data['seo_title']),'description'=>$translator->translate($data['description']),'keywords'=>$translator->translate($data['keywords']));
	                    DB::table('web_vest_b2c_jezik')->insert($jezik_data);                 
	                }
	            }
				AdminSupport::saveLog('VESTI_DODAJ', array(DB::table('web_vest_b2c_jezik')->max('web_vest_b2c_id')));
            }
			return Redirect::to(AdminOptions::base_url().'admin/vest/' . $id.($jezik_id==1 ? '' : '/'.$jezik_id))->with('message', 'Uspešno ste sačuvali vest!');
		}
	}

	public function deleteNews($web_vest_b2c_id){
		AdminSupport::saveLog('VESTI_OBRISI', array($web_vest_b2c_id));
		DB::table('web_vest_b2c')->where('web_vest_b2c_id', $web_vest_b2c_id)->delete();
		Session::flash('message', 'Uspešno ste izbrisali vest!');
		
		return Redirect::to(AdminOptions::base_url().'admin/vesti');

	}

	public function filter($id) {
		if($id == 'aktivni') {
			$vesti = DB::table('web_vest_b2c')->where('aktuelno', 1)->paginate(20);
		} elseif($id == 'neaktivni') {
			$vesti = DB::table('web_vest_b2c')->where('aktuelno', 0)->orderBy('datum', 'DESC')->orderBy('naslov', 'ASC')->paginate(20);
		}
		

		$data=array(
			"strana"=>'vesti',
			"title"=> 'Vesti',
			"vesti" => $vesti,
			"count_aktivne" => DB::table('web_vest_b2c')->where('aktuelno', 1)->count(),
			"count_neaktivne" => DB::table('web_vest_b2c')->where('aktuelno', 0)->count(),
			"count_sve" => DB::table('web_vest_b2c')->count()

			);
		return View::make('admin/page', $data);
	}

}