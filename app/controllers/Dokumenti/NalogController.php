<?php

class NalogController extends Controller {

    public function nalog(){
        $partner = DokumentiOptions::user('saradnik');
        if(is_null($partner)){
        	return Redirect::to(DokumentiOptions::base_url().'dokumenti');
        }
        $is_is = B2bOptions::info_sys('wings') || B2bOptions::info_sys('calculus') || B2bOptions::info_sys('infograf') || B2bOptions::info_sys('logik');

        $data = array(
            'strana' => 'nalog',
            'title' => 'Nalog',
			'partner' =>  $partner,
			'is_is' => $is_is
        );

        return View::make('dokumenti/pages/nalog',$data);
    }

    public function nalog_save(){
        $partner = DokumentiOptions::user('saradnik');
        if(is_null($partner)){
        	return Redirect::to(DokumentiOptions::base_url().'dokumenti');
        }
        $partner_id = $partner->id;
        $inputs = Input::all();

        $rules = array(
                'naziv' => 'required|regex:'.Support::regex().'|between:2,50',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
                'mesto' => 'required|regex:'.Support::regex().'|between:2,100',
                'kontakt_osoba' => 'required|regex:'.AdminSupport::regex().'|max:255',
                'telefon' => 'required|regex:'.Support::regex().'|between:0,30',
                'mail' => 'required|email|max:50|unique:partner,mail,'.$partner_id.',partner_id',
                'pib' => 'required|numeric|digits_between:9,9',
                'broj_maticni' => 'numeric|digits_between:8,8',
                'telefon' => 'required|numeric',
                'login' => 'required',
                'password' => 'required'
               
            );
        $messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'max' => 'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!'
            );

         $validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else{

        DB::table('partner')->where('partner_id',$partner_id)->update($inputs);

        return Redirect::back()->with('message','Uspešno ste sačuvali podatke!');;
        }
    }

}