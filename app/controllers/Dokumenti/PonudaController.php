<?php

class PonudaController extends Controller {

	public function ponude(){
        $limit = 30;
        $page = Input::get('page') ? Input::get('page') : 1;
        $sortColumn = Input::get('sort_column') ? Input::get('sort_column') : 'broj_dokumenta';
        $sortDirection = Input::get('sort_direction') ? Input::get('sort_direction') : 'desc';
        $search = Input::get('search');
        $vrstaDokumenta = Input::get('vrsta_dokumenta') ? Input::get('vrsta_dokumenta') : 'ponuda';
        $partner_id = Input::get('partner_id') ? Input::get('partner_id') : null;
        $dokumenti_status_id = !is_null(Input::get('dokumenti_status_id')) ? Input::get('dokumenti_status_id') : 'all';
        $isb2b = Input::get('isb2b') == 1 ? true : false;

        $selectBrojDokumenta = "p.broj_dokumenta";
        $selectNazivPartnera = "(select naziv from partner where partner_id = p.partner_id limit 1)";
        $selectDatumPonude = "p.datum_ponude";
        $selectRokPlacanja = "p.vazi_do";
        $selectIznos = "p.iznos";
        $selectStatusName = "(select naziv from dokumenti_status where dokumenti_status_id = p.dokumenti_status_id limit 1) as status";

        $ponudeQuery = "select ponuda_id, ".$selectBrojDokumenta.", ".$selectNazivPartnera." as naziv_partnera, ".$selectDatumPonude.", ".$selectRokPlacanja.", ".$selectIznos.", ".$selectStatusName." from ponuda p";

        $whereArr = array();
        if(!is_null($partner=DokumentiOptions::user('saradnik'))){
            $whereArr[] = "(select parent_id from partner where partner_id = p.partner_id limit 1) = ".$partner->partner_id."";
        }else{
            if(is_null($partner_id)){
                $whereArr[] = "p.partner_id not in (select partner_id from partner where parent_id is not null)";
            }
        }
        if(!is_null($search) && $search != ''){
            $whereArr[] = "(".$selectBrojDokumenta." ilike '%".$search."%' or ".$selectNazivPartnera." ilike '%".$search."%')";
        }
        if($vrstaDokumenta == 'ponuda'){
            $whereArr[] = "predracun = 0";
        }elseif($vrstaDokumenta == 'predracun'){
            $whereArr[] = "predracun = 1";
        }
        if(!is_null($partner_id) && intval($partner_id) > 0){
            $whereArr[] = "(partner_id = ".$partner_id." or p.partner_id in (select partner_id from partner where parent_id = ".$partner_id."))";
        }
        if(!is_null($dokumenti_status_id) && $dokumenti_status_id != 'all' && intval($dokumenti_status_id) >= 0){
            $whereArr[] = "dokumenti_status_id ".($dokumenti_status_id==0 ? "IS NULL" : "= ".$dokumenti_status_id);
        }
        if(count($whereArr) > 0){
        	$ponudeQuery .= " where ".implode(" and ",$whereArr);
        }

        $column = $selectNazivPartnera;
        if($sortColumn == 'naziv'){
            $column = $selectNazivPartnera;
        }else if($sortColumn == 'broj_dokumenta'){
            $column = $selectBrojDokumenta;
        }else if($sortColumn == 'datum_ponude'){
            $column = $selectDatumPonude;
        }else if($sortColumn == 'vazi_do'){
            $column = $selectRokPlacanja;
        }else if($sortColumn == 'iznos'){
            $column = $selectIznos;
        }
        $ponudeQuery .= " order by ".$column." ".$sortDirection;


        $ponudeCount = count(DB::select($ponudeQuery));
        $ponudeQuery .= " limit ".strval($limit)." offset ".strval(($page-1)*$limit);
        $ponude = DB::select($ponudeQuery);
        $data=array(
            'strana' => 'ponude',
            'title' => 'Ponude',
            'search' => !is_null($search) ? $search : '',
            'ponude' => $ponude,
            'count' => $ponudeCount,
            'limit' => $limit,
            'sort_column' => $sortColumn,
            'sort_direction' => $sortDirection,
            'vrsta_dokumenta' => $vrstaDokumenta,
            'partner_id' => $partner_id,
            'dokumenti_status_id' => $dokumenti_status_id,
            'isb2b' => $isb2b
            );
        if($isb2b){
            $data['seo'] = array(
                'title' => 'Ponude',
                'description' => 'Ponude',
                'keywords' => 'ponude'
            );
        }

        return View::make('dokumenti/pages/ponude', $data); 
    }

    public function partner_pretraga(){
        $search = trim(Input::get('partner'));
        $reci = explode(" ",$search);

        $nazivFormatiran = array();
        foreach($reci as $rec){
            $nazivFormatiran[] = "naziv ILIKE '%" . $rec . "%'";
        }

        $partnerQuery = "SELECT * FROM partner ".(count($nazivFormatiran)>0 ? "WHERE (".implode(' OR ',$nazivFormatiran).")" : "");
        if(!is_null($partner=DokumentiOptions::user('saradnik'))){
            $partnerQuery .= (count($nazivFormatiran)>0 ? " AND" : "WHERE")." parent_id = ".$partner->partner_id;
        }
        $partnerQuery .= " ORDER BY naziv ASC";

        header('Content-type: text/plain; charset=utf-8');          
        $list = "<ul class='JSPartnerSearchList'>";
        foreach (DB::select($partnerQuery) as $partner) {
            $list .= "
                <li class='JSPartnerSearchItem' data-partner_id='".$partner->partner_id."'>
                    <div class='JSPartnerSearchLink'>"
                        ."<span class='JSPartnerSearchNaziv'>" . $partner->naziv . "</span>"
                        ."<span class='JSPartnerSearchPib'>" . $partner->pib . "</span>"
                        ."<span class='JSPartnerSearchAdresa'>" . $partner->adresa . "</span>
                    </div>
                </li>";
        }
        $list .= "</ul>";
        echo $list;      
    }

    public function partner_podaci(){
        $partner_id = Input::get('partner_id');
        $partner = DB::table('partner')->where('partner_id',$partner_id)->first();

        $partnerData = array(
            'pib' => $partner->pib,
            'adresa' => $partner->adresa,
            'mesto' => $partner->mesto,
            'kontakt_osoba' => $partner->kontakt_osoba,
            'mail' => $partner->mail,
            'telefon' => $partner->telefon
        );
        return json_encode($partnerData);
    }

	public function ponuda($ponuda_id){
        if($ponuda_id > 0 && !is_null($partner=DokumentiOptions::user('saradnik'))){
            if(count(DB::select("select * from ponuda p where (select parent_id from partner where partner_id = p.partner_id limit 1) = ".$partner->partner_id." and ponuda_id=".$ponuda_id)) == 0){
                return Response::make('Forbidden', 403);
            }
        }

        $novaStavka = Input::get('nova_stavka') ? Input::get('nova_stavka') : 0;
        $isb2b = Input::get('isb2b') == 1 ? true : false;
        $podesavanja = Dokumenti::podesavanja();
        $ponuda = $ponuda_id > 0 ? DB::table('ponuda')->where('ponuda_id',$ponuda_id)->first() : (object) array('ponuda_id'=>0,'partner_id'=>null,'broj_dokumenta'=>'Nova ponuda','datum_ponude'=>date('Y-m-d'),'vazi_do'=>date('Y-m-d'),'predracun'=>0,'dokumenti_status_id'=>null,'rok_isporuke'=>date('Y-m-d'),'nacin_placanja'=>null,'tekst'=>$podesavanja->sablon,'tekst_footer'=>$podesavanja->footer);

        $partner = $ponuda_id > 0 ? DB::table('partner')->where('partner_id',$ponuda->partner_id)->first() : (object) array('partner_id'=>0);

        $stavke = $ponuda_id > 0 ? DB::table('ponuda_stavka')->where('ponuda_id',$ponuda->ponuda_id)->orderBy('broj_stavke','asc')->get() : array();
        $stavkeRoba = $ponuda_id > 0 ? DB::table('ponuda_stavka')->select('roba_id')->whereNotNull('roba_id')->where('ponuda_id',$ponuda->ponuda_id)->orderBy('broj_stavke','asc')->get() : array();

        $data=array(
            'strana' => 'ponuda',
            'title' => 'Ponuda '.$ponuda->broj_dokumenta,
            'partner' => $partner,
            'ponuda' => $ponuda,
            'stavke' => $stavke,
            'stavkeUkupanIznos' => DB::select("select sum(pcena*kolicina) as iznos from ponuda_stavka where ponuda_id=".$ponuda_id)[0]->iznos,
            'roba_ids' => array_map('current',$stavkeRoba),
            'predracun' => DB::table('predracun')->where('ponuda_id',$ponuda_id)->first(),
            'racun' => DB::table('racun')->where('ponuda_id',$ponuda_id)->first(),
            'checkPredracuni' => DB::table('predracun')->where('ponuda_id',$ponuda->ponuda_id)->count() > 0,
            'checkRacuni' => DB::table('racun')->where('ponuda_id',$ponuda->ponuda_id)->count() > 0,
            'novaStavka' => ($novaStavka == 1),
            'isb2b' => $isb2b,
            'default_ponuda_id' => Session::has('dokumenti_ponuda_id'.Options::server()) && Session::get('dokumenti_ponuda_id'.Options::server()) == $ponuda_id ? 1 : 0
            );
        if($isb2b){
            $data['seo'] = array(
                'title' => 'Ponuda',
                'description' => 'Ponuda',
                'keywords' => 'ponuda'
            );
        } 
        return View::make('dokumenti/pages/ponuda', $data); 
	}
    public function ponuda_post(){
        $isb2b = Input::get('isb2b') == 1 ? true : false;
        $submit_order = array_key_exists('submit_order',Input::get()) ? 1 : 0;
        $submit_clone = array_key_exists('submit_clone',Input::get()) ? 1 : 0;
        $ponuda_id = Input::get('ponuda_id');
        $partner_id = Input::get('partner_id');
        $naziv = Input::get('naziv');
        $adresa = Input::get('adresa');
        $mesto = Input::get('mesto');
        $pib = Input::get('pib');
        $kontakt_osoba = Input::get('kontakt_osoba');
        $mail = Input::get('mail');
        $telefon = Input::get('telefon');
        $naziv_stavka = Input::get('naziv_stavka');
        $datum_ponude = Input::get('datum_ponude');
        $vazi_do = Input::get('vazi_do');
        $rok_isporuke = Input::get('rok_isporuke');
        $nacin_placanja = Input::get('nacin_placanja');
        $dokumenti_status_id = Input::get('dokumenti_status_id');
        $tekst = Input::get('tekst');
        $tekst_footer = Input::get('tekst_footer');
        $predracun = Input::get('predracun') ? 1 : 0;
        $racun = Input::get('racun') ? 1 : 0;
        $narudzbina = Input::get('narudzbina') ? 1 : 0;
        $u_korpu = Input::get('u_korpu') ? 1 : 0;
        $default_ponuda = Input::get('default_ponuda') ? 1 : 0;

        $oldPonudaSablon = $ponuda_id > 0 ? DB::table('ponuda')->select('tekst','tekst_footer')->where('ponuda_id',$ponuda_id)->first() : (object) array('tekst' => $tekst,'tekst_footer' => $tekst_footer);

        if(DB::table('racun')->where('ponuda_id',$ponuda_id)->count() > 0){
            return Redirect::back();
        }
        if(DB::table('predracun')->where('ponuda_id',$ponuda_id)->count() > 0){
            return Redirect::back();
        }
        if(!is_null(DokumentiOptions::user('saradnik')) && $submit_order && Input::get('u_korpu')==0 && DB::table('ponuda')->where('ponuda_id',$ponuda_id)->pluck('u_korpu')==1){
            return Redirect::to(DokumentiOptions::base_url().($isb2b?'b2b/':'').'dokumenti/ponuda/'.$ponuda_id)->withInput()->with('submit_order',true);
        }

        $data = array(
            'partner_id' => $partner_id,
            'naziv' => $naziv,
            'adresa' => $adresa,
            'mesto' => $mesto,
            'pib' => $pib,
            'kontakt_osoba' => $kontakt_osoba,
            'mail' => $mail,
            'telefon' => $telefon,
            'naziv_stavka' => $naziv_stavka,
            'datum_ponude' => Dokumenti::dateInversion($datum_ponude),
            'vazi_do' => Dokumenti::dateInversion($vazi_do),
            'rok_isporuke' => Dokumenti::dateInversion($rok_isporuke),
            'nacin_placanja' => $nacin_placanja,
            'dokumenti_status_id' => $dokumenti_status_id,
            'predracun' => $predracun,
            'racun' => $racun
        );
        $validator_arr = array(
            'partner_id' => 'required|integer',
            'naziv' => 'required|regex:'.Support::regex().'|between:2,255',
            'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
            'mesto' => 'regex:'.Support::regex().'|between:2,100', 
            'pib' => 'numeric',
            'kontakt_osoba' => 'regex:'.Support::regex().'|max:50',
            'mail' => 'regex:'.Support::regex().'|email|max:50',
            'telefon' => 'regex:'.Support::regex().'|max:50',
            'datum_ponude' => 'required|date',
            'vazi_do' => 'date',
            'rok_isporuke' => 'date',
            'nacin_placanja' => 'regex:'.Support::regex().'|max:200',
            'dokumenti_status_id' => 'integer',
            'predracun' => 'required|integer|in:0,1',
            'racun' => 'required|integer|in:0,1',
            'naziv_stavka' => 'regex:'.Support::regex().'|max:255'
        );
        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'not_in' => 'Niste popunili polje!',
            'max' => 'Dužina sadržaja nije dozvoljena!',
            'integer' => 'Vrednost sadržaja nije dozvoljena!',
            'digits_between' => 'Dužina sadržaja nije dozvoljena!',
            'between' => 'Dužina sadržaja nije dozvoljena!',
            'date' => 'Neodgovarajući format!'
        );

        $validator = Validator::make($data, $validator_arr, $messages);

        if($validator->fails()){
            return Redirect::to(DokumentiOptions::base_url().($isb2b?'b2b/':'').'dokumenti/ponuda/'.$ponuda_id)->withInput()->withErrors($validator);
        }
        $partnerdata = array(
            'partner_id' => $data['partner_id'],
            'naziv' => $data['naziv'],
            'adresa' => $data['adresa'],
            'mesto' => $data['mesto'],
            'pib' => $data['pib'],
            'kontakt_osoba' => $data['kontakt_osoba'],
            'mail' => $data['mail'],
            'telefon' => $data['telefon']     
        );
        if($partnerdata['partner_id'] == 0){
            $data['partner_id'] = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
            $partnerdata['partner_id'] = $data['partner_id'];
            $partnerdata['drzava_id'] = 0;
            $partnerdata['rabat'] = 0.00;
            $partnerdata['limit_p'] = 0.00;
            $partnerdata['parent_id'] = !is_null($partnerSaradnik=DokumentiOptions::user('saradnik')) ? $partnerSaradnik->partner_id : null;
            DB::table('partner')->insert($partnerdata);            
        }else{
            unset($partnerdata['partner_id']);
            DB::table('partner')->where('partner_id', $data['partner_id'])->update($partnerdata);
        }

        $podesavanja = Dokumenti::podesavanja();
        $dbdata = array(
            'partner_id' => $data['partner_id'],
            'datum_ponude' => $data['datum_ponude'],
            'vazi_do' => $data['vazi_do'],
            'rok_isporuke' => $data['rok_isporuke'],
            'nacin_placanja' => $data['nacin_placanja'],
            'dokumenti_status_id' => $data['dokumenti_status_id'] == 0 ? null : $data['dokumenti_status_id'],
            'tekst' => !empty(strip_tags($tekst)) ? $tekst : $podesavanja->sablon,
            'tekst_footer' => !empty(strip_tags($tekst_footer)) ? $tekst_footer : $podesavanja->footer
        );

        if($ponuda_id==0){
            $ponuda_id = DB::select("SELECT nextval('ponuda_ponuda_id_seq') as ponuda_id")[0]->ponuda_id;
            $dbdata['ponuda_id'] = $ponuda_id;
            $dbdata['broj_dokumenta'] = "PON".str_pad($ponuda_id,5,"0", STR_PAD_LEFT);
            $dbdata['vrsta_dokumenta_id'] = 40;
            DB::table('ponuda')->insert($dbdata);
        }else{
            DB::table('ponuda')->where('ponuda_id',$ponuda_id)->update($dbdata);
        }

        $racun_id = null;
        if($racun == 1){
            $dbdata['datum_racuna'] = $datum_ponude;
            $dbdata['rok_placanja'] = $vazi_do;
            unset($dbdata['datum_ponude']);
            unset($dbdata['vazi_do']);
            unset($dbdata['predracun']);
            unset($dbdata['tekst']);
            unset($dbdata['tekst_footer']);
            $dbdata['ponuda_id'] = $ponuda_id;
            $racun_id = DB::select("SELECT nextval('racun_racun_id_seq1') as racun_id")[0]->racun_id;
            $dbdata['racun_id'] = $racun_id;
            $dbdata['broj_dokumenta'] = "RAC".str_pad($racun_id,5,"0", STR_PAD_LEFT);
            $dbdata['vrsta_dokumenta_id'] = 100;

            DB::table('racun')->insert($dbdata);

            DB::statement("insert into racun_stavka (racun_id,broj_stavke,naziv_stavke,roba_id,nab_cena,pdv,pcena,kolicina) select ".$racun_id.",broj_stavke,naziv_stavke,roba_id,nab_cena,pdv,pcena,kolicina from ponuda_stavka where ponuda_id=".$ponuda_id);

            $iznos = DB::select("select sum(pcena*kolicina) as iznos from racun_stavka where racun_id=".$racun_id);
            DB::table('racun')->where('racun_id',$racun_id)->update(array('iznos'=>$iznos[0]->iznos));
        }
        $predracun_id = null;
        if($predracun == 1){
            $dbdata['datum_predracuna'] = $datum_ponude;
            $dbdata['vazi_do'] = $vazi_do;
            unset($dbdata['datum_ponude']);
            unset($dbdata['datum_racuna']);
            unset($dbdata['predracun']);
            unset($dbdata['rok_placanja']);
            unset($dbdata['racun_id']);
            $dbdata['ponuda_id'] = $ponuda_id;
            $predracun_id = DB::select("SELECT nextval('predracun_predracun_id_seq') as predracun_id")[0]->predracun_id;
            $dbdata['predracun_id'] = $predracun_id;
            $dbdata['broj_dokumenta'] = "PRE".str_pad($predracun_id,5,"0", STR_PAD_LEFT);
            $dbdata['vrsta_dokumenta_id'] = 50;
            $dbdata['tekst'] = !empty(strip_tags($tekst)) ? $tekst : $podesavanja->sablon;
            $dbdata['tekst_footer'] = !empty(strip_tags($tekst_footer)) ? $tekst_footer : $podesavanja->footer;

            DB::table('predracun')->insert($dbdata);

            DB::statement("insert into predracun_stavka (predracun_id,broj_stavke,naziv_stavke,roba_id,nab_cena,pdv,pcena,kolicina) select ".$predracun_id.",broj_stavke,naziv_stavke,roba_id,nab_cena,pdv,pcena,kolicina from ponuda_stavka where ponuda_id=".$ponuda_id);

            $iznos = DB::select("select sum(pcena*kolicina) as iznos from predracun_stavka where predracun_id=".$predracun_id);
            DB::table('predracun')->where('predracun_id',$predracun_id)->update(array('iznos'=>$iznos[0]->iznos));
        }

        if($default_ponuda == 1){
            Session::put('dokumenti_ponuda_id'.Options::server(),$ponuda_id);
        }else{
            if(Session::get('dokumenti_ponuda_id'.Options::server())==$ponuda_id){
                Session::forget('dokumenti_ponuda_id'.Options::server());
            }
        }
        // if($narudzbina == 1){
        //     Dokumenti::makeOrder($ponuda_id);
        // }

        $ponudaSablon = DB::table('ponuda')->select('tekst','tekst_footer')->where('ponuda_id',$ponuda_id)->first();
        $sablon_change = 0;
        if(
            ((!empty(strip_tags($tekst)) && $oldPonudaSablon->tekst != $ponudaSablon->tekst && $tekst != $podesavanja->sablon) || 
            (!empty(strip_tags($tekst_footer)) && $oldPonudaSablon->tekst_footer != $ponudaSablon->tekst_footer && $tekst_footer != $podesavanja->footer))
        ){
            $sablon_change = 1;
        }

        if($submit_clone){
            $ponuda_id = Dokumenti::clone($ponuda_id);
        }
        if($submit_order){
            Dokumenti::toCart($ponuda_id);
            DB::table('ponuda')->where('ponuda_id',$ponuda_id)->update(array('u_korpu'=>1));
            Session::forget('dokumenti_ponuda_id'.Options::server());
        }
        return Redirect::to(DokumentiOptions::base_url().($isb2b?'b2b/':'').'dokumenti/ponuda/'.$ponuda_id)->with('message',$narudzbina == 0 ? 'Uspešno ste sačuvali ponudu!' : 'Uspešno ste poslali narudžbinu!')->with('sablon_change',$sablon_change);
    }

    public function ponuda_stavka_save(){
        $data = Input::get();
        if(DB::table('predracun')->where('ponuda_id',$data['ponuda_id'])->count() > 0 || DB::table('racun')->where('ponuda_id',$data['ponuda_id'])->count() > 0){
            return json_encode(array('success'=>false));
        }
        $ponuda_stavka = null;

        if($data['ponuda_stavka_id'] == 0){
            if($data['change'] == 'roba'){
                $roba = DB::table('roba')->where('roba_id',$data['roba_id'])->first();
                $rabat = !is_null(DokumentiOptions::user('admin')) ? intval(B2bArticle::b2bRabatCene($roba->roba_id,DB::table('ponuda')->where('ponuda_id',$data['ponuda_id'])->pluck('partner_id'))->ukupan_rabat) : 0;
                $pdv = DB::table('tarifna_grupa')->where('tarifna_grupa_id', $roba->tarifna_grupa_id)->pluck('porez');
                $data['ponuda_stavka_id'] = DB::select("SELECT nextval('ponuda_stavka_ponuda_stavka_id_seq')")[0]->nextval;

                $insertData = array(
                    'ponuda_stavka_id' => $data['ponuda_stavka_id'],
                    'ponuda_id' => $data['ponuda_id'],
                    'broj_stavke' => DB::table('ponuda_stavka')->where('ponuda_id',$data['ponuda_id'])->max('broj_stavke')+1,
                    'roba_id' => $roba->roba_id,
                    'naziv_stavke' => $roba->naziv,
                    'kolicina' => 1,
                    'pdv' => $pdv,
                    'rabat' => $rabat,
                    'added_rabat' => 0,
                    'nab_cena' => B2bOptions::gnrl_options(3028) == 1 ? $roba->web_cena/(1+$pdv/100) : $roba->racunska_cena_end,
                    'pcena' => B2bOptions::gnrl_options(3028) == 1 ? $roba->web_cena/(1+$pdv/100)*(1-$rabat/100)*(1+$pdv/100) : $roba->racunska_cena_end*(1-$rabat/100)*(1+$pdv/100)
                );
            }else if($data['change'] == 'naziv_stavke'){
                $data['ponuda_stavka_id'] = DB::select("SELECT nextval('ponuda_stavka_ponuda_stavka_id_seq')")[0]->nextval;
                $insertData = array(
                    'ponuda_stavka_id' => $data['ponuda_stavka_id'],
                    'ponuda_id' => $data['ponuda_id'],
                    'broj_stavke' => DB::table('ponuda_stavka')->where('ponuda_id',$data['ponuda_id'])->max('broj_stavke')+1,
                    'naziv_stavke' => $data['naziv_stavke'],
                    'kolicina' => 1,
                    'pdv' => 20,
                    'rabat' => 0,
                    'added_rabat' => 0,
                    'nab_cena' => 0,
                    'pcena' => 0
                );                
            }
            DB::table('ponuda_stavka')->insert($insertData);
        }else{

            $ponuda_stavka = DB::table('ponuda_stavka')->where('ponuda_stavka_id',$data['ponuda_stavka_id'])->first();

            if($data['change'] == 'roba'){
                $roba = DB::table('roba')->where('roba_id',$data['roba_id'])->first();
                $pdv = DB::table('tarifna_grupa')->where('tarifna_grupa_id', $roba->tarifna_grupa_id)->pluck('porez');
                $rabat = !is_null(DokumentiOptions::user('admin')) ? intval(B2bArticle::b2bRabatCene($roba->roba_id,DB::table('ponuda')->where('ponuda_id',$data['ponuda_id'])->pluck('partner_id'))->ukupan_rabat) : 0;
                $updateData = array(
                    'roba_id' => $roba->roba_id,
                    'pdv' => $pdv,
                    'rabat' => $rabat,
                    'added_rabat' => 0,
                    'naziv_stavke' => $roba->naziv,
                    'nab_cena' => B2bOptions::gnrl_options(3028) == 1 ? $roba->web_cena/(1+$pdv/100) : $roba->racunska_cena_end,
                    'pcena' => B2bOptions::gnrl_options(3028) == 1 ? $roba->web_cena/(1+$pdv/100)*(1-$rabat/100)*(1+$pdv/100) : $roba->racunska_cena_end*(1-$rabat/100)*(1+$pdv/100)
                );
            }else if($data['change'] == 'naziv_stavke'){
                $updateData = array(
                    'roba_id' => null,
                    'naziv_stavke' => $data['naziv_stavke']
                );
            }else if($data['change'] == 'kolicina'){
                if($data['narudzbina']==1){
                    $updateData = array(
                        'kolicina_narudzbine' => $data['kolicina']
                    );                    
                }else{
                    $updateData = array(
                        'kolicina' => $data['kolicina']
                    );
                }
            }else if($data['change'] == 'pdv'){
                $updateData = array(
                    'pdv' => $data['pdv'],
                    'pcena' => $ponuda_stavka->nab_cena * (1-$ponuda_stavka->rabat/100) * (1-$ponuda_stavka->added_rabat/100) * (1+$data['pdv']/100)
                );
            }else if($data['change'] == 'rabat'){
                $updateData = array(
                    'rabat' => $data['rabat'],
                    'pcena' => $ponuda_stavka->nab_cena*(1-$data['rabat']/100) * (1-$ponuda_stavka->added_rabat/100) * (1+$ponuda_stavka->pdv/100)
                );
            }else if($data['change'] == 'added_rabat'){
                $updateData = array(
                    'added_rabat' => $data['added_rabat'],
                    'pcena' => $ponuda_stavka->nab_cena*(1-$ponuda_stavka->rabat/100) * (1-$data['added_rabat']/100) * (1+$ponuda_stavka->pdv/100)
                );
            }else if($data['change'] == 'nab_cena'){
                $updateData = array(
                    'nab_cena' => $data['nab_cena'],
                    'pcena' => $data['nab_cena']*(1-$ponuda_stavka->rabat/100) * (1-$ponuda_stavka->added_rabat/100) * (1+$ponuda_stavka->pdv/100)
                );
            }else if($data['change'] == 'naziv_stavke'){
                $updateData = array(
                    'naziv_stavke' => $data['naziv_stavke'],
                    'roba_id' => null
                );
            }

            DB::table('ponuda_stavka')->where('ponuda_stavka_id',$data['ponuda_stavka_id'])->update($updateData);
        }

        $ponuda_stavka = DB::table('ponuda_stavka')->where('ponuda_stavka_id',$data['ponuda_stavka_id'])->first();

        $iznos = DB::select("select sum(pcena*".(isset($data['narudzbina']) && $data['narudzbina']==1 ? "kolicina_narudzbine" : "kolicina").") as iznos from ponuda_stavka where ponuda_id=".$ponuda_stavka->ponuda_id);
        DB::table('ponuda')->where('ponuda_id',$ponuda_stavka->ponuda_id)->update(array('iznos'=>$iznos[0]->iznos));

        $responseData = array(
            'success' => true,
            'roba_id' => $ponuda_stavka->roba_id,
            'nab_cena' => $ponuda_stavka->nab_cena,
            'pdv' => $ponuda_stavka->pdv,
            'pcena' => $ponuda_stavka->pcena,
            'rabat' => $ponuda_stavka->rabat,
            'added_rabat' => $ponuda_stavka->added_rabat,
            'rabat_cena' => $ponuda_stavka->nab_cena*(1-$ponuda_stavka->rabat/100)*(1-$ponuda_stavka->added_rabat/100),
            'kolicina' => isset($data['narudzbina']) && $data['narudzbina']==1 ? $ponuda_stavka->kolicina_narudzbine : $ponuda_stavka->kolicina,
            'opis_robe' => $ponuda_stavka->opis_robe,
            'naziv_stavke' => $ponuda_stavka->naziv_stavke,
            'ukupna_cena' => $ponuda_stavka->nab_cena*(1-$ponuda_stavka->rabat/100)*(1-$ponuda_stavka->added_rabat/100)*(1+$ponuda_stavka->pdv/100)*(isset($data['narudzbina']) && $data['narudzbina']==1 ? $ponuda_stavka->kolicina_narudzbine : $ponuda_stavka->kolicina),
            'iznos' => $iznos[0]->iznos
        );
        return json_encode($responseData);
    }

    public function ponuda_stavka_delete($ponuda_stavka_id){
        $isb2b = Input::get('isb2b') == 1 ? true : false;
        $ponuda_id = DB::table('ponuda_stavka')->where(array('ponuda_stavka_id'=>$ponuda_stavka_id))->pluck('ponuda_id');

        DB::table('ponuda_stavka')->where(array('ponuda_stavka_id'=>$ponuda_stavka_id))->delete();

        $iznos = DB::select("select sum(pcena*kolicina) as iznos from ponuda_stavka where ponuda_id=".$ponuda_id);
        DB::table('ponuda')->where('ponuda_id',$ponuda_id)->update(array('iznos'=>$iznos[0]->iznos));

        return Redirect::to(DokumentiOptions::base_url().($isb2b?'b2b/':'').'dokumenti/ponuda/'.$ponuda_id.'#JSStavke')->with('message','Uspešno ste uklonili stavku!');
    }
    public function ponuda_delete($ponuda_id){
        $isb2b = Input::get('isb2b') == 1 ? true : false;
        if(DB::table('racun')->where('ponuda_id',$ponuda_id)->count() > 0){
            return Redirect::back();
        }
        DB::table('ponuda_stavka')->where(array('ponuda_id'=>$ponuda_id))->delete();
        DB::table('ponuda')->where(array('ponuda_id'=>$ponuda_id))->delete();

        Session::forget('dokumenti_ponuda_id'.Options::server());

        return Redirect::to(DokumentiOptions::base_url().($isb2b?'b2b/':'').'dokumenti/ponude');
    }

    public function ponuda_kolicina_narudzbine(){
        $ponuda_id = Input::get('ponuda_id');
        DB::statement("update ponuda_stavka set kolicina_narudzbine = kolicina where ponuda_id = ".$ponuda_id);
    }

    public function ponuda_bez_pdv(){
        $ponuda_id = Input::get('ponuda_id');
        DB::statement("update ponuda_stavka set pdv=0, pcena=(nab_cena*(1-rabat/100)*(1-added_rabat/100)) where ponuda_id = ".$ponuda_id);
        $iznos = DB::select("select sum(pcena*kolicina) as iznos from ponuda_stavka where ponuda_id=".$ponuda_id);
        DB::table('ponuda')->where('ponuda_id',$ponuda_id)->update(array('iznos'=>$iznos[0]->iznos));
    }

    public function ponuda_pdf($ponuda_id){
        $saradnik = DokumentiOptions::user('saradnik');
        $ponuda = DB::table('ponuda')->where('ponuda_id',$ponuda_id)->first();

        $ponudaCene = (object) array(
            'ukupno' => DB::select("select sum(nab_cena*(1-rabat/100)*(1-added_rabat/100)*kolicina) as ukupno from ponuda_stavka where ponuda_id=".$ponuda_id."")[0]->ukupno,
            'pdv' => DB::select("select sum(nab_cena*(1-rabat/100)*(1-added_rabat/100)*kolicina*(pdv/100)) as pdv from ponuda_stavka where ponuda_id=".$ponuda_id."")[0]->pdv,
            'ukupno_pdv' => DB::select("select sum(nab_cena*(1-rabat/100)*(1-added_rabat/100)*kolicina*(1+pdv/100)) as ukupno_pdv from ponuda_stavka where ponuda_id=".$ponuda_id."")[0]->ukupno_pdv,
        );


        $show_header = true;
        $limit = 9;
        $stavke_count = DB::table('ponuda_stavka')->where('ponuda_id',$ponuda->ponuda_id)->count();
        $count_pages = intdiv($stavke_count,$limit);
        $mod = fmod($stavke_count,$limit);
        if($mod > 0){ $count_pages++; }
        if($mod > 2){
            $count_pages++;
            $show_header = false;
        }else if($mod == 0){
            $count_pages++;
            $show_header = false;
        }
        $rbr = 1;

        $data = array(
            'ponuda' => $ponuda,
            'saradnik' => $saradnik,
            'partner' => DB::table('partner')->where('partner_id',$ponuda->partner_id)->first(),
            'podesavanja' => Dokumenti::podesavanja(),
            'ponudaCene' => $ponudaCene,
            'show_header' => $show_header,
            'limit' => $limit,
            'count_pages' => $count_pages,
            'rbr' => $rbr
        );

        // return View::make('dokumenti.pdf.ponuda', $data);
        $pdf = App::make('dompdf');
        $pdf->loadView('dokumenti.pdf.ponuda', $data);

        return $pdf->stream();
    }


}