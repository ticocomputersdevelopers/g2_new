<?php 

class CompareController extends Controller {

	public function compareajax(){
		$lang = Language::multi() ? Request::segment(1) : null;

	  	if(Session::has('compare_ids')){
		  	if(count(Session::get('compare_ids')) <= 4 && !in_array(Input::get('id'), Session::get('compare_ids'))){
		  		Session::push('compare_ids', Input::get('id'));
		    }
		}else{
			Session::push('compare_ids', Input::get('id'));
		}
		return Response::json(array('count_compare'=>count(Session::get('compare_ids'))));
	  }
	public function clear_compare(){
		$lang = Language::multi() ? Request::segment(1) : null;

		if(Session::has('compare_ids')){
			$ids = Session::get('compare_ids');
	        foreach($ids as $key => $id){
	            if(Input::get('clear_id') == $id){
	                unset($ids[$key]);
	            }
	        }
	     	Session::forget('compare_ids');
	     	foreach($ids as $id){
	     		Session::push('compare_ids', $id);
	     	}	        
		 }
		 return Response::json(array('count_compare'=>count(Session::get('compare_ids'))));
	} 

      public function compare(){
      	$lang = Language::multi() ? Request::segment(1) : null;

      	if(Session::has('compare_ids')){
	      	$ids = Session::get('compare_ids');
	     	if(Input::get('clear_id')!=null){
		        foreach($ids as $key => $id){
		            if(Input::get('clear_id') == $id){
		                unset($ids[$key]);
		            }
		        }
	     	}
	     	Session::forget('compare_ids');
	     	foreach($ids as $id){
	     		Session::push('compare_ids', $id);
	     	}
	     	$ids = Session::get('compare_ids');

	     	if(is_array($ids) && count($ids)>0){

		        $table = '<table><td></td>';
		        foreach($ids as $id){
		            $table .= '<td><span class="JSclearCompare" data-id="'.$id.'">'.Language::trans('Obriši').'</span></td>';
		        }
		        $table .= '<tr><td class="compare-table-info">'.Language::trans('naziv').'</td>';
		        foreach($ids as $id){
		            $table .= '<td>'.Language::trans(All::getCompare($id)[0]->naziv_web).'</td>';
		        }
		        $table .= '<tr><td class="compare-table-info">'.Language::trans('slika').'</td>';
		        foreach($ids as $id){
		            $table .= '<td><img src="'.Options::base_url().Product::web_slika($id).'"></td>';
		        }
		        $table .= '</tr><tr><td class="compare-table-info">'.Language::trans('cena').'</td>';
		        foreach($ids as $id){
		        	$akcijska_cena = Cart::cena(All::getCompare($id)[0]->akcijska_cena);
		        	$web_cena = Cart::cena(Options::checkCena()=='web_cena'?All::getCompare($id)[0]->web_cena:All::getCompare($id)[0]->mpcena);
		        	if(All::getCompare($id)[0]->akcija_flag_primeni==1 && $akcijska_cena<$web_cena && $akcijska_cena!=0){
		        		$table .= '<td>'.$akcijska_cena.'</td>';
		        	}else{
		            	$table .= '<td>'.$web_cena.'</td>';
		        	}
		        }       
		        $table .= '</tr><tr><td class="compare-table-info">'.Language::trans('proizvođač').'</td>';
		        foreach($ids as $id){
		            $table .= '<td>'.All::getCompare($id)[0]->proizvodjac.'</td>';
		        }
		        $table .= '</tr>';

	            foreach(All::getCompare($ids) as $karak){
	                $table .= '<tr><td>'.Language::trans($karak->naziv).'</td>';
	                
	                foreach($ids as $id){
	                    if(All::getVrednost($id,$karak->naziv)){
	                        $table .= '<td>'.All::getVrednost($id,$karak->naziv).'</td>';
	                    }else{
	                        $table .= '<td></td>';
	                    }
	                }
	                $table .= '</tr>';
	            }
		        $table .= '</table>';
		        $content = $table;
	     	}else{
	     		$content = Language::trans('Najmanje dva artikla mogu biti upoređena').'!';
	     	}
     	}else{
     		$content = Language::trans('Najmanje dva artikla mogu biti upoređena').'!';
     	}
     	return Response::json(array('content'=>$content, 'count_compare'=>count(Session::get('compare_ids'))));
     } 


} 