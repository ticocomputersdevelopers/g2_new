<?php 
use Service\ElasticSearchService;

class SearchController extends Controller {

	public function search(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $org_search = Request::segment(2+$offset);
        $grupa_pr_id = Request::segment(3+$offset);

		$proizvodjac = Request::get('m');
		$karakteristike = Request::get('ch');
		$cene = Request::get('pr');
		$grupe = Request::get('gr');

		$search = pg_escape_string(strtolower(urldecode($org_search)));

		$a = array('љ','њ','е','р','т','з','у','и','о','п','ш','ђ','а','с','д','ф','г','х','ј','к','л','ч','ћ','ж','ѕ','џ','ц','в','б','н','м', 'Љ','Њ','Е','Р','Т','З','У','И','О','П','Ш','Ђ','А','С','Д','Ф','Г','Х','Ј','К','Л','Ч','Ћ','Ж','Ѕ','Џ','Ц','В','Б','Н','М','Č','Š','č','š','Ć','Đ','ć','đ','Ž','ž'); 
  		$b = array('lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm', 'lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm','C','S','c','s','C','Dj','c','dj','Z','z'); 

  		if(count($jezikKods=DB::table('jezik')->where(array('aktivan'=>1))->get())==1 && $jezikKods[0]->kod=='cir'){
  			$search = str_replace($b, $a, $search);
  		}else{
  			$search = str_replace($a, $b, $search);
  		}

		if(Session::has('limit')){
			$limit=Session::get('limit');
		}
		else { 
			$limit=20;
		}
	    if(Input::get('page')){
	    	$pageNo = Input::get('page');
	    }else{
	    	$pageNo = 1; 
	    }
	    $offset = ($pageNo-1)*$limit;

		$reci = explode(" ", $search);
		
			$nazivFormatiran0 = '';
		// Za svaku pretrazenu rec dodaje deo query-ja
		foreach ($reci as $rec) {
			$nazivFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('naziv_web')." ILIKE '%" . $rec . "%' ";
			// Brise AND samo na pocetku querija, a na ostalim recim ostaje
			$nazivFormatiran = substr($nazivFormatiran0, 4);
		}

		$naziv_char_translate0 = '';
		// Za svaku pretrazenu rec dodaje deo query-ja
		foreach ($reci as $rec) {
			$naziv_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('naziv_web').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			// Brise AND samo na pocetku querija, a na ostalim recim ostaje
			$naziv_char_translate = substr($naziv_char_translate0, 4);
		}

		$tagsFormatiran0 = '';
		foreach ($reci as $rec) {
			$tagsFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('tags')." ILIKE '%" . $rec . "%' ";
			$tagsFormatiran = substr($tagsFormatiran0, 4);
		}

		$tags_char_translate0 = '';
		foreach ($reci as $rec) {
			$tags_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('tags').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$tags_char_translate = substr($tags_char_translate0, 4);
		}

		$opisFormatiran0 = '';
		foreach ($reci as $rec) {
			$opisFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('web_opis')." ILIKE '%" . $rec . "%' ";
			$opisFormatiran = substr($opisFormatiran0, 4);
		}

		$opis_char_translate0 = '';
		foreach ($reci as $rec) {
			$opis_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('web_opis').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$opis_char_translate = substr($opis_char_translate0, 4);
		}

		$karakFormatiran0 = '';
		foreach ($reci as $rec) {
			$karakFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('karakteristika_naziv')." ILIKE '%" . $rec . "%' ";
			$karakFormatiran = substr($karakFormatiran0, 4);
		}

		$karak_char_translate0 = '';
		foreach ($reci as $rec) {
			$karak_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('karakteristika_naziv').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$karak_char_translate = substr($karak_char_translate0, 4);
		}

		$karakVrednostFormatiran0 = '';
		foreach ($reci as $rec) {
			$karakVrednostFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('karakteristika_vrednost')." ILIKE '%" . $rec . "%' ";
			$karakVrednostFormatiran = substr($karakVrednostFormatiran0, 4);
		}

		$karakVrednost_char_translate0 = '';
		foreach ($reci as $rec) {
			$karakVrednost_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('karakteristika_vrednost').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$karakVrednost_char_translate = substr($karakVrednost_char_translate0, 4);
		}

		$dopunski_nazivFormatiran0 = '';
		foreach ($reci as $rec) {
			$dopunski_nazivFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('naziv_dopunski')." ILIKE '%" . $rec . "%' ";
			$dopunski_nazivFormatiran = substr($dopunski_nazivFormatiran0, 4);
		}

		$dopunski_naziv_char_translate0 = '';
		foreach ($reci as $rec) {
			$dopunski_naziv_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('naziv_dopunski').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$dopunski_naziv_char_translate = substr($dopunski_naziv_char_translate0, 4);
		}

		$sifraisFormatiran0 = '';
			foreach ($reci as $rec) {
				$sifraisFormatiran0 .= "AND r.sifra_is ILIKE '%" . $rec . "%' ";
				$sifraisFormatiran = substr($sifraisFormatiran0, 4);
		}

		$roba_idFormatiran0 = '';
			foreach ($reci as $rec) {
				$roba_idFormatiran0 .= "AND r.roba_id::varchar ILIKE '%" . $rec . "%' ";
				$roba_idFormatiran = substr($roba_idFormatiran0, 4);
		}

		$SKUFormatiran0 = '';
		foreach ($reci as $rec) {
			$SKUFormatiran0 .= "AND sku ILIKE '%" . $rec . "%' ";
			$SKUFormatiran = substr($SKUFormatiran0, 4);
		}

		$sifradFormatiran0 = '';
		foreach ($reci as $rec) {
			$sifradFormatiran0 .= "AND sifra_d ILIKE '%" . $rec . "%' ";
			$sifradFormatiran = substr($sifradFormatiran0, 4);
		}

		$proizvodjacFormatiran0 = '';
		foreach ($reci as $rec) {
			$proizvodjacFormatiran0 .= "AND proizvodjac_id= (select proizvodjac_id FROM proizvodjac where naziv ILIKE '%" . $rec . "%' LIMIT 1 )";
			$proizvodjacFormatiran = substr($proizvodjacFormatiran0, 4);
		}

		$CASFormatiran0 = '';
		foreach ($reci as $rec) {
			$CASFormatiran0 .= "AND cas_number = '".$rec."' ";
			$CASFormatiran = substr($CASFormatiran0, 4);
		}

		$nazivFajlaFormatiran0 = '';
		foreach ($reci as $rec) {
			$nazivFajlaFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('naziv')." ILIKE '%" . $rec . "%' ";
			$nazivFajlaFormatiran = substr($nazivFajlaFormatiran0, 4);
		}

		$nazivFajla_char_translate0 = '';
		foreach ($reci as $rec) {
			$nazivFajla_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('naziv').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
			$nazivFajla_char_translate = substr($nazivFajla_char_translate0, 4);
		}

	    $grupa_pr_ids=array();
	    $grupe_query = "";
	    if($grupa_pr_id){
		    Groups::allGroups($grupa_pr_ids,$grupa_pr_id);
		    $grupe_query = "r.grupa_pr_id IN (".implode(",",$grupa_pr_ids).") AND ";
	    }


		//NOVI QUERY SA KARAKTERISTIKAMA
		$query = "SELECT DISTINCT r.roba_id, ".Options::checkCena().", r.grupa_pr_id,naziv_web, rbr, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) AS grupa FROM roba r ".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join')."";
		$where =" WHERE flag_aktivan=1 ".Product::checkLager().Product::checkActiveGroup().Product::checkImage()." AND flag_prikazi_u_cenovniku=1 AND ".$grupe_query."(" . $nazivFormatiran . " OR " . $naziv_char_translate . " OR " . $tagsFormatiran . " OR " . $roba_idFormatiran . " OR " . $tags_char_translate . " OR " . $opisFormatiran . " OR " . $proizvodjacFormatiran . " OR " . $opis_char_translate . " OR " . $dopunski_nazivFormatiran . " OR " . $dopunski_naziv_char_translate . " OR " . $sifraisFormatiran . " OR " .$SKUFormatiran. " OR " .$CASFormatiran. " OR " .$sifradFormatiran. " OR r.roba_id IN (SELECT roba_id FROM dobavljac_cenovnik_karakteristike dck WHERE dck.roba_id <> -1 AND (" . $karakFormatiran . " OR " . $karak_char_translate . " OR " . $karakVrednostFormatiran . " OR " . $karakVrednost_char_translate . ")) OR r.roba_id in (SELECT roba_id FROM web_files WHERE roba_id <> -1 AND (" . $nazivFajlaFormatiran . " OR ".$nazivFajla_char_translate.")))";
		
		$query_init = "SELECT DISTINCT r.roba_id FROM roba r ".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join')."";
            $proiz_array=array();
            $krak_array=array();
            $gru_array=array();

			if(!is_null($proizvodjac) && !is_null($karakteristike)){
				if($proizvodjac!="0" && $karakteristike!="0"){

					$krak_array=explode("-",$karakteristike);
					$proiz_array=explode("-",$proizvodjac);

					if(Options::filters_type()==0){

						$where .= " AND proizvodjac_id IN (".implode(',',$proiz_array).")";

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$i = count(DB::select("SELECT DISTINCT grupa_pr_naziv_id FROM grupa_pr_vrednost WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";
						
					}else{
						$where .= " AND proizvodjac_id = ".$proizvodjac."";
						$query_karak="";
						$i=0;
						foreach($krak_array as $row){
							$i++;
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";					

					}		

				}

				if($proizvodjac!="0" && $karakteristike=="0"){
					$proiz_array=explode("-",$proizvodjac);
					$krak_array[]="-1";
					if(Options::filters_type()==0){
						$query_proz="";
						foreach($proiz_array as $proiz_id){
							$query_proz .= $proiz_id.",";
						}
						$where .= " AND proizvodjac_id IN (".substr($query_proz,0,-1).")";
					}else{
						$where .= " AND proizvodjac_id = ".$proizvodjac."";
					}	

				}

				if($proizvodjac=="0" && $karakteristike!="0"){
					$proiz_array[]="0";		

					$krak_array=explode("-",$karakteristike);

					if(Options::filters_type()==0){

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$i = count(DB::select("SELECT DISTINCT grupa_pr_naziv_id FROM grupa_pr_vrednost WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";					

					}else{

						$query_karak="";
						$i=0;
						foreach($krak_array as $row){
							$i++;
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";					

					}	

				}

			}

			$select_max = "SELECT MAX(web_cena) AS max_web_cena FROM roba r ".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join')."";
			$artikli_cena = DB::select($select_max.$where);
			$max_web_cena = $artikli_cena[0] ? $artikli_cena[0]->max_web_cena : 0;

			$select_min = "SELECT MIN(web_cena) AS min_web_cena FROM roba r ".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join')."";
			$artikli_cena = DB::select($select_min.$where);
			$min_web_cena = $artikli_cena[0] ? $artikli_cena[0]->min_web_cena : 0;
		
		if(!is_null($cene)){
			$cene_arr = explode('-',$cene);
			if($cene_arr[1]>0 && $cene_arr[1]>=$cene_arr[0]){
				$where .= " AND web_cena BETWEEN ".$cene_arr[0]." AND ".ceil($cene_arr[1])."";
			}
		}

		if(!empty($grupe) ){
			$gru_array=explode("-",$grupe);

			$query_gr="";
			foreach($gru_array as $gr_id){
				$query_gr .= $gr_id.",";
			}
			$where .= " AND r.grupa_pr_id IN (".substr($query_gr,0,-1).")";

			$query_grupa = "SELECT DISTINCT r.grupa_pr_id FROM roba r ".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join')."";
			
		}else{
			$gru_array[]="0";	
						$query_gr="";
			foreach($gru_array as $gr_id){
				$query_gr .= $gr_id.",";
			}
			$query_grupa = "SELECT DISTINCT r.grupa_pr_id FROM roba r ".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join')."";
		}
 				    if(Session::has('filter_lager')){
					if(Session::get('filter_lager')=='lager-yes'){
					$where .= " AND (SELECT SUM(kolicina) FROM lager l WHERE l.roba_id=r.roba_id GROUP BY l.roba_id) > 0 ";
					}
					else if(Session::get('filter_lager')=='lager-no'){
					$where .= " AND (SELECT SUM(kolicina) FROM lager l WHERE l.roba_id=r.roba_id GROUP BY l.roba_id) = 0";
					}
				}else{
					$where .= " AND (SELECT SUM(kolicina) FROM lager l WHERE l.roba_id=r.roba_id GROUP BY l.roba_id) > 0 ";
				}
		if(Session::has('order')){
			if(Session::get('order')=='price_asc')
			{
			$query_products=DB::select($query.$where." ORDER BY ".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
			}
			else if(Session::get('order')=='price_desc'){
			$query_products=DB::select($query.$where." ORDER BY ".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
			}
			else if(Session::get('order')=='news'){
			$query_products=DB::select($query.$where." ORDER BY roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
			}
			else if(Session::get('order')=='name'){
			$query_products=DB::select($query.$where." ORDER BY naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
			}
			else if(Session::get('order')=='rbr'){
			$query_products=DB::select($query.$where." ORDER BY rbr ASC LIMIT ".$limit." OFFSET ".$offset."");
			}
		}
		else {
	    	$query_products=DB::select($query.$where." ORDER BY naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
		}		
		

		$data=array(
			"strana"=>'pretraga',
			"title"=>"Pretraga",
			"description"=>"",
			"keywords"=>"",
			"url"=>'search/'.$org_search,
			"articles"=>$query_products,
			"grupe"=>!is_null($gru_array) ? All::getGrupe($query_grupa.$where) :  "0",
			"max_web_cena" => $max_web_cena,
			"min_web_cena" => $min_web_cena,
			"niz_grupa"=>$gru_array,
			"niz_proiz"=>$proiz_array,
			"niz_karakteristike"=>$krak_array,
			"proizvodjac_ids"=>!empty($proizvodjac) ? $proizvodjac : "0",
			"grupa_ids"=>!empty($grupe) ? $grupe : "0",
			"kara"=>!empty($karakteristike) ? $karakteristike : "0",
			"cene"=> !is_null($cene) ? $cene : '0-0',
			"count_products"=>count(DB::select($query.$where)),
			"filter_prikazi"=>1,
			"characteristics"=>0,
			"manufacturers"=>All::getProizvodjaci($query_init.$where),
			"limit"=>$limit
		);
		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);
	
	
	}	


	public function livesearch() {
        $lang = Language::multi() ? Request::segment(1) : null;

			$search = Input::get('search');
			$grupa_pr_id = Input::get('grupa_pr_id');

			$search = pg_escape_string(strtolower($search));
			$a = array('љ','њ','е','р','т','з','у','и','о','п','ш','ђ','а','с','д','ф','г','х','ј','к','л','ч','ћ','ж','ѕ','џ','ц','в','б','н','м', 'Љ','Њ','Е','Р','Т','З','У','И','О','П','Ш','Ђ','А','С','Д','Ф','Г','Х','Ј','К','Л','Ч','Ћ','Ж','Ѕ','Џ','Ц','В','Б','Н','М','Č','Š','č','š','Ć','Đ','ć','đ','Ž','ž'); 
	  		$b = array('lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm', 'lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm','C','S','c','s','C','Dj','c','dj','Z','z'); 

	  		if(count($jezikKods=DB::table('jezik')->where(array('aktivan'=>1))->get())==1 && $jezikKods[0]->kod=='cir'){
	  			$search = str_replace($b, $a, $search);
	  		}else{
	  			$search = str_replace($a, $b, $search);
	  		} 

	  		if(Options::gnrl_options(3055) == 1){
				$elastic = new ElasticSearchService();
				$groups = $elastic->searchGroups($search,$grupa_pr_id)['hits']['hits'];
				$manufacturers = $elastic->searchManufacturers($search,$grupa_pr_id)['hits']['hits'];
				$products = $elastic->searchProducts($search,$grupa_pr_id)['hits']['hits'];

				$list = "<ul class='JSsearch_list'>";
				if(($manufacturersCount=count($manufacturers)) > 0){
					$list .= "<li class='search_list__title'>".Language::trans_def('Proizvođači')."</li>";					
					foreach (array_slice($manufacturers,0,5) as $manufacturer) {
						$manufacturer = $manufacturer['_source'];
						$list .= "
							<li class='search_list__item'>
								<a class='search_list__item__link' href='" . Options::base_url().Url_mod::slug_trans('proizvodjac').'/'. $manufacturer['slug'] . "'>"
									."<span class='search_list__item__link__text'>" . $manufacturer['name'] . "</span>"
									."<span class='search_list__item__link__cat'>" . $manufacturer['count'] . "</span>
								</a>
							</li>";
					}
				} 
				if(($productsCount=count($products)) > 0){
					$list .= "<li class='search_list__title'>".Language::trans_def('Proizvodi')."</li>";					
					foreach ($products as $product) {
						$product = $product['_source'];
						$list .= "
							<li class='search_list__item'>
								<a class='search_list__item__link' href='" . Options::base_url() .Url_mod::slug_trans('artikal').'/'. $product['slug'] . "'>"
									."<span class='search_list__item__link__text'>" . $product['name'] . "</span>
								</a>
							</li>";
					}
				}
				if(($groupsCount=count($groups)) > 0){
					$list .= "<li class='search_list__title'>".Language::trans_def('Grupe')."</li>";					
					foreach (array_slice($groups,0,5) as $group) {
						$group = $group['_source'];
						$list .= "
							<li class='search_list__item'>
								<a class='search_list__item__link' href='" . Options::base_url() . $group['slug'] . "'>"
									."<span class='search_list__item__link__text'>" . $group['name'] . "</span>"
									."<span class='search_list__item__link__cat'>" . $group['count'] . "</span>
								</a>
							</li>";
					}
				}
				$list .= "</ul>";

				if($manufacturersCount==0 && $productsCount==0 && $groupsCount==0){
					echo '';
					die;
				}

				header('Content-type: text/plain; charset=utf-8');		
				echo $list;
				die;		
	  		}

			$reci = explode(" ", $search);

			$nazivFormatiran0 = '';

			// Za svaku pretrazenu rec dodaje deo query-ja
			foreach ($reci as $rec) {
				$nazivFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('naziv_web')." ILIKE '%" . $rec . "%' ";
				// Brise AND samo na pocetku querija, a na ostalim recim ostaje
				$nazivFormatiran = substr($nazivFormatiran0, 4);
			}

			$naziv_char_translate0 = '';
			// Za svaku pretrazenu rec dodaje deo query-ja
			foreach ($reci as $rec) {
				$naziv_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('naziv_web').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				// Brise AND samo na pocetku querija, a na ostalim recim ostaje
				$naziv_char_translate = substr($naziv_char_translate0, 4);
			}

			$tagsFormatiran0 = '';
			foreach ($reci as $rec) {
				$tagsFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('tags')." ILIKE '%" . $rec . "%' ";
				$tagsFormatiran = substr($tagsFormatiran0, 4);
			}

			$tags_char_translate0 = '';
			foreach ($reci as $rec) {
				$tags_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('tags').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$tags_char_translate = substr($tags_char_translate0, 4);
			}

			$opisFormatiran0 = '';
			foreach ($reci as $rec) {
				$opisFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('web_opis')." ILIKE '%" . $rec . "%' ";
				$opisFormatiran = substr($opisFormatiran0, 4);
			}

			$opis_char_translate0 = '';
			foreach ($reci as $rec) {
				$opis_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('web_opis').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$opis_char_translate = substr($opis_char_translate0, 4);
			}

			$karakFormatiran0 = '';
			foreach ($reci as $rec) {
				$karakFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('karakteristika_naziv')." ILIKE '%" . $rec . "%' ";
				$karakFormatiran = substr($karakFormatiran0, 4);
			}

			$karak_char_translate0 = '';
			foreach ($reci as $rec) {
				$karak_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('karakteristika_naziv').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$karak_char_translate = substr($karak_char_translate0, 4);
			}

			$karakVrednostFormatiran0 = '';
			foreach ($reci as $rec) {
				$karakVrednostFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('karakteristika_vrednost')." ILIKE '%" . $rec . "%' ";
				$karakVrednostFormatiran = substr($karakVrednostFormatiran0, 4);
			}

			$karakVrednost_char_translate0 = '';
			foreach ($reci as $rec) {
				$karakVrednost_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('karakteristika_vrednost').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$karakVrednost_char_translate = substr($karakVrednost_char_translate0, 4);
			}

			$dopunski_nazivFormatiran0 = '';
			foreach ($reci as $rec) {
				$dopunski_nazivFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('naziv_dopunski')." ILIKE '%" . $rec . "%' ";
				$dopunski_nazivFormatiran = substr($dopunski_nazivFormatiran0, 4);
			}

			$dopunski_naziv_char_translate0 = '';
			foreach ($reci as $rec) {
				$dopunski_naziv_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('naziv_dopunski').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$dopunski_naziv_char_translate = substr($dopunski_naziv_char_translate0, 4);
			}

			$sifraisFormatiran0 = '';
			foreach ($reci as $rec) {
				$sifraisFormatiran0 .= "AND r.sifra_is ILIKE '%" . $rec . "%' ";
				$sifraisFormatiran = substr($sifraisFormatiran0, 4);
			}

			$SKUFormatiran0 = '';
			foreach ($reci as $rec) {
				$SKUFormatiran0 .= "AND sku ILIKE '%" . $rec . "%' ";
				$SKUFormatiran = substr($SKUFormatiran0, 4);
			}

			$sifradFormatiran0 = '';
			foreach ($reci as $rec) {
				$sifradFormatiran0 .= "AND sifra_d ILIKE '%" . $rec . "%' ";
				$sifradFormatiran = substr($sifradFormatiran0, 4);
			}
			
			$roba_idFormatiran0 = '';
			foreach ($reci as $rec) {
				$roba_idFormatiran0 .= "AND r.roba_id::varchar ILIKE '%" . $rec . "%' ";
				$roba_idFormatiran = substr($roba_idFormatiran0, 4);
			}

			$proizvodjacFormatiran0 = '';
			foreach ($reci as $rec) {
			$proizvodjacFormatiran0 .= "AND proizvodjac_id= (select proizvodjac_id FROM proizvodjac where naziv ILIKE '%" . $rec . "%' LIMIT 1 )";
			$proizvodjacFormatiran = substr($proizvodjacFormatiran0, 4);
			}

			$CASFormatiran0 = '';
			foreach ($reci as $rec) {
				$CASFormatiran0 .= "AND cas_number = '".$rec."' ";
				$CASFormatiran = substr($CASFormatiran0, 4);
			}

			$nazivFajlaFormatiran0 = '';
			foreach ($reci as $rec) {
				$nazivFajlaFormatiran0 .= "AND ".self::searchQueryStringSpecialCharacters('naziv')." ILIKE '%" . $rec . "%' ";
				$nazivFajlaFormatiran = substr($nazivFajlaFormatiran0, 4);
			}

			$nazivFajla_char_translate0 = '';
			foreach ($reci as $rec) {
				$nazivFajla_char_translate0 .= "AND translate (".self::searchQueryStringSpecialCharacters('naziv').", 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
				$nazivFajla_char_translate = substr($nazivFajla_char_translate0, 4);
			}

		    $grupa_pr_ids=array();
		    $grupe_query = "";
		    if($grupa_pr_id){
			    Groups::allGroups($grupa_pr_ids,$grupa_pr_id);
			    $grupe_query = "r.grupa_pr_id IN (".implode(",",$grupa_pr_ids).") AND ";
		    }

			$articles = DB::select("SELECT DISTINCT r.roba_id, naziv_web, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) AS grupa FROM roba r ".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join')." WHERE flag_aktivan=1 ".Product::checkLager().Product::checkActiveGroup().Product::checkImage()." AND flag_prikazi_u_cenovniku=1 AND ".$grupe_query."(" . $nazivFormatiran . " OR " . $naziv_char_translate . " OR " . $tagsFormatiran . " OR " . $roba_idFormatiran . " OR " . $tags_char_translate . " OR " . $proizvodjacFormatiran . " OR ". $opisFormatiran . " OR " . $opis_char_translate . " OR " . $dopunski_nazivFormatiran . " OR " . $dopunski_naziv_char_translate . " OR " . $sifraisFormatiran . " OR " .$SKUFormatiran. " OR " .$CASFormatiran. " OR " .$sifradFormatiran. " OR r.roba_id IN (SELECT roba_id FROM dobavljac_cenovnik_karakteristike WHERE roba_id <> -1 AND (" . $karakFormatiran . " OR " . $karak_char_translate . " OR " . $karakVrednostFormatiran . " OR " . $karakVrednost_char_translate . ")) OR r.roba_id in (SELECT roba_id FROM web_files WHERE roba_id <> -1 AND (" . $nazivFajlaFormatiran . " OR ".$nazivFajla_char_translate."))) ORDER BY naziv_web ASC");
			header('Content-type: text/plain; charset=utf-8');			
			$list = "<ul class='JSsearch_list'>";
			foreach ($articles as $article) {
				$list .= "
					<li class='search_list__item'>
						<a class='search_list__item__link' href='" . Options::base_url() .Url_mod::slug_trans('artikal').'/'. Url_mod::slugify(Product::seo_title($article->roba_id)) . "'>"
							."<span class='search_list__item__link__text'>" . $article->naziv_web . "</span>"
							."<span class='search_list__item__link__cat'>" . $article->grupa . "</span>
						</a>
					</li>";
			}

			$list .= "</ul>";
			echo $list;

	}

	public static function searchQueryStringSpecialCharacters ($column) {
        return "replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(".$column.",'&#262;','Ć'),'&#263;','ć'),'&#352;','Š'),'&#353;','š'),'&#268;','Č'),'&#269;','č'),'&#272;','Ð'),'&#273;','đ'),'&#381;','Ž'),'&#382;','ž'),'&#956;','μ')";
    }



} 