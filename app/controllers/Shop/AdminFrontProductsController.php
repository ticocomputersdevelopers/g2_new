<?php


class AdminFrontProductsController extends Controller {

    function product_list()
    {
        $grupa_pr_id = Input::get('grupa_pr_id');
        $na_webu = Input::get('na_webu');
        $akcija = Input::get('na_akciji');
        $slike = Input::get('slike');
        $opis = Input::get('opis');
        $search = Input::get('search_string');

        $pageNo = Input::get('page');
        $limit = 20;
        $offset = ($pageNo-1)*$limit;                
        $main_query = "SELECT DISTINCT r.roba_id, flag_prikazi_u_cenovniku FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id";
        $where_query = " WHERE r.flag_aktivan = 1 ";
        $paginate_query = " ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."";

        if($grupa_pr_id > 0){
            $grupa_pr_ids = array();
            Groups::allGroups($grupa_pr_ids,$grupa_pr_id);
            $where_query .= "AND ( r.grupa_pr_id IN(".implode(",",$grupa_pr_ids).") OR rg.grupa_pr_id IN(".implode(",",$grupa_pr_ids)."))";
        }
        if($na_webu != null && $na_webu != 'null'){
            $where_query .= "AND flag_prikazi_u_cenovniku = ".$na_webu."";
        }
        if($akcija != null && $akcija != 'null'){
            $where_query .= "AND akcija_flag_primeni = ".$akcija."";
        }      
        if($slike != null && $slike != 'null'){
            if($slike == 1){
                $where_query .= "AND r.roba_id IN (SELECT roba_id FROM web_slika)";
            }else{
                $where_query .= "AND r.roba_id NOT IN (SELECT roba_id FROM web_slika)";
            }
        }
        if($opis != null && $opis != 'null'){
            if($opis == 1){
                $where_query .= "AND r.web_opis IS NOT NULL AND web_opis <> ''";
            }else{
                $where_query .= "AND (r.web_opis IS NULL OR web_opis = '')";
            }
        }
        if($search != null && $search != ''){
            foreach(explode(' ',$search) as $word){
                $where_query .= "AND (naziv_web ILIKE '%".$word."%' OR sku = '".$word."'";
                if(is_numeric($word)){
                    $where_query .= " OR r.roba_id = ".$word."";
                }
                $where_query .= ")";
            }
        }

        $count = count(DB::select($main_query.$where_query));
        $maxPage = intval(($count/$limit));
        if((5 % 3) > 0){
            $maxPage++;
        }
        $products=DB::select($main_query.$where_query.$paginate_query);

        $content = View::make('shop/front_admin/partials/product-list',array('products'=>$products))->render();

        return Response::json(array('content' => $content, 'max_page' => $maxPage, 'count' => $count));       
    }
    function product_list_execute()
    {
        $action = Input::get('action');
        $roba_ids = Input::get('roba_ids');
        $check = Input::get('check');

        if($action == "web_on"){
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('flag_prikazi_u_cenovniku'=>1));
            AdminSupport::saveLog('FRONT_ARTIKLI_STAVI_NA_WEB', $roba_ids);
        }elseif($action == "web_off") {
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('flag_prikazi_u_cenovniku'=>0));
            AdminSupport::saveLog('FRONT_ARTIKLI_SKINI_SA_WEBA', $roba_ids);
        }elseif($action == "sell_on") {
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('akcija_flag_primeni'=>1));
            AdminSupport::saveLog('FRONT_ARTIKLI_STAVI_NA_PRODAJU', $roba_ids);
        }elseif($action == "sell_off") {
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('akcija_flag_primeni'=>0));
            AdminSupport::saveLog('FRONT_ARTIKLI_SKINI_SA_PRODAJE', $roba_ids);
        }elseif($action == "delete") {
            $mesages = '';
            AdminSupport::saveLog('ARTIKLI_OBRISI', $roba_ids);
            foreach ($roba_ids as $roba_id) {
                $result = AdminArticles::delete($roba_id, $check);
                $mesages .= $result->message;
            }
            return Response::json(array('mesages' => $mesages));
        }

    }

    function product()
    {
        $roba_id = Input::get('roba_id');
        $roba = DB::table('roba')->where('roba_id',$roba_id)->first();

        $default_magacin_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
        $lager = DB::table('lager')->where(array('roba_id'=>$roba_id, 'orgj_id'=>$default_magacin_id))->first();
        if(is_null($lager)){
            $lager = (object) array('orgj_id'=>$default_magacin_id,'kolicina'=>0);
        }

        $data=array(
            "naziv"=> $roba_id != 0 ? $roba->naziv : null,
            "naziv_web"=> $roba_id != 0 ? $roba->naziv_web : null,
            "grupa_pr_id"=> $roba_id != 0 ? $roba->grupa_pr_id : null,
            "grupa_pr_grupa"=> $roba_id != 0 ? DB::table('grupa_pr')->where('grupa_pr_id',$roba->grupa_pr_id)->pluck('grupa') : null,
            "jedinica_mere_id"=> $roba_id != 0 ? $roba->jedinica_mere_id : 0,
            "orgj_id"=> $roba_id != 0 ? $lager->orgj_id : $default_magacin_id,
            "roba_flag_cene"=> $roba_id != 0 ? DB::table('roba_flag_cene')->where('roba_flag_cene_id',$roba->roba_flag_cene_id)->pluck('naziv') : null,
            "tip_artikla"=> $roba_id != 0 ? DB::table('tip_artikla')->where('tip_artikla_id',$roba->tip_cene)->pluck('naziv') : null,
            "akcija_flag_primeni"=> $roba_id != 0 ? $roba->akcija_flag_primeni : 0,
            "web_cena"=> $roba_id != 0 ? round($roba->web_cena,2) : null,
            "kolicina"=> $roba_id != 0 ? round($lager->kolicina) : null,
            "flag_prikazi_u_cenovniku"=> $roba_id != 0 ? $roba->flag_prikazi_u_cenovniku : 1,
            "tarifna_grupa_id"=> $roba_id != 0 ? $roba->tarifna_grupa_id : 2,
            "akcijska_cena"=> $roba_id != 0 ? round($roba->akcijska_cena,2) : 0,
            "web_opis"=> $roba_id != 0 ? $roba->web_opis : null,
            "datum_akcije_od"=> $roba_id != 0 ? $roba->datum_akcije_od : null,
            "datum_akcije_do"=> $roba_id != 0 ? $roba->datum_akcije_do : null,
            "sku"=> $roba_id != 0 ? $roba->sku : null,
            "roba_id"=> $roba_id
        );

        $content = View::make('shop/front_admin/partials/product-details',$data)->render();

        return Response::json(array('content' => $content));
    }


}