<?php

class ExternController extends Controller {

	public function export_xls($kind,$partner_slug=null){
		$domain = explode('/',Request::root())[2];

		if(is_null($partner_slug) || $domain != 'wbp.tico.rs'){
			return Redirect::to(Options::base_url());
		}

		$conn = null;
		$old_db = false;
		switch ($partner_slug) {
			case 'tico':
				$conn = DB::connection('pg_tico');
				break;
			case 'gembird':
				$old_db = true;
				$conn = DB::connection('pg_gamebird');
				break;
			case 'kondoras':
				$old_db = true;
				$conn = DB::connection('pg_kondoras');
				break;
			default:
				return Redirect::to(Options::base_url());
				break;
		}

		if($kind == 'partneri'){
			if($old_db){
				$partners = $conn->select("SELECT partner_id, naziv, mail, adresa, telefon, pib, broj_maticni, (SELECT mesto FROM mesto WHERE mesto_id = p.mesto_id) AS mesto, (SELECT COUNT(*) FROM web_b2b_narudzbina WHERE partner_id = p.partner_id) AS narudzbina_count FROM partner p WHERE partner_id > 0 ORDER BY partner_id ASC");
			}else{
				$partners = $conn->select("SELECT partner_id, naziv, mail, adresa, mesto, telefon, pib, broj_maticni, (SELECT COUNT(*) FROM web_b2b_narudzbina WHERE partner_id = p.partner_id) AS narudzbina_count FROM partner p WHERE partner_id > 0 ORDER BY partner_id ASC");
			}
		}elseif($kind == 'kupci'){
			if($old_db){
				$kupaci = $conn->select("SELECT DISTINCT web_kupac_id, naziv, pib, ime, prezime, flag_vrsta_kupca, email, adresa, telefon, (SELECT mesto FROM mesto WHERE mesto_id = k.mesto_id) as mesto FROM web_kupac k WHERE web_kupac_id > 0 ORDER BY web_kupac_id ASC");
			}else{
				$kupaci = $conn->select("SELECT DISTINCT web_kupac_id, naziv, pib, ime, prezime, flag_vrsta_kupca, email, adresa, telefon, mesto FROM web_kupac WHERE web_kupac_id > 0 ORDER BY web_kupac_id ASC");
			}
		}else{
			return Redirect::to(Options::base_url());
		}

		$export_file = $partner_slug."_".$kind.".xls";
		ob_end_clean();
		ini_set('zlib.output_compression','Off');
		  
		header('Pragma: public');
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");                 // Date in the past  
		header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1
		header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1
		header("Pragma: no-cache");
		header("Expires: 0");
		header('Content-Transfer-Encoding: none');
		header('Content-Type: application/vnd.ms-excel; charset=utf-8;');                // This should work for IE & Opera
		header("Content-type: application/x-msexcel");                    // This should work for the rest
		header('Content-Disposition: attachment; filename="'.basename($export_file).'"');

		// header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		// header("Content-disposition: attachment; filename=".$partner_slug."_partneri.xls");


		if($kind == 'partneri'){
			echo 'Naziv'."\t" . 'Adresa' . "\t" . 'Mesto'."\t" . 'Telefon'."\t" . 'PIB'."\t" . 'Matični broj'."\t" . 'Email'."\t" . 'ID'. "\t\n";
			foreach($partners as $partner)
			{
				echo self::clear($partner->naziv)."\t" . self::clear($partner->adresa) . "\t" . self::clear($partner->mesto)."\t" . self::clear($partner->telefon)."\t" . self::clear($partner->pib)."\t" . self::clear($partner->broj_maticni)."\t" . ($partner->mail == 'NULL' ? '' : self::clear($partner->mail))."\t" . ($partner->narudzbina_count > 0 ? $partner->partner_id : ''). "\t\n";
			}
		}elseif($kind == 'kupci'){
			echo 'Naziv'."\t" . 'Vrsta' . "\t" . 'Adresa' . "\t" . 'Mesto'."\t" . 'Telefon'."\t" . 'Email'. "\t\n";
			foreach($kupaci as $kupac)
			{
				echo self::clear($kupac->flag_vrsta_kupca == 0 ? ($kupac->ime.' '.$kupac->prezime) : ($kupac->naziv.' '.$kupac->pib))."\t" . $kupac->flag_vrsta_kupca . "\t" . self::clear($kupac->adresa) . "\t" . self::clear($kupac->mesto)."\t" . self::clear($kupac->telefon)."\t" . ($kupac->email == 'NULL' ? '' : self::clear($kupac->email)). "\t\n";
			}
		}
	}

	public function clear($string){
		return str_replace(array(',',';'), array('',''), preg_replace('/\s+/', ' ', trim($string)));
	}
}