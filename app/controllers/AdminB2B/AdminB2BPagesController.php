<?php

class AdminB2BPagesController extends Controller {

   public function stranice($stranica_id,$jezik_id=1){
        
        $query_list_pages=DB::table('web_b2b_seo')->orderBy('rb_strane','asc')->get();
        $query_stranica=DB::table('web_b2b_seo')->where('web_b2b_seo_id',$stranica_id)->first();
        $stranice=DB::table('web_b2b_seo')->where('web_b2b_seo_id',$stranica_id)->get();
        if(!is_null($query_stranica)){
            $stranica_jezik=DB::table('web_b2b_seo_jezik')->where(array('web_b2b_seo_id'=>$query_stranica->web_b2b_seo_id, 'jezik_id'=>$jezik_id))->first();
        }

        $data=array(
        "strana"=>'b2b_stranice',
        "title"=>"Stranice",
        "query_list_pages"=>$query_list_pages,
        "stranice"=>$stranice,
        "stranica"=>!is_null($query_stranica) ? $query_stranica->naziv_stranice : 'nova',
        "jezik_id"=>$jezik_id,
        "jezici" => DB::table('jezik')->where('aktivan',1)->get()
        );

        if($stranica_id==0 || is_null($query_stranica)){
            $data['jezik_id']=$jezik_id;
            $data['naziv']='';
            $data['seo_title']='';
            $data['content']='';
            $data['keywords']='';
            $data['desription']='';
            $data['status']=1;
            $data['flag_page']=0;
            $data['web_b2b_seo_id']=0;
            $data['flag_b2b'] = 0;
            $data['tip_artikla_id']=-1;
            $data['grupa_pr_id']=-1;
            $data['disable']=0;
            $data['menu_top']=false;
            $data['header_menu']=false;
            $data['footer']=false;
            $data['b2b_header']=false;
            $data['b2b_footer']=false;
            $data['parent_id']=null;
        } else {
            $data['jezik_id']=$jezik_id;
            $data['naziv']=$query_stranica->title;
            $data['seo_title']= $stranica_jezik ? $stranica_jezik->title : '';
            $data['content']= $stranica_jezik ? $stranica_jezik->sadrzaj : '';
            $data['keywords']= $stranica_jezik ? $stranica_jezik->keywords : '';
            $data['desription']=$stranica_jezik ? $stranica_jezik->description : '';
            $data['status']=2;
            $data['flag_page']=$query_stranica->flag_page;
            $data['web_b2b_seo_id']=$query_stranica->web_b2b_seo_id;
            $data['flag_b2b']=$query_stranica->flag_b2b_show;
            $data['tip_artikla_id']=$query_stranica->tip_artikla_id;
            $data['grupa_pr_id']=$query_stranica->grupa_pr_id;
            $data['disable']=$query_stranica->disable;
            $data['menu_top']=$query_stranica->menu_top ? true : false;
            $data['header_menu']=$query_stranica->header_menu ? true : false;
            $data['footer']=$query_stranica->footer ? true : false;
            $data['b2b_header']=$query_stranica->b2b_header ? true : false;
            $data['b2b_footer']=$query_stranica->b2b_footer ? true : false;
            $data['parent_id']=$query_stranica->parent_id;
        }        

    	return View::make('adminb2b/pages/b2b_stranice', $data);
    }

    public function store(){
        
    $web_b2b_seo_id=Input::get('web_b2b_seo_id');
        $disable = 0;
        if($web_b2b_seo_id != 0){
            $web_b2b_seo = DB::table('web_b2b_seo')->where('web_b2b_seo_id',$web_b2b_seo_id)->first();
            if($web_b2b_seo){
                $disable = $web_b2b_seo->disable;
            }else{
                return Redirect::to(AdminOptions::base_url().'admin/b2b/b2b_stranice/nova');
            }
        }
        $status=Input::get('status');
        $naslov=$disable==0 ? Input::get('page_name') : $web_b2b_seo->title;
        $content= $disable==0 ? Input::get('content') : null;
        $seo_title=Input::get('seo_title');
        $keywords=Input::get('keywords');
        $description=Input::get('description');
        $header_menu=Input::get('header_menu');
        $footer_menu=Input::get('footer_menu');
        $top_menu=Input::get('top_menu');
        $b2b_header_menu=Input::get('b2b_header_menu');
        $b2b_footer_menu=Input::get('b2b_footer_menu');
        $jezik_id=Input::get('jezik_id');
        $naziv_stranice=AdminOptions::slug_trans($naslov);
        $tip_artikla_id= $disable==0 ? Input::get('tip_artikla_id') : -1;
        $grupa_pr_id= $disable==0 ? Input::get('grupa_pr_id') : -1;
        $parent_id= $disable==0 ? (Input::get('parent_id') > 0 ? Input::get('parent_id') : null ) : null;
        
    
        $data=array(
        'naziv_stranice'=>$naziv_stranice,
        'flag_page'=>0,
        'title'=>$naslov,
        'flag_b2b_show'=>0,
        'tip_artikla_id' => $tip_artikla_id,
        'grupa_pr_id' => $grupa_pr_id,
        'parent_id' => $parent_id,
        'menu_top' => isset($top_menu) ? 1 : 0,
        'header_menu' => isset($header_menu) ? 1 : 0,
        'footer' => isset($footer_menu) ? 1 : 0,
        'b2b_header' => isset($b2b_header_menu) ? 1 : 0,
        'b2b_footer' => isset($b2b_footer_menu) ? 1 : 0,

        );

        $seo_data = array('seo_title'=>$seo_title,'description'=>$description,'keywords'=>$keywords);
        $validator = Validator::make($seo_data, array('seo_title' => 'max:60','description' => 'max:320', 'keywords' => 'max:159'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/b2b/b2b_stranice/'.$web_b2b_seo_id.($jezik_id != 1 ? '/'.$jezik_id : ''))->withInput()->withErrors($validator->messages());
        }

        if($web_b2b_seo_id==0){
            DB::table('web_b2b_seo')->insert($data);
            $web_b2b_seo_id = DB::table('web_b2b_seo')->where('naziv_stranice',$naziv_stranice)->pluck('web_b2b_seo_id');
            AdminSupport::saveLog('B2B_PRODAVNICA_DODAJ', array(DB::table('web_b2b_seo')->max('web_b2b_seo_id')));
        }
        else {
            DB::table('web_b2b_seo')->where('web_b2b_seo_id',$web_b2b_seo_id)->update($data);
            AdminSupport::saveLog('B2B_PRODAVNICA_IZMENI', array($web_b2b_seo_id));
        }
        $query = DB::table('web_b2b_seo_jezik')->where(array('web_b2b_seo_id'=>$web_b2b_seo_id, 'jezik_id'=>$jezik_id));
        $jezik_data = array('sadrzaj'=>$content,'title'=>$seo_title,'description'=>$description,'keywords'=>$keywords);
        if(!is_null($query->first())){
            $query->update($jezik_data);
        }else{
            if($content || $seo_title || $description || $keywords){
                $jezik_data['web_b2b_seo_id'] = $web_b2b_seo_id;
                $jezik_data['jezik_id'] = $jezik_id;
                DB::table('web_b2b_seo_jezik')->insert($jezik_data);
            }
        }
        
        
        return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/b2b_stranice/'.$web_b2b_seo_id.($jezik_id != 1 ? '/'.$jezik_id : ''))->with('message','Uspešno ste sačuvali podatke.');

}
    public function delete_page(){

        $web_b2b_seo_id = Input::get('web_b2b_seo_id');
        if($web_b2b_seo_id != 0){
            AdminSupport::saveLog('B2B_PRODAVNICA_OBRISI', array($web_b2b_seo_id));
            DB::table('web_b2b_seo')->where(array('web_b2b_seo_id'=>$web_b2b_seo_id,'disable'=>0))->delete();            
        }
        return Redirect::to('admin/b2b/b2b_stranice/0')->with('message','Uspešno ste obrisali stranicu.');

    }
    public function delete_image(){
        $link=".".Input::get('url');
        if(File::exists($link)){
        File::delete($link);
        AdminSupport::saveLog('B2B_UPLOADOVANE_SLIKE_OBRISI');
        echo $link." Ok";
        }
        else {
        echo $link." Nije uredu";
        }
        
        
    }
    public function image_upload(){
            $slika_ime=$_FILES['img']['name'];
            $slika_tmp_ime=$_FILES['img']['tmp_name'];
            move_uploaded_file($slika_tmp_ime,"images/upload/$slika_ime");
            AdminSupport::saveLog('B2B_STRANICE_SLIKA_POSTAVI');
            return Redirect::back();
          // echo Admin_model::upload_directory();
    }   
}