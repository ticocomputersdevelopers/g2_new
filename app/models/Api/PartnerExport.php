<?php

class PartnerExport {


	public static function string_format($string){
		// return preg_replace('/[^A-Za-z0-9 !%&()=*,.+-_@:;<>\'čćžšđČĆŽŠĐ\-]/', '',iconv(mb_detect_encoding($string, mb_detect_order(), true), "UTF-8//TRANSLIT//IGNORE",$string));
		return $string;
	}
	public static function xml_node($xml,$name,$value,$parent){
		$xmltag = $xml->createElement($name);
	    $xmltagText = $xml->createTextNode($value);
	    $xmltag->appendChild($xmltagText);
	    $parent->appendChild($xmltag);
	}

	public static function slike($roba_id){
        return array_map('current',DB::table('web_slika')->select('putanja')->where('roba_id',$roba_id)->limit(8)->orderBy('akcija','desc')->get());	
	}

	public static function characteristics($roba_id,$web_flag_karakteristike){
		$karak_arr = array();
		if($web_flag_karakteristike == 1){
			$generisane = DB::select("SELECT (SELECT naziv FROM grupa_pr_naziv WHERE grupa_pr_naziv_id = wrk.grupa_pr_naziv_id) as naziv, vrednost FROM web_roba_karakteristike wrk WHERE roba_id = ".$roba_id." ORDER BY rbr ASC");
			$karak_arr[0] = (object) array('group' => 'Osnovne karakteristike', 'characteristic' => array());
			foreach($generisane as $row){
				$karak_arr[0]->characteristic[] = (object) array('name' => $row->naziv, 'value' => $row->vrednost);
			}
		}
		elseif($web_flag_karakteristike == 2){
			$dobavljac = DB::select("SELECT karakteristika_grupa, karakteristika_naziv, karakteristika_vrednost FROM dobavljac_cenovnik_karakteristike WHERE roba_id = ".$roba_id." ORDER BY dobavljac_cenovnik_karakteristike_id ASC");
			$check_groups = false;
			foreach($dobavljac as $row){
				if(!is_null($row->karakteristika_grupa) && $row->karakteristika_grupa != '' && (self::index_charac_group($karak_arr,$row->karakteristika_grupa) === false)){
					$karak_arr[] = (object) array('group' => $row->karakteristika_grupa, 'characteristic' => array());
					$check_groups = true;
				}else{
					if(!$check_groups && (self::index_charac_group($karak_arr,'Osnovne karakteristike') === false)){
						$karak_arr[] = (object) array('group' => 'Osnovne karakteristike', 'characteristic' => array());
					}
				}

				if(!is_null($row->karakteristika_grupa) && $row->karakteristika_grupa != ''){
					$karak_arr[self::index_charac_group($karak_arr,$row->karakteristika_grupa)]->characteristic[] = (object) array('name' => $row->karakteristika_naziv, 'value' => $row->karakteristika_vrednost);
				}else{
					$karak_arr[self::index_charac_group($karak_arr,'Osnovne karakteristike')]->characteristic[] = (object) array('name' => $row->karakteristika_naziv, 'value' => $row->karakteristika_vrednost);					
				}
			}
		}
		return $karak_arr;
	}

	public static function index_charac_group($charac_arr,$charac_group){
		foreach($charac_arr as $key => $value){
			if($charac_group == $value->group){
				return $key;
				break;
			}
		}
		return false;
	}

}