<?php
namespace IsSkala;

use DB;
use All;

class Image {

	public static function table_body($images,array $mappedArticles){
		$result_arr = array();

		$image_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
		foreach($images as $image) {
			if(isset($mappedArticles[$image->id_proizvoda])){
			    $image_id++;

			    $roba_id = intval($mappedArticles[$image->id_proizvoda]);
			    $image_type = Support::image_type($image->putanja);

			    if(!is_null($roba_id) && $roba_id != '' && !is_null($image_type)){
				    $akcija = 0;
					$putanja = "images/products/big/".strval($image_id).".".$image_type;

					$result_arr[] = "(".strval($image_id).",".strval($roba_id).",(NULL)::bytea,".strval($akcija).",-1,NULL,(NULL)::integer,1,'".$putanja."',NULL,0,'".strval($image->id_slike)."')";
				}
			}


		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_slika'"));
		$table_temp = "(VALUES ".$table_temp_body.") web_slika_temp(".implode(',',$columns).")";

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="web_slika_id" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = web_slika_temp.".$col."";
			}
		}
		// DB::statement("UPDATE web_slika t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=web_slika_temp.id_is");

		//insert
		DB::statement("INSERT INTO web_slika (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM web_slika t WHERE t.id_is=web_slika_temp.id_is))");

		DB::statement("UPDATE web_slika wsu SET akcija = 1 FROM (SELECT web_slika_id, roba_id ,row_number() over (PARTITION BY roba_id ORDER BY web_slika_id) AS rn FROM web_slika) ws WHERE ws.rn = 1 AND wsu.web_slika_id = ws.web_slika_id and akcija = 0");

		DB::statement("SELECT setval('web_slika_web_slika_id_seq', (SELECT MAX(web_slika_id) FROM web_slika) + 1, FALSE)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_slika'"));
		$table_temp = "(VALUES ".$table_temp_body.") web_slika_temp(".implode(',',$columns).")";

		DB::statement("DELETE FROM web_slika WHERE id_is NOT IN (SELECT id_is FROM ".$table_temp.") AND id_is IS NOT NULL AND id_is <> ''");
	}

	public static function new_images($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_slika'"));
		$table_temp = "(VALUES ".$table_temp_body.") web_slika_temp(".implode(',',$columns).")";

		$result = DB::select("SELECT putanja, id_is FROM ".$table_temp." WHERE web_slika_temp.id_is NOT IN(SELECT id_is FROM web_slika WHERE id_is <> '' AND id_is IS NOT NULL)");

		$images = array();
		foreach($result as $row){
			$images[$row->id_is] = $row->putanja;
		}
		return $images;
	}

}