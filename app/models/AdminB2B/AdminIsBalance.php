<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 7200);

use IsBalance\ApiData;
use IsBalance\Article;
use IsBalance\Stock;
use IsBalance\Partner;
use IsBalance\PartnerCard;
use IsBalance\PartnerSeller;
use IsBalance\SalesPerson;
use IsBalance\Support;
use IsBalance\Price;

class AdminIsBalance {
    // http://g2labor.tico.rs/auto-import-is/5a8dc096af77e07eb75a34f6b82dc9f8
    public static function execute(){

        try {
            $token = ApiData::getToken();
           

             if(!is_null($token)){
                
                // $grupe=ApiData::data($token,'groups');
                // $podgrupe=ApiData::data($token,'subgroups');
                // $proizvodjac=ApiData::data($token,'manufactures');
                
                $partners=ApiData::data($token,'clients');
                //All::dd($partners);
                $resultPartner = Partner::table_body($partners);
                Partner::query_insert_update($resultPartner->body,array('sifra','naziv','adresa','mesto','imenik_id','sifra_connect'));

                $mappedPartners = Support::getMappedSellersPartners();
                $mappedSellers = Support::getMappedSellers();
                
                $komercijalista=array_unique(ApiData::data($token,'agents'), SORT_REGULAR);
               // All::dd($komercijalista);
                $resultKomercijalista = SalesPerson::table_body($komercijalista);
                SalesPerson::query_insert_update($resultKomercijalista->body);
                
                $resultPartnerSeller = PartnerSeller::table_body($komercijalista,$mappedSellers,$mappedPartners);
                PartnerSeller::query_insert_update($resultPartnerSeller->body);
 
                $articles = array();
                foreach(range(1,100) as $page){
                    $articles = array_merge($articles,ApiData::articles($token,$page));
                
                } 
                
                $resultArticle = Article::table_body($articles);
                Article::query_insert_update($resultArticle->body,array('flag_prikazi_u_cenovniku','id_is','flag_aktivan','flag_cenovnik','naziv_web','naziv','proizvodjac_id'));      
                Article::query_update_unexists($resultArticle->body);
                // Support::entityDecode();
                Support::updateArticles();
                Support::hemikalije_model();

                // LAGER
                $articlesStock=ApiData::stock($token);
                $mapped_articles = Support::getMappedArticles();
                $resultArticleStock = Stock::table_body($articlesStock,$mapped_articles);
                Stock::query_insert_update($resultArticleStock->body,array('kolicina'));
              
                $mappedArticlesIdIs = Support::getMappedArticlesIdIs();
                $cene=ApiData::price($token);
               // All::dd($cene);
                $resultPrice = Price::table_body($cene,$mappedArticlesIdIs);
                $roba_ids=Price::query_insert_update($resultPrice->body,array('cena'));
                Support::postUpdate($roba_ids);
            }
            else{
                AdminB2BIS::saveISLog('false');
                return (object) array('success'=>false);
            }

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);

        } catch (Exception $e) {
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());            
        }
    }

    public static function executePartnerFull(){

        try {
            $token = ApiData::getToken();
            if(!is_null($token)){
                
                //partner
                $partners=array();
                $partnarIds=array();
                $apiPartners = ApiData::partners($token, 1);
                $pages=intdiv($apiPartners['TotalRows'], 100) + (fmod($apiPartners['TotalRows'],100) >0 ? 1 : 0);
                $pages=1;
                foreach (range(1, $pages) as $page) {
                    $apiPartners = ApiData::partners($token,$page,true);
                    foreach($apiPartners['Rows'] as $row){
                        if(!in_array($row['CustomerId'],$partnarIds)){
                            $partnarIds[] = $row['CustomerId'];
                            $partners[] = $row;
                        }
                    }
                }
                $resultPartner = Partner::table_body($partners);
                Partner::query_insert_update($resultPartner->body,array('sifra','naziv','adresa','mesto','rabat'));
               
            }
            else{
                AdminB2BIS::saveISLog('false');
                return (object) array('success'=>false);
            }

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);

        } catch (Exception $e) {
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());            
        }
    }


}