<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsNSSport\DBData;
use IsNSSport\Support;
use IsNSSport\Group;
use IsNSSport\Manufacturer;
use IsNSSport\Article;
use IsNSSport\Stock;
use IsNSSport\Partner;
use IsNSSport\Course;
use IsNSSport\Image;
use IsNSSport\SendArticleData;


class AdminIsNSSport {

    public static function execute(){
        // try {

            //articles
            $articles = DBData::articles('Maloprodaja');

            // //groups
            // $groups = Support::filteredGroups($articles);
            // Support::saveGroups($groups);

            //articles
            $resultArticle = Article::table_body($articles);
            Article::query_insert_update($resultArticle->body,array('grupa_pr_id','web_opis','barkod','racunska_cena_nc','racunska_cena_end','mpcena','web_cena','proizvodjac_id','tarifna_grupa_id','flag_prikazi_u_cenovniku'));
            Article::query_update_unexists($resultArticle->body);
            $mappedArticles = Support::getMappedArticles();

            //stock
            $resultStock = Stock::table_body($articles,$mappedArticles);
            Stock::query_insert_update($resultStock->body);


            // //partner
            // $partners = DBData::partners();
            // $resultPartner = Partner::table_body($partners,array('id_kategorije'));
            // Partner::query_insert_update($resultPartner->body);

            // //images
            // $images = DBData::images();
            // $resultImage = Image::table_body($images);
            // $new_images = Image::new_images($resultImage->body);
            // Image::query_insert_update($resultImage->body);
            // Image::query_update_unexists($resultImage->body);
            // Support::save_image_files($images,$new_images);

            // //send article data
            // SendArticleData::query_insert_update();

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        // }catch (Exception $e){
        //     AdminB2BIS::saveISLog('false');
        //     return (object) array('success'=>false,'message'=>$e->getMessage());
        // }
    }



}