<?php
namespace IsBalance;
use DB;

class Stock {
	public static function table_body($articles,$mappedArticles){

		$result_arr = array();
		$roba_id = -1;
		foreach($articles as $article) {
    		$roba_id--;

			$sifra_is = $article->itemCode;
			$id_is = $article->itemId;
			$roba_id = isset($mappedArticles[strval($sifra_is)]) ? $mappedArticles[strval($sifra_is)] : null;
			$kolicina = intval($article->quantity) - (intval($article->reservedQuantity));
			$stockroom=	$article->stockId;
			$stockroom = $article->stockId == 1 ? 1 : 4;
			if(isset($roba_id)){
				if($kolicina < 0){
					$kolicina = 0;
				}	

				$result_arr[] = "(".strval($roba_id).",".intval($stockroom).",0,0,0,".strval($kolicina).",0,0,0,0,0,0,0,0,(NULL)::integer,0,0,0,0,0,2014,-1,0,0,'".strval($id_is)."')";
			}
		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="sifra_is"){
		    	$updated_columns[] = "".$col." = lager_temp.".$col."";
			}
		}

		DB::statement("UPDATE lager t SET kolicina = lager_temp.kolicina FROM ".$table_temp." WHERE t.roba_id=lager_temp.roba_id AND t.orgj_id=lager_temp.orgj_id AND t.poslovna_godina_id=lager_temp.poslovna_godina_id AND (select flag_zakljucan from roba where roba_id = t.roba_id limit 1) = 'false'");

		$where = "";
		if(DB::table('lager')->count() > 0){
			$where .= " WHERE (roba_id, orgj_id, poslovna_godina_id) NOT IN (SELECT roba_id, orgj_id, poslovna_godina_id FROM lager)";
		}

		//insert
		DB::statement("INSERT INTO lager (roba_id, orgj_id, kolicina, poslovna_godina_id) SELECT roba_id, orgj_id, kolicina, poslovna_godina_id FROM ".$table_temp.$where."");

	}

}