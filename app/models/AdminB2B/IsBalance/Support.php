<?php
namespace IsBalance;

use DB;
use Groups;


class Support {


	public static function getPartnerId($sifra_kupca_logik){
		$partner = DB::table('partner')->where('id_is',$sifra_kupca_logik)->first();
		if(!is_null($partner)){
			return $partner->partner_id;
		}
		return null;
	}
	public static function getMappedSellers(){
		$mapped = array();
		$sellers = DB::table('imenik')->select('imenik_id','ime','prezime')->whereNotNull('ime')->whereNotNull('prezime')->where('kvota',32.00)->get();
		foreach($sellers as $seller){
			$mapped[strval(trim($seller->ime.' '.$seller->prezime))] = $seller->imenik_id;
		}
		return $mapped;
	}
	public static function getMappedSellersPartners(){
		$mapped = array();

		$partners = DB::table('partner')->select('sifra','id_is','partner_id')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($partners as $partner){
			$mapped[trim($partner->id_is)] = $partner->partner_id;
		}

		return $mapped;
	}
	public static function getTarifnaGrupaId($naziv_tarifne_grupe,$vrednost_tarifne_grupe=20){
		$tg = DB::table('tarifna_grupa')->where(array('porez'=>$vrednost_tarifne_grupe))->first();

		if(is_null($tg)){
			$next_id = DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1;
			DB::table('tarifna_grupa')->insert(array(
				'tarifna_grupa_id' => $next_id,
				'sifra' => $next_id,
				'naziv' => substr($naziv_tarifne_grupe,0,99),
				'porez' => $vrednost_tarifne_grupe,
				'active' => 1,
				'default_tarifna_grupa' => 0,
				'tip' => substr($vrednost_tarifne_grupe,0,19)
				));
			$tg = DB::table('tarifna_grupa')->where('porez',$vrednost_tarifne_grupe)->first();
		}
		return $tg->tarifna_grupa_id;
	}

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		$proizvodjac_id=DB::table('proizvodjac')->max('proizvodjac_id')+1;
		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'proizvodjac_id' => $proizvodjac_id,
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}

	public static function getGrupaIdByParent($parent_group_name,$group_name){
		$parent_group_name = trim($parent_group_name);
		$group_name = trim($group_name);
		$groups = DB::table('grupa_pr')->where('grupa',$group_name)->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();
		$parent_groups = DB::table('grupa_pr')->where('grupa',$parent_group_name)->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();

		$main_group_id = self::getNewGrupaId('PANTEON GRUPE OSTALO');

		$grupa_pr_id = $main_group_id;
		foreach($groups as $group){
			foreach($parent_groups as $parent_group){
				if($parent_group->grupa_pr_id == $group->parrent_grupa_pr_id){
					$grupa_pr_id = $group->grupa_pr_id;
					break;
				}
			}
			if($grupa_pr_id != $main_group_id){
				break;
			}
		}
		return $grupa_pr_id;
	}

	public static function getGrupaId($group_name){
		$group_name = trim($group_name);
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->whereNotNull('sifra_is')->where('sifra_is','!=','')->first();

		if(is_null($group)){
			$grupa_pr_id = self::getNewGrupaId('PANTEON GRUPE');
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
		}
		return $grupa_pr_id;
	}

	public static function getNewGrupaId($group_name){
		$group_name = trim($group_name);
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->whereNotNull('sifra_is')->where('sifra_is','!=','')->first();

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name,
				'parrent_grupa_pr_id' => 0,
				'sifra' => $grupa_pr_id,
				'sifra_is' => $group_name
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
		}
		return $grupa_pr_id;
	}

	public static function saveSingleGroup($group_name,$group_parent=null,$update_parent=false){
		$group_name = trim($group_name);
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->whereNotNull('sifra_is')->where('sifra_is','!=','')->first();

		$group_parent_id = self::getGrupaId('PANTEON GRUPE');
		if(!is_null($group_parent) && trim($group_parent) != ''){
			$group_parent_id = self::getGrupaId(trim($group_parent));
		}

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			if(is_null($grupa_pr_id)){
				$grupa_pr_id = 1;
			}
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name,
				'parrent_grupa_pr_id' => $group_parent_id,
				'sifra' => $grupa_pr_id,
				'sifra_is' => $group_name
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
			if($update_parent){
				DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->update(array('parrent_grupa_pr_id' => $group_parent_id));				
			}
		}
		return $grupa_pr_id;
	}

	public static function filteredGroups($articles){
		$groups = array();
		foreach($articles as $article) {
			if(isset($article->acClassif2) && $article->acClassif2 != ''){
				$groups[mb_convert_encoding($article->acClassif2,mb_detect_encoding($article->acClassif2),"UTF-8")] = mb_convert_encoding($article->acClassif,mb_detect_encoding($article->acClassif),"UTF-8");
			}
		}
		return $groups;
	}

	public static function parentGroups($allgroups,$group,&$tree){
		if(!is_null($group) && $group != ''){
			$tree[] = $group;
			if(isset($allgroups[$group]) && $allgroups[$group] != ''){
				self::parentGroups($allgroups,$allgroups[$group],$tree);
			}
		}
	}

	public static function saveGroups($groups){
		foreach($groups as $grupa => $nadgrupa) {
			if($grupa != $nadgrupa){
				$tree = array($grupa);
				self::parentGroups($groups,$nadgrupa,$tree);
				for ($i=(count($tree)-1);$i>=0;$i--) {
					self::saveSingleGroup($tree[$i],(isset($tree[$i+1]) ? $tree[$i+1] : null),true);
				}
			}
		}
	}

	public static function getPartnerCategoryId($category_name){
		$category_name = trim($category_name);
		$category = DB::table('partner_kategorija')->where('naziv',$category_name)->first();

		if(is_null($category)){
			$id_kategorije = DB::select("SELECT MAX(id_kategorije) + 1 AS max FROM partner_kategorija")[0]->max;
			if(is_null($id_kategorije)){
				$id_kategorije = 1;
			}
			DB::table('partner_kategorija')->insert(array(
				'id_kategorije' => $id_kategorije,
				'naziv' => $category_name,
				'rabat' => 0,
				'active' => 1
				));
		}else{
			$id_kategorije = $category->id_kategorije;
		}
		return $id_kategorije;
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->sifra_is] = $article->roba_id;
		}
		return $mapped;
	}
	public static function getMappedArticlesIdIs(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->id_is] = $article->roba_id;
		}
		return $mapped;
	}
	public static function getMappedPartners(){
		$mapped = array();

		$partners = DB::table('partner')->select('sifra','partner_id')->whereNotNull('sifra')->where('sifra','!=','')->get();
		foreach($partners as $partner){
			$mapped[trim($partner->sifra)] = $partner->partner_id;
		}

		return $mapped;
	}

	public static function entityDecode(array $id_iss=[]){

		DB::statement("update roba set naziv = replace(naziv, '&#353;', 'š')");
		DB::statement("update roba set naziv = replace(naziv, '&#273;', 'đ')");
		DB::statement("update roba set naziv = replace(naziv, '&#269;', 'č')");
		DB::statement("update roba set naziv = replace(naziv, '&#263;', 'ć')");
		DB::statement("update roba set naziv = replace(naziv, '&#382;', 'ž')");
		DB::statement("update roba set naziv = replace(naziv, '&#352;', 'Š')");
		DB::statement("update roba set naziv = replace(naziv, '&#272;', 'Đ')");
		DB::statement("update roba set naziv = replace(naziv, '&#268;', 'Č')");
		DB::statement("update roba set naziv = replace(naziv, '&#262;', 'Ć')");
		DB::statement("update roba set naziv = replace(naziv, '&#381;', 'Ž')");
		DB::statement("update roba set naziv = replace(naziv, '&#8220;', '\"')");
		DB::statement("update roba set naziv = replace(naziv, '&#8211;', '-')");
		DB::statement("update roba set naziv = replace(naziv, '&#176;', '°')");
		DB::statement("update roba set naziv = replace(naziv, '\', '')");
		

		DB::statement("update roba set naziv_web = replace(naziv_web, '&#353;', 'š')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#273;', 'đ')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#269;', 'č')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#263;', 'ć')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#382;', 'ž')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#352;', 'Š')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#272;', 'Đ')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#268;', 'Č')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#262;', 'Ć')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#381;', 'Ž')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#8220;', '\"')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#8211;', '-')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '&#176;', '°')");
		DB::statement("update roba set naziv_web = replace(naziv_web, '\', '')");

		//PARTNERI
		DB::statement("update partner set naziv = replace(naziv, '&#353;', 'š'), adresa = replace(adresa, '&#353;', 'š'), mesto = replace(mesto, '&#353;', 'š'), naziv_puni = replace(naziv_puni, '&#353;', 'š')");
		DB::statement("update partner set naziv = replace(naziv, '&#273;', 'đ'), adresa = replace(adresa, '&#273;', 'đ'), mesto = replace(mesto, '&#273;', 'đ'), naziv_puni = replace(naziv_puni, '&#273;', 'đ')");
		DB::statement("update partner set naziv = replace(naziv, '&#269;', 'č'), adresa = replace(adresa, '&#269;', 'č'), mesto = replace(mesto, '&#269;', 'č'), naziv_puni = replace(naziv_puni, '&#269;', 'č')");
		DB::statement("update partner set naziv = replace(naziv, '&#263;', 'ć'), adresa = replace(adresa, '&#263;', 'ć'), mesto = replace(mesto, '&#263;', 'ć'), naziv_puni = replace(naziv_puni, '&#263;', 'ć')");
		DB::statement("update partner set naziv = replace(naziv, '&#382;', 'ž'), adresa = replace(adresa, '&#382;', 'ž'), mesto = replace(mesto, '&#382;', 'ž'), naziv_puni = replace(naziv_puni, '&#382;', 'ž')");
		DB::statement("update partner set naziv = replace(naziv, '&#352;', 'Š'), adresa = replace(adresa, '&#352;', 'Š'), mesto = replace(mesto, '&#352;', 'Š'), naziv_puni = replace(naziv_puni, '&#352;', 'Š')");
		DB::statement("update partner set naziv = replace(naziv, '&#272;', 'Đ'), adresa = replace(adresa, '&#272;', 'Đ'), mesto = replace(mesto, '&#272;', 'Đ'), naziv_puni = replace(naziv_puni, '&#272;', 'Đ')");
		DB::statement("update partner set naziv = replace(naziv, '&#268;', 'Č'), adresa = replace(adresa, '&#268;', 'Č'), mesto = replace(mesto, '&#268;', 'Č'), naziv_puni = replace(naziv_puni, '&#268;', 'Č')");
		DB::statement("update partner set naziv = replace(naziv, '&#262;', 'Ć'), adresa = replace(adresa, '&#262;', 'Ć'), mesto = replace(mesto, '&#262;', 'Ć'), naziv_puni = replace(naziv_puni, '&#262;', 'Ć')");
		DB::statement("update partner set naziv = replace(naziv, '&#381;', 'Ž'), adresa = replace(adresa, '&#381;', 'Ž'), mesto = replace(mesto, '&#381;', 'Ž'), naziv_puni = replace(naziv_puni, '&#381;', 'Ž')");		
    }

	public static function convert($text){
		// $text = preg_replace('/[^a-zA-Z0-9\ \!\%\&\(\)\=\*\/\,\.\+\-\_\@\?\:\;\<\>\'\"\č\ć\ž\š\đ\Č\Ć\Ž\Š\Đ]/', '',$text);
		// $text = str_replace("'", '"', $text);
		// $text = iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$text);
		// $text = pg_escape_string($text);
		// return $text;

		$text = str_replace(array("è","ð","æ","Æ","'"),array("č","đ","ć","Ć",'"'),$text);

        /* Only do the slow convert if there are 8-bit characters */
        if ( !preg_match("/[\200-\237]/", $text) && !preg_match("/[\241-\377]/", $text) )
               return $text;

        // decode three byte unicode characters
          $text = preg_replace_callback("/([\340-\357])([\200-\277])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-224)*4096+(ord($matches[2])-128)*64+(ord($matches[3])-128)).\';\';'),
                    $text);

        // decode two byte unicode characters
          $text = preg_replace_callback("/([\300-\337])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-192)*64+(ord($matches[2])-128)).\';\';'),
                    $text);
        $text = str_replace("\n","<br>",$text);
        return pg_escape_string($text);
	}
	public static function updateArticles(){
		
		DB::statement("
		    UPDATE roba 
		    SET roba_flag_cene_id = 
		        CASE 
		            WHEN roba_id NOT IN (SELECT roba_id FROM lager) OR (SELECT COALESCE(SUM(kolicina), 0) FROM lager WHERE lager.roba_id = roba.roba_id) = 0 THEN 6
		            WHEN (SELECT COALESCE(SUM(kolicina), 0) FROM lager WHERE lager.roba_id = roba.roba_id) > 0 THEN 1
		            ELSE roba.roba_flag_cene_id
		        END
		");

	}
	public static function postUpdate($roba_ids){

		foreach ($roba_ids as $roba_id) {
			$web_cena = DB::table("cenovnik_partner")->where('roba_id',$roba_id)->where('discountgroup','A')->pluck('cena');
			$web_cena = $web_cena*1.2;
			
			if($web_cena>0){
				DB::statement("UPDATE roba SET web_cena =".number_format($web_cena, 2, '.', '')." WHERE roba_id=".$roba_id." ");
			}
		}
	}

	public static function roba_cene($id_is){
		return DB::table('roba')->select('racunska_cena_end','stara_cena')->where('id_is',$id_is)->first();
	}

	public static function lager_magacin($roba_id,$orgj_id){
		return DB::table('lager')->select('kolicina','poc_kolicina')->where(array('roba_id'=>$roba_id,'orgj_id'=>$orgj_id))->first();
	}
	public static function hemikalije_model(){
		 DB::statement("UPDATE roba set cas_number = model WHERE grupa_pr_id = 53");
	}

}