<?php
namespace IsBalance;

use DB;

class SalesPerson {

	public static function table_body($komercijalisti){

		$result_arr = array();

		$komercijalista_id = DB::select("SELECT nextval('imenik_magacin_imenik_magacin_id_seq')")[0]->nextval;
		foreach($komercijalisti as $komercijalista) {
			
			if(!empty($komercijalista->id)){
				
		    	$komercijalista_id++;
	            $id_is=$komercijalista->id;
	            $naziv = explode(" ", $komercijalista->name);
	            $ime =$naziv[0];
	            $prezime = $naziv[1];
	            $mail=isset($komercijalista->email) ? substr($komercijalista->email,0,100) : null;
	            $mobilni=$komercijalista->businessPhone;

		        $result_arr[] = "(".strval($komercijalista_id).",NULL,'".trim($ime)."',NULL,'".trim($prezime)."',NULL,NULL,NULL,NULL,NULL,NULL,'".trim($mobilni)."',(NULL)::date,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,1,0,0,0,0,NULL,-1,NULL,'".$mail."',-1,(NULL)::date,(NULL)::bytea,NULL,NULL,'',0,-1,32,0,0,0,0,0,0,1,1,1,1,0,(NULL)::integer,(NULL)::date,0,'".$id_is."')";
		    }
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='imenik'"));
        $table_temp = "(VALUES ".$table_temp_body.") imenik_temp(".implode(',',$columns).")";

       // DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="imenik_id" ){
                $updated_columns[] = "".$col." = imenik_temp.".$col."";
            }
        }


        //insert
        DB::statement("INSERT INTO imenik (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM imenik t WHERE t.id_is=imenik_temp.id_is))");
        //update
        DB::statement("UPDATE imenik i SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE i.id_is=imenik_temp.id_is::varchar");

		DB::statement("SELECT setval('imenik_magacin_imenik_magacin_id_seq', (SELECT MAX(imenik_id) FROM imenik) + 1, FALSE)");
	}

	public static function query_delete_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='imenik'"));
		$table_temp = "(VALUES ".$table_temp_body.") imenik_temp(".implode(',',$columns).")";

		//DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("DELETE FROM imenik WHERE id_is NOT IN (SELECT id_is FROM ".$table_temp.") AND kvota=32");
		//DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}