<?php
namespace IsBalance;
use DB;
use All;

class Price {

	public static function table_body($prices,$mappedArticles){
		$result_arr = array();
		$partner_cena_result_arr = array();
		$codes = array();

		foreach ($prices as $price) {
		    if ($price->headerId == 'osnovni') {
		        foreach ($price->prices as $key => $value) {
		            if ($value > 0 && is_numeric($value)) {
		                $id_is = $price->itemId;
		                $roba_id = isset($mappedArticles[strval($id_is)]) ? $mappedArticles[strval($id_is)] : null;
		                $cena = $value;
		                $rabat = 0;
		                $discountgroup = str_replace("price", "", $key);
		                if(isset($roba_id)){
		                $result_arr[] = "(" . strval($roba_id) . ",'" . $discountgroup . "'," . $cena . "," . $rabat . ")";
		            	}
		            }
		        }
		    }
		}

		// Povratak rezultata
		return (object) array("body" => implode(",", $result_arr));
						
	}
	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='cenovnik_partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") cenovnik_partner_temp(".implode(',',$columns).")";
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="discountgroup"){
		    	$updated_columns[] = "".$col." = cenovnik_partner_temp.".$col."";
			}
		}


		//insert
		DB::statement("INSERT INTO cenovnik_partner (SELECT * FROM ".$table_temp." WHERE (cenovnik_partner_temp.roba_id) NOT IN (SELECT roba_id FROM cenovnik_partner))");
		DB::statement("UPDATE cenovnik_partner rpk SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE cenovnik_partner_temp.discountgroup = rpk.discountgroup AND cenovnik_partner_temp.roba_id=rpk.roba_id");

		$roba_id=DB::select("SELECT DISTINCT roba_id FROM ".$table_temp." ");
		// $roba_id=array_column($roba_id, 'roba_id')[0];

		
		$roba_ids=array_column($roba_id, 'roba_id');
		return $roba_ids;
		
	}
	
	public static function query_delete_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='cenovnik_partner_artikal'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("DELETE FROM cenovnik_partner_artikal WHERE roba_id NOT IN (SELECT roba_id FROM ".$table_temp.")");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}
}