<?php
namespace IsBalance;
use DB;
use Options;
use All;

class Article {

	public static function table_body($articles){
		//$kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');

		$result_arr = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;
		
		foreach($articles as $article) {
				$roba_id++;
				$sifra_k++;
			
				$id_is = $article->id;
				$sifra_is =  $article->code;				
				$naziv = $article->name;

				$jedinica_mere_id =Support::getJedinicaMereId($article->uom);
				$web_opis = pg_escape_string($article->description);
				$proizvodjac_id = Support::getProizvodjacId($article->manufacturer->name);
				
				$grupa_pr_id = 100;				
				$tarifna_grupa_id = 0;

				$barkod = "NULL";
				$model = $article->model;			

				$racunska_cena_nc =  0;
				$mpcena = 0;
				$web_cena = $mpcena;
				$racunska_cena_a = $racunska_cena_nc;
				$racunska_cena_end = $racunska_cena_nc;
				
				$stara_cena =0;

				$marza = 0;
				$flag_aktivan = 1;
				$flag_prikazi_u_cenovniku = '1';
				$flag_cenovnik = '1';

				$result_arr[] = "(".strval($roba_id).",NULL,'".Support::convert($naziv)."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr(Support::convert($naziv),0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".$flag_cenovnik.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_a).",".strval($racunska_cena_end).",".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,".strval($marza).",(NULL)::integer,'".Support::convert($naziv)."',".$flag_cenovnik.",NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,".$barkod.",".strval($stara_cena).",0,1,1,-1,'".$model."',NULL,NULL,NULL,0,0.00,0.00,0.00,0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,0,0,(NULL)::date,(NULL)::date,0.00,0.00,1,NULL,NULL)";
			
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		//DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="sifra_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_is=roba_temp.sifra_is::varchar AND t.sifra_is IS NOT NULL AND t.sifra_is <> '' AND t.flag_zakljucan='false'");	
		//insert
		DB::statement("INSERT INTO roba( SELECT * FROM ".$table_temp." WHERE (roba_temp.sifra_is)::varchar NOT IN (SELECT sifra_is FROM roba WHERE sifra_is IS NOT NULL AND sifra_is <> '') )");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		//DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		DB::statement("UPDATE roba t SET flag_aktivan = 0,flag_prikazi_u_cenovniku = 0,flag_cenovnik = 0 WHERE t.id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
	}

}