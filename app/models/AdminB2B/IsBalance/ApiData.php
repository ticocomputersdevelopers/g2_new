<?php
namespace IsBalance;
use All;


class ApiData {

	public static function getToken(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://ws.melany.rs/api/rest/auth',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>'{
			  "username": "g2_svc",
			  "password": "9jy8t8aZm06vCXLU3C3",
			  "duration": 0,
			  "company": 142
			}',
		  CURLOPT_HTTPHEADER => array(
		    'accept: application/json',
		    'Content-Type: application/json'
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    }else {
	      $result = json_decode($response);	
	      if(isset($result)){
		      return $result->code;
		  }else{
		  	  return array();
		  }
	    }

	}
	public static function articles($token,$page=1){
		$limit = 1000;
		$offset = ($page-1)*$limit;
		//https://ws.melany.rs/api/rest/bort/items/available?isActive=false&itemCode=CL40.2011.0005&limit=1
		$curl = curl_init();https://ws.melany.rs/api/rest/bort/items/available?isActive=false&itemCode=CL40.2011.0005&limit=1

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://ws.melany.rs/api/rest/bort/items/available?isActive=true&offset='.$offset.'&limit='.strval($limit).'',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'accept: application/json',
		    'Authorization: Bearer '.$token.''
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
	//	All::dd(json_decode($response));
	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    }else {
	      $result = json_decode($response);	
	      if(isset($result)){
		      return $result;
		  }else{
		  	  return array();
		  }
	    }
	}
	public static function data($token,$kind){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://ws.melany.rs/api/rest/bort/'.$kind.'/available',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'accept: application/json',
		    'Authorization: Bearer '.$token.''
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		//All::dd(json_decode($response));
	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    }else {
	      $result = json_decode($response);	
	      if(isset($result)){
		      return $result;
		  }else{
		  	  return array();
		  }
	    }
	}
	public static function price($token){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://ws.melany.rs/api/rest/bort/price-list/items',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'accept: application/json',
		    'Authorization: Bearer '.$token.''
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		
		curl_close($curl);
		//All::dd(json_decode($response));
	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    }else {
	      $result = json_decode($response);	
	      if(isset($result)){
		      return $result;
		  }else{
		  	  return array();
		  }
	    }


	}

	public static function stock($token){

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'https://ws.melany.rs/api/rest/bort/stocks/level',
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	  CURLOPT_HTTPHEADER => array(
			    'accept: application/json',
			    'Authorization: Bearer '.$token.''
			  ),
			));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		//All::dd(json_decode($response));
	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    }else {
	      $result = json_decode($response);	
	      if(isset($result)){
		      return $result;
		  }else{
		  	  return array();
		  }
	    }


	}
	
	public static function partners($token){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://ws.melany.rs/api/rest/bort/clients/available',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'accept: application/json',
		    'Authorization: Bearer '.$token.''
		  ),
		));

		$response = curl_exec($curl);

		$err = curl_error($curl);
		
		curl_close($curl);
	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    }else {
	      $result = json_decode($response);	
	      if(isset($result)){
		      return $result;
		  }else{
		  	  return array();
		  }
	    }

	}

}