<?php
namespace ISCalculus;
use DB;

class Price {

	public static function table_body($articles){
		$result_arr = array();
		$codes = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;
		// $grupa_pr_id = DB::select("SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id > 0")[0]->grupa_pr_id;
		$grupa_pr_id = 1;

		foreach($articles as $article) {

			$roba_id++;
			$sifra_k++;
			$sifra_is = $article->Sifra;

			$racunska_cena_nc = $article->Cena * DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
			$mpcena = intval($racunska_cena_nc); //(1+intval($article->porez)/100)
			$web_cena = intval($racunska_cena_nc);


			$result_arr[] = "(".strval($roba_id).",NULL,'',NULL,NULL,NULL,0,0,(NULL)::integer,-1,-1,".strval($sifra_k).",NULL,NULL,'',0,-1,0,0,0,0,9,0,0,0,0,1,1,0,NULL,1,".strval($racunska_cena_nc).",0,".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,NULL,0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."',NULL)";


		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_update($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		$updated_columns = array();
		foreach(array('racunska_cena_nc','racunska_cena_end','web_cena') as $col){
	    	$updated_columns[] = "".$col." = roba_temp.".$col."";
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.sifra_is=roba_temp.sifra_is::varchar");
	}

}