<?php
use SimpleXMLElement;

class Calculus {

	public static function customer_cart($komid){
		$url = B2BOptions::info_sys('calculus')->api_url.'/CalculusWebService.asmx/KarticaKomitenta';
		$post_array = array(
			'nadan'=>'',
			'zr'=>'',
			'valuta'=>'',
			'iznosi'=>'',
			'profav'=>'',
			'kurs'=>'',
			'valzad'=>'',
			'valzadfiksno'=>'',
			'vanvalute'=>'',
			'orgjedkom'=>'',
			'komid'=>$komid,
			'iskljucistorno'=>'',
			'orgjedid'=>'',
			'kupdob'=>'',
			'zaduzenjaod'=>'',
			'zaduzenjado'=>''
			);
		return self::postCurl($url,$post_array);
	}
	public static function order($partner_sifra,$nacin_isporuke='',$napomena='',$sifra_magacin='VP',$document='PR'){
		$url = B2BOptions::info_sys('calculus')->api_url.'/CalculusWebService.asmx/UbaciZagDok';
		$post_array = array(
			'vrstadokumenta' =>	$document,
			'datum' => date('Y-m-d'),
			'komitent' => $partner_sifra,
			'magacin' => $sifra_magacin,
			'kreator' => $sifra_magacin,
			'valuta' => 'DIN',
			'napomena' => $napomena,
			'nacinisporuke' => $nacin_isporuke,
			'poslat' => '',
			'agent' => '',
			'datumprometa' => '',
			'valutaplacanja' => '',
			'ekstdok1' => '',
			'ekstdok2' => '',
			'ekstdok3' => '',
			'status' => '',
			'prokmag' => '',
			'prokknj' => '',
			'zavrsen' => '',
			'statusdok' => '',
			);
		return self::postCurl($url,$post_array,false);
	}

	public static function addOrderStavka($document_id,$sifra_stavka,$kolicina,$rabat='',$sifra_magacin='VP'){
		$url = B2BOptions::info_sys('calculus')->api_url.'/CalculusWebService.asmx/UbaciStavDok';
		$post_array = array(
			'kljucdok' => $document_id,
			'magacin' => $sifra_magacin,
			'artusl' => 'A',
			'sifartusl' => $sifra_stavka,
			'kolicina' => $kolicina,
			'rabatk' => $rabat,
			'nazartusl' => '',
			'tarifa' => '',
			'poruceno' => '',
			'nabcena' => '',
			'vpcena' => '',
			'mpcena' => '',
			'popust' => '',
			'poulazu' => '',
			'napomena' => '',
			'popustk' => '',
			'lomk' => '',
			'rabatd' => '',
			'popustd' => '',
			'lomd' => '',
			);
		return self::postCurl($url,$post_array,false);
	}

	public static function customer_cart_body($items,$partner_id){
		$result_arr = array();
		$web_b2b_kartica_id = DB::select("SELECT nextval('web_b2b_kartica_web_b2b_kartica_id_seq')")[0]->nextval;

		foreach($items as $item) {
			if(isset($item->Datum)){

				$web_b2b_kartica_id++;
				$datum_dokumenta = isset($item->Datum) ? $item->Datum : '1970-00-00T00:00:00+02:00';
				$vrsta_dokumenta = isset($item->BrojDok) ? $item->BrojDok : '1970-00-00T00:00:00+02:00';
				$duguje = isset($item->KupacIznos) ? $item->KupacIznos : 0.00;
				$potrazuje = isset($item->DobavljacIznos) ? $item->DobavljacIznos : 0.00;
				$saldo = (floatval($duguje)-floatval($potrazuje)) > 0 ? (floatval($duguje)-floatval($potrazuje)) : 0.00;

				// $id_is = substr(($partner_id.'-'.substr($datum_dokumenta,0,10).B2bUrl::slugify($vrsta_dokumenta).$duguje.$potrazuje.(isset($item->BR) ? $item->BR : '').(isset($item->CB) ? $item->CB : '')),0,254);
				$id_is = $partner_id.'-'.$web_b2b_kartica_id;

				$result_arr[] = "(".$web_b2b_kartica_id.",".strval($partner_id).",(NULL)::integer,'".$vrsta_dokumenta."',('".$datum_dokumenta."')::date,('".$datum_dokumenta."')::date,'".$vrsta_dokumenta."',".strval($duguje).",".strval($potrazuje).",".strval($saldo).",(NULL)::integer,'".strval($id_is)."')";
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function customer_cart_insert_update($table_temp_body,$partner_id,$upd_cols=array()) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_b2b_kartica'"));
		$table_temp = "(VALUES ".$table_temp_body.") web_b2b_kartica_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="web_b2b_kartica_id"){
		    	$updated_columns[] = "".$col." = web_b2b_kartica_temp.".$col."";
			}
		}
		// DB::statement("UPDATE web_b2b_kartica t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=web_b2b_kartica_temp.id_is");
		DB::statement("DELETE FROM web_b2b_kartica WHERE partner_id=".$partner_id."");
		//insert
		DB::statement("INSERT INTO web_b2b_kartica (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM web_b2b_kartica t WHERE t.id_is=web_b2b_kartica_temp.id_is))");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

		DB::statement("SELECT setval('web_b2b_kartica_web_b2b_kartica_id_seq', (SELECT MAX(web_b2b_kartica_id) FROM web_b2b_kartica), FALSE)");
	}


	public static function updateCart($partner_id=null){
        if(is_null($partner_id)){
            $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        }
		$partner = DB::table('partner')->where('partner_id',$partner_id)->first();

		$items = self::customer_cart(intval($partner->id_is));
        if(count($items) > 0){
            $resultCart = self::customer_cart_body($items,$partner->partner_id);
            if(isset($resultCart->body) && $resultCart->body != ''){
            	self::customer_cart_insert_update($resultCart->body,$partner->partner_id);
            }
        }

	}

    public static function createOrder($cartItems,$nacin_isporuke='',$napomena=''){
    	$success = false;

        $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        $partner = DB::table('partner')->where('partner_id',$partner_id)->whereNotNull('id_is')->first();
        if(!is_null($partner)){
	        $order_response = self::order($partner->sifra,$nacin_isporuke,$napomena);
	        $document_id = intval($order_response);

	        foreach($cartItems as $stavka){
	            $sifra_stavka = DB::table('roba')->where('roba_id',$stavka->roba_id)->pluck('sifra_is');
	            $stavkaDokumenta = self::addOrderStavka($document_id,$sifra_stavka,$stavka->kolicina,B2bArticle::b2bRabatCene($stavka->roba_id)->ukupan_rabat);
	        }
	        $success = true;
	    }
	    return (object) array('success'=>$success, 'order_id'=>$document_id);
    }

	public static function postCurl($url,$post_array,$format_response=true){
	    $post_string = http_build_query($post_array);

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded','Content-Length: '.strlen($post_string)));
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	    $response = curl_exec($ch);
	    curl_close($ch);
		
		$xml = new SimpleXMLElement($response);

		if($format_response){
			$ns = $xml->getNamespaces(true);
			$xml->registerXPathNamespace('d', $ns['diffgr']);
			$result = $xml->xpath('//d:diffgram/NewDataSet/Table');
		}else{
			$result = strval($xml);
		}
		return $result;
	}

}