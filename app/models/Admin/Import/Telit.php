<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Telit {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/telit/telit_xml/telit.xml');
			$products_file = "files/telit/telit_xml/telit.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}


		if($continue){
			$str=file_get_contents($products_file);
			//$str=preg_replace('/(<\?xml[^?]+?)utf-8/i', '$1Windows-1250', $str);
			file_put_contents($products_file, $str);
			$products = simplexml_load_file($products_file);


			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			foreach ($products as $product):
			if(!empty($product->sifra_artikla)){

				$flag_opis_postoji = "0";

				$images = $product->xpath('images/image');
				$flag_slika_postoji = "0";
				$i = 0;
				foreach ($images as $slika):
					if($i==0){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$product->sifra_artikla."','".$slika."',1 )");
					}else{
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$product->sifra_artikla."','".$slika."',0 )");
					}
					$i++;
					$flag_slika_postoji = "1";
				endforeach;	

				$sPolja = '';
				$sVrednosti = '';

				//$opis = $product->opis;
				if(isset($product->pdv)) {
					$pdv = $product->pdv;
				} else {
					$pdv = 0;
				}
				$opis = addslashes(Support::encodeTo1250($product->opis));
				$opis = pg_escape_string($opis);

				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $product->sifra_artikla . "',";
				$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->proizvodjac)) . "',";
				$sPolja .= "naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->naziv)) . "',";
				$sPolja .= "opis,";						$sVrednosti .= "'" . $opis . "',";
				$sPolja .= "grupa,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->grupa)) . "',";
				$sPolja .= "pdv,";						$sVrednosti .= " 20,";
				$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->kolicina . ",";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($product->mpcena,1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";
				$sPolja .= "web_flag_karakteristike";	$sVrednosti .= " 0";
				// $sPolja .= "flag_opis_postoji,";		$sVrednosti .= "" . $flag_opis_postoji . ",";
				// $sPolja .= "flag_slika_postoji";		$sVrednosti .= "" . $flag_slika_postoji . "";
						
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");


			}

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/telit/telit_xml/telit.xml');
			$products_file = "files/telit/telit_xml/telit.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}
		$products = simplexml_load_file($products_file);

		if($continue){
			$str=file_get_contents($products_file);
			//$str=preg_replace('/(<\?xml[^?]+?)utf-8/i', '$1Windows-1250', $str);
			file_put_contents($products_file, $str);
			$products = simplexml_load_file($products_file);
			
			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			foreach ($products as $product):
			if(!empty($product->sifra_artikla)){
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $product->sifra_artikla . "',";
				$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->kolicina . ",";
				$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($product->mpcena,1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}