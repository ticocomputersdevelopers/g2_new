<?php

class AdminNacinPlacanja {

	public static function all() {
		return DB::table('web_nacin_placanja')->where('web_nacin_placanja_id', '!=', -1)->get();
	}

	public static function getSingle($id){
		return DB::table('web_nacin_placanja')->where('web_nacin_placanja_id', $id)->first();
	}

	public static function getFiscalName($web_nacin_placanja_id) {
		$racun_vrsta_placanja_naziv_id =  DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$web_nacin_placanja_id)->pluck('racun_vrsta_placanja_naziv_id');
		return DB::table('racun_vrsta_placanja_naziv')->where('racun_vrsta_placanja_naziv_id',$racun_vrsta_placanja_naziv_id)->pluck('racun_vrednost');

	}

	public static function getFisalValue($web_nacin_placanja_id) {
		return DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$web_nacin_placanja_id)->pluck('racun_vrsta_placanja_naziv_id');
	}

}