<?php 

class Url_mod {

	//pretvara reci u link
	 public static function slugify($str,$lang=null){
	    $table = array(
	            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
	            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
	            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
	            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
	            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
	            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
	            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
	            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-',")"=> ""," )"=> ""
	    );
	    $string = strtolower(strtr($str, $table));
	    $string = str_replace(array('А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','Љ','М','Н','Њ','О','П','Р','С','Т','Ћ','У','Ф','Х','Ц','Ч','Џ','Ш','а','б','в','г','д','ђ','е','ж','з','и','ј','к','л','љ','м','н','њ','о','п','р','с','т','ћ','у','ф','х','ц','ч','џ','ш'),array('a','b','v','g','d','dj','e','z','z','i','j','k','l','lj','m','n','nj','o','p','r','s','t','c','u','f','h','c','c','dz','s','a','b','v','g','d','dj','e','z','z','i','j','k','l','lj','m','n','nj','o','p','r','s','t','c','u','f','h','c','c','dz','s'), $string);
	    $urlKey = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
		return $urlKey;
	}

	//prevodi rec u prevedeni slug
	public static function slug_trans($str,$lang=null){
		return self::slugify(Language::trans($str,$lang));
	}

	public static function page_slug($naziv_stranice,$lang=null){
        if($lang==null){
            $lang = Language::lang();
        }
		$web_b2c_seo = DB::table('web_b2c_seo')->where('naziv_stranice',$naziv_stranice)->first();
		$web_b2c_seo_jezik = DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>$web_b2c_seo->web_b2c_seo_id,'jezik_id'=>DB::table('jezik')->where(array('kod'=>$lang, 'aktivan'=>1))->pluck('jezik_id')))->first();
		if(!is_null($web_b2c_seo_jezik) && !is_null($web_b2c_seo_jezik->slug) && !is_null($web_b2c_seo_jezik->naziv) && $web_b2c_seo_jezik->slug != '' && $web_b2c_seo_jezik->naziv != ''){
			return (object) array('slug'=>$web_b2c_seo_jezik->slug,'naziv'=>$web_b2c_seo_jezik->naziv);
		}
		return (object) array('slug'=>$naziv_stranice,'naziv'=>$web_b2c_seo->title);
	}

	public static function slug_convert_page($string,$lang=null){
        if($lang==null){
            $lang = Language::lang();
        }
        $web_b2c_seo_jezik = DB::table('web_b2c_seo_jezik')->where(array('jezik_id'=>DB::table('jezik')->where(array('kod'=>$lang, 'aktivan'=>1))->pluck('jezik_id'), 'slug'=>$string))->first();
        if(is_null($web_b2c_seo_jezik) ){
        	return $string;
        }
    	return DB::table('web_b2c_seo')->where('web_b2c_seo_id',$web_b2c_seo_jezik->web_b2c_seo_id)->pluck('naziv_stranice');
	}

	 public static function filters_convert($str){
	 	$str = Language::trans($str);
		 $string_new=strtolower($str);
		$urlKey = strtr($string_new, array("š"=>"s","č"=>"c","ć"=>"c","đ"=>"dj","ž"=>"z","Š"=>"s","Č"=>"c","Ć"=>"c","Đ"=>"dj","Ž"=>"z"," "=>"_","-"=>"",','=>"","."=>"","*"=>"","?"=> "","!"=> "","/"=> "","("=> "",")"=> "",'"'=> "",'%'=> "",'#'=> "",'$'=> "",'+'=> "",'-'=> "",'`'=> "",'@'=> "",'^'=> "",'&'=> "",'{'=> "",'}'=> "",'['=> "",']'=> "",';' => "",'|'=> "" ,':'=> "" ,'<'=> "",'>'=> "",'_'=> "" ));
	
		return $urlKey;	
	}

	public static function grupa_pr_id_bc($grupa){
		$grupa_pr_id=0;
		$grupa_prs = DB::table('grupa_pr')->where(array('prikaz'=>1,'web_b2c_prikazi'=>1))->get();
		foreach($grupa_prs as $row){
			if($grupa == trim(self::slug_trans($row->grupa))){
				$grupa_pr_id=$row->grupa_pr_id;
				break;
			}
		}
		return $grupa_pr_id;

	}
	public static function get_grupa_pr_id($grupe){
		$sameGroupIds = array();
		for ($i = 0; $i<count($grupe); $i++) {
			$sameGroupIds[] = self::sameGroupIds($grupe[$i]);
		}

		$ids = array(0);
		foreach($sameGroupIds as $groupIds){
			$ids = array_map('current',DB::table('grupa_pr')->select('grupa_pr_id')->whereIn('grupa_pr_id',$groupIds)->whereIn('parrent_grupa_pr_id',$ids)->get());
		}
		return isset($ids[0]) ? $ids[0] : 0;

	}
	public static function sameGroupIds($grupa){
		$grupa_pr_ids = array();
		$grupa_prs = DB::table('grupa_pr')->where(array('prikaz'=>1,'web_b2c_prikazi'=>1))->get();
		foreach($grupa_prs as $row){
			if($grupa == trim(self::slug_trans($row->grupa)) && $row->grupa_pr_id > 0){
				$grupa_pr_ids[] = $row->grupa_pr_id;
			}
		}
		return $grupa_pr_ids;
	}

	public static function blog_link($web_vest_b2c_id){
        $jezik_id = DB::table('jezik')->where(array('aktivan'=>1, 'kod'=>Language::lang()))->pluck('jezik_id');
        $naslov = DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$web_vest_b2c_id, 'jezik_id'=>$jezik_id))->pluck('naslov');
		return self::slugify($naslov);
	}
	
	// public static function search_conver($str){
	// 	 $string_new=strtolower($str); 
	// 	$urlKey = strtr($string_new, array("&frasl;"=>"/"," "=>"%"));
	
	// 	return $urlKey;
	// }

	// public static function text_convert($string){
	// 	$urlKey = strtr($string, array("?"=>"&scaron;","?"=>"&#269;","?"=>"&#263;","?"=>"&#273;","?"=>"&#382;","?"=>"&Scaron;","?"=>"&#268;","?"=>"&#262;","?"=>"&#272;","?"=>"&#381;",'"'=>"","±"=>" "));
 //    	return $urlKey;
	// }
	
	// public static function keywords_convert($string){
	// 	$urlKey = strtr($string, array("?"=>"&scaron;","?"=>"&#269;","?"=>"&#263;","?"=>"&#273;","?"=>"&#382;","?"=>"&Scaron;","?"=>"&#268;","?"=>"&#262;","?"=>"&#272;","?"=>"&#381;" ," "=>","));
	// return $urlKey;
	// }
	// public static function sitempat_convert($string){
	// 	$urlKey = strtr($string, array("&"=>" "));
	// return $urlKey;
	// }
	
	// public static function breadcrumbs($separator = '', $home = '') {
 //    	$path = array_filter(explode('/', parse_url(strtr(urldecode($_SERVER['REQUEST_URI']), array()), PHP_URL_PATH)));
 //    	$base_url = substr($_SERVER['SERVER_PROTOCOL'], 0, strpos($_SERVER['SERVER_PROTOCOL'], '/')) . '://' . $_SERVER['HTTP_HOST'] . '/';
 //    	$breadcrumbs = array("<li><a href=".Options::base_url().">".All::get_title_page_start()."</a></li>");
 //    	$tmp = array_keys($path);
 //    	$last = end($tmp);
 //    	unset($tmp);
 //    	foreach ($path as $x => $crumb){
 //        	$title = ucwords(str_replace(array('.php', '_'), array('', ' '), $crumb));
	// 		if ($x == 0){
	//         	$breadcrumbs[]  = "<li><a href=".Options::base_url().">".All::get_title_page_start()."</a></li>";
	// 		}elseif($x > 1 && $x < $last){
	// 			$tmp = "<li><a class=b-kategorija href=\"$base_url";
	// 			for($i = 1; $i <= $x; $i++){
	// 				$tmp .= $path[$i] . '/';
	// 			}
 //                $tmp .= "\">$title</a></li>";
	// 			$breadcrumbs[] = $tmp;
	// 			unset($tmp);
 //        	}else{
 //                $breadcrumbs[] = "<li>".$title."</li>";
	// 		}
 //    	}
 //    	unset($breadcrumbs[0],$breadcrumbs[1]);
 //    	return implode($separator, $breadcrumbs);
	// }

	public static function breadcrumbs2(){
		$url=$_SERVER['REQUEST_URI'];
		$offset = Language::segment_offset()==0?1:0;
		$url_arr = explode('/',$url);

		$string = "<li><a href='".Options::domain()."'>".All::get_title_page_start()."</a></li>";
		$end = count($url_arr)-1;
		for($i=(2-$offset);$i<=$end;$i++){

			$gr_arr = array();
			foreach($url_arr as $key => $grp){
				if($key <= $i && $key >= (2-$offset)){
					$gr_arr[] = $grp;
				}
			}

			if($i < $end){
				$string .= "<li><a href='".Options::base_url().implode('/',$gr_arr)."'>".Groups::getGrupa(Url_mod::grupa_pr_id_bc($url_arr[$i]))."</a></li>";
			}else{
				$url_arr[$i] = explode('?',$url_arr[$i])[0];
				$string .= "<li>".Groups::getGrupa(Url_mod::grupa_pr_id_bc($url_arr[$i]))."</li>";
			}
		}
		echo $string;
	}
	
	// public static function b2bBreadcrumbs(){
	// 	$url=$_SERVER['REQUEST_URI'];
	// 	$url_arr = explode('/',$url);
		
	// 	$string = "<li><a href='".Options::base_url()."b2b'>".All::get_title_page_start()."</a></li>";
	// 	$end = count($url_arr)-1;
	// 	for($i=3;$i<=$end;$i++){

	// 		$gr_arr = array();
	// 		foreach($url_arr as $key => $grp){
	// 			if($key <= $i && $key >= 3){
	// 				$gr_arr[] = $grp;
	// 			}
	// 		}

	// 		if($i < $end){
	// 			$string .= "<li><a href='".Options::base_url()."b2b/artikli/".implode('/',$gr_arr)."'>".Groups::getGrupa(Url_mod::grupa_pr_id_bc($url_arr[$i]))."</a></li>";
	// 		}else{
	// 			$url_arr[$i] = explode('?',$url_arr[$i])[0];
	// 			$string .= "<li>".Groups::getGrupa(Url_mod::grupa_pr_id_bc($url_arr[$i]))."</li>";
	// 		}
	// 	}
	// 	echo $string;
	// }

	public static function checkOldLink($link){
		$stari_link = DB::table('stari_linkovi')->where('link',trim($link))->first();
		if(!is_null($stari_link)){
			if($stari_link->kategorija == 0 && ( !is_null($stari_link->id) || !is_null($stari_link->sifra)) ) {
				$result = DB::select("select roba_id from roba where ".(!is_null($stari_link->id) ? "roba_id = ".$stari_link->id." or " : "")."sifra_d ilike '".$stari_link->sifra."'");
				if(isset($result[0])){
					return Options::base_url().self::slug_trans('artikal').'/'.self::slug_trans(Product::seo_title($result[0]->roba_id));
				}
			}else if($stari_link->kategorija == 1 && !is_null($stari_link->id)){
				$result = DB::select("select grupa_pr_id from grupa_pr where grupa_pr_id = ".$stari_link->id."");
				if(isset($result[0])){
					return Groups::category_link($result[0]->grupa_pr_id);
				}				
			}else if($stari_link->kategorija == 2 && !is_null($stari_link->id)){
				$result = DB::select("select web_b2c_seo_id from web_b2c_seo where web_b2c_seo_id = ".$stari_link->id."");
				if(isset($result[0])){
					return Url_mod::page_slug($result[0]->naziv_stranice)->slug;
				}				
			}else if(is_null($stari_link->id) && is_null($stari_link->sifra) && is_null($stari_link->kategorija)) {
				return Options::base_url().$stari_link->novi_link;
			}
		}
		return null;
	}

	// public static function change_lang_link($lang){
	// 	$requestArr = explode('/',Request::path());
	// 	$requestArr[0] = $lang;
	// 	return Options::domain().implode('/',$requestArr);
	// }
	
}