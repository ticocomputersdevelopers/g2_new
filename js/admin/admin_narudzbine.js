$(document).ready(function() {

		$("#JSKupacDetail").click(function() {
			window.open(base_url + 'admin/kupci_partneri/kupci/' + $("#JSWebKupac").val(), '_blank');
		});	

		$("#JSWebKupac").change(function() {
			$.post(base_url + 'admin/ajax/narudzbina-kupac', {web_kupac_id:$(this).val()}, function (response){
				$('#JSKupacAjaxContent').html(response);
			 });
		});
		$("#OrderSelect").change(function() {
			var id = $(this).val();
			var current = $(this).data('order');
			if(id != 0){
			window.location.href=base_url + 'admin/narudzbina/' + id;
			}else{
			window.location.href= base_url + 'admin/narudzbina/' + current;	
			}
		});
		//filtriranje po dodatnom statusu
	  	$("#JSOrderStatusSearch").click(function(){
	  		var narudzbina_status_id = $('#narudzbina_status_id').val();

	  		var search = $('#search-btn').val();
	  		var status = $(this).data('status');
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
			var pickup_id = $('#pickup_id').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!narudzbina_status_id){
				narudzbina_status_id = ['0'];
		 		}
		 		if(!search){
					search = '0';
		 		}
		 		if(!pickup_id){
					pickup_id = '0';
		 		}
				var status_str= location.pathname.split('/')[3];
				var statuses = status_str.split('-');
				var index = statuses.indexOf(status);
				if(index !== -1 ){
					statuses.splice(index,1);
				}else{
					statuses.push(status);			
				}

				if(statuses.length == 0)
				{
					statuses.push('sve');
				}

			window.location.href = base_url + 'admin/porudzbine/' + status_str +'/'+ narudzbina_status_id.join('-') + '/' + datum_od + '/' + datum_do + '/' + search + '/' + pickup_id;
	  	});
		//filtriranje po cityexpress statusu
	  	$("#pickup_id").change(function(){
	  		var pickup_id = $(this).val();	  		
	  		var narudzbina_status_id = $('#narudzbina_status_id').val();
	  		var search = $('#search-btn').val();
	  		var status = $(this).data('status');
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!pickup_id){
				pickup_id = '0';
		 		}
		 		if(!search){
					search = '0';
		 		}
		 		if(!pickup_id){
					pickup_id = '0';
		 		}
				var status_str= location.pathname.split('/')[3];
				var statuses = status_str.split('-');
				var index = statuses.indexOf(status);
				if(index !== -1 ){
					statuses.splice(index,1);
				}else{
					statuses.push(status);			
				}

				if(statuses.length == 0)
				{
					statuses.push('sve');
				}
			window.location.href = base_url + 'admin/porudzbine/' + status_str +'/'+ narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search + '/' + pickup_id;
	  	});
		$(".search_select").select2({
		width: '90%',
        language: {
            noResults: function () {
            return translate("Nema rezultata");
            }
        }
    	});
		//filtriranje po statusu
		$('.JSStatusNarudzbine').click(function() {
			var search = $('#search-btn').val();
			var status = $(this).data('status');
			var narudzbina_status_id = $('#narudzbina_status_id').val();
			var pickup_id = $('#pickup_id').val();
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!narudzbina_status_id){
				narudzbina_status_id = '0';
		 		}
		 		if(!search){
				search = '0';
		 		}
		 		if(!pickup_id){
					pickup_id = '0';
		 		}
				var status_str= location.pathname.split('/')[3];
				var statuses = status_str.split('-');
				var index = statuses.indexOf(status);
				if(index !== -1 ){
					statuses.splice(index,1);
				}else{
					statuses.push(status);			
				}

				if(statuses.length == 0)
				{
					statuses.push('sve');
				}
			window.location.href = base_url + 'admin/porudzbine/' + statuses.join('-') + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search + '/' + pickup_id;
		});
		
		$('#JSFilterClear').click(function(){

			window.location.href = base_url+'admin/porudzbine/sve/0/0/0/0/0';

		});

		$('#search-btn').click(function(){

		var search = $('#search').val();
		var status = $(this).data('status');
		var narudzbina_status_id = $('#narudzbina_status_id').val();
		var pickup_id = $('#pickup_id').val();
		var datum_od = $('#datepicker-from').val();
		var datum_do = $('#datepicker-to').val();
		var status_str= location.pathname.split('/')[3];
			if(!datum_od){
			datum_od = '0';
			}
			if(!datum_do){
			datum_do = '0';
		 	}
		 	if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		 	}
		 	if(!search){
			search = '0';
		 	}
	 		if(!pickup_id){
				pickup_id = '0';
	 		}				

		window.location.href = base_url + 'admin/porudzbine/' + status_str +'/'+ narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search + '/' + pickup_id;
		
		});

		$('#search').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#search').val() != ''){
        	$("#search-btn").trigger("click");
    	}
    	});

		//kalendar za filtriranje po datumu
		$('#datepicker-from').change(function() {
		var search = $('#search-btn').val();	
		var status_str= location.pathname.split('/')[3];
		var narudzbina_status_id = $('#narudzbina_status_id').val();
		var datum_od = $(this).val();
		var datum_do = $(this).data('datumdo');
		var pickup_id = $('#pickup_id').val();

		if(!datum_od){
				datum_od = '0';
		}
		if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		}
		if(!search){
			search = '0';
		}
 		if(!pickup_id){
			pickup_id = '0';
 		}
		window.location.href = base_url + 'admin/porudzbine/' + status_str + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search + '/' + pickup_id;
		});

		$('#datepicker-to').change(function() {
		 var search = $('#search-btn').val();
		 var datum_do = $(this).val();
		 var status_str= location.pathname.split('/')[3];
		 var narudzbina_status_id = $('#narudzbina_status_id').val();
		 var pickup_id = $('#pickup_id').val();
		 var datum_od = $(this).data('datumod');
		 if(!datum_do){
				datum_do = '0';
		 }
		 if(!narudzbina_status_id){
			narudzbina_status_id = '0';
		 }
		 if(!search){
			search = '0';
		 }	
 		if(!pickup_id){
			pickup_id = '0';
 		}
		 window.location.href = base_url + 'admin/porudzbine/' + status_str + '/' + narudzbina_status_id + '/' + datum_od + '/' + datum_do + '/' + search + '/' + pickup_id;
		});

		// promena statusa narudzbine
		$(".order-status__select").change(function() {

		$selected = $(this).children(":selected");

		var status_target = $selected.data('status-target');
		var web_b2c_narudzbina_id = $selected.data('narudzbina-id');

		$.ajax({
				type: "POST",
				url: base_url + 'admin/ajax/promena_statusa_narudzbine',
				data: { status_target: status_target, web_b2c_narudzbina_id: web_b2c_narudzbina_id },
				success: function(msg) {
						// alert(msg);
						location.reload();
				}
		}); // alert(web_b2c_narudzbina_id+' '+status_target);
		});

		// $('.JSMoreWiew').click(function(e) {
		// 		var web_b2c_narudzbina_id = $(this).data('id-narudzbina');

		// 		if (e.target.tagName.toLowerCase() != 'select' && e.target.tagName.toLowerCase() != 'i') { 
		// 			window.location.href = base_url + 'admin/narudzbina/' + web_b2c_narudzbina_id;  
		// 		}
				// $.ajax({
				//     type: "POST",
				//     url: base_url + 'admin/ajax/porudzbina_vise',
				//     data: { web_b2c_narudzbina_id: web_b2c_narudzbina_id },
				//     success: function(msg) {
				//         $('#ordersDitailsModal').find('.content').html(msg);
				//         $('#ordersDitailsModal').foundation('reveal', 'open');
				//     }
				// });
		// });





		$('.JSDeleting').on('click', function(){
			var web_b2c_narudzbina_id = $(this).parent('tr').data('id-narudzbina');
		  	alertify.confirm(translate("Da li ste sigurni da želite da obrišete narudžbinu?"),
		    function(e){
		    	if(e){
		        window.location.href = base_url + 'admin/porudzbina/delete/' + web_b2c_narudzbina_id;
		    	}
		    });

		});



	var articlesTime = 0;
	$('#JSsearch_porudzbine').on("keyup", function() {
				clearTimeout(articlesTime);
				articlesTime = setTimeout(function(){
					$('.articles_list').remove();
					var articles = $('#JSsearch_porudzbine').val();
					var narudzbina_id = $('#narudzbina_id').val();
					if (articles != '' && articles.length > 2) {
						$.post(base_url + 'admin/ajax/narudzbine_search', {articles:articles,narudzbina_id:narudzbina_id}, function (response){
							$('#search_content').html(response);
						});
					}
				}, 500);
	});

	$('.JSDeleteStavka').click(function() {

		var stavka_id = $(this).data('stavka-id');
		var narudzbina_id = $('#narudzbina_id').val();

		$.post(base_url + 'admin/ajax/delete_stavka', {stavka_id:stavka_id}, function (response){
			window.location.href = base_url + 'admin/narudzbina/' + narudzbina_id;
		});
	});

	$('.JSCenaInput').hide();

		$('.JSEditCenaBtn').on('click', function(){
		$(this).parent().children('.JSCenaInput').show();
		$(this).parent().children('.JSCenaInput').focus();
		});

		$('.JSCenaInput').focusout(function(){
			$(this).css('display', 'none');
		});

		$('.JSCenaInput').on("keyup", function(event) {
		var val = $(this).val();
		var stavka_id = $(this).data('stavka-id');
		var narudzbina_id = $('#narudzbina_id').val();
		

		if(event.keyCode == 13){
			$(this).closest('td').find('.JSCenaVrednost').html(val);
	    	$(this).focusout();

    	$.post(base_url+'admin/ajax/editCena', {stavka_id:stavka_id, val: val, narudzbina_id: narudzbina_id}, function (response){
    		var data = JSON.parse(response);
			$('#cena-artikla').text(data.cena_artikla);
			$('#troskovi-isporuke').text(data.troskovi_isporuke);
			$('#ukupna-cena').text(data.ukupna_cena);

    	});
  		}
	});

		$('.JStrosakInput').hide();

		$('.JSEdittrosakBtn').on('click', function(){
			$(this).parent().children('.JStrosakInput').show();
			$(this).parent().children('.JStrosakInput').focus();
		});

		$('.JStrosakInput').focusout(function(){
			$(this).css('display', 'none');
		});

		$('.JStrosakInput').on("keyup", function(event) {
			var val = $(this).val();
			var stavka_id = $(this).data('stavka-id');
			var narudzbina_id = $('#narudzbina_id').val();


			if(event.keyCode == 13){
				$(this).closest('trosak').find('.JStrosakVrednost').html(val);			
		    	$(this).focusout();

	    	$.post(base_url+'admin/ajax/editTrosak', {stavka_id:stavka_id, val: val, narudzbina_id: narudzbina_id}, function (response){
	    		var data = JSON.parse(response);
	    		$('#cena-artikla').text(data.cena_artikla);
				$('#troskovi-isporuke').text(data.troskovi_isporuke);
				$('#ukupna-cena').text(data.ukupna_cena);

	    	});
	  		}
		});

	$('.kolicina').keydown(false);
	$('.kolicina').change(function() {
		var stavka_id = $(this).data('stavka-id');
		var kolicina = $(this).val();  
		var narudzbina_id = $('#narudzbina_id').val();
		var lager = $(this).closest('tr').find('.JSLager');

		if(kolicina == 0 || kolicina =='') {
			$(this).css('border-color', 'red');
		} else {
			$(this).css('border-color', '');
		}

		$.post(base_url + 'admin/ajax/update_kolicina', {stavka_id:stavka_id, kolicina:kolicina, narudzbina_id:narudzbina_id}, function (response){
			var data = JSON.parse(response);
			$('#cena-artikla').text(data.cena_artikla);
			$('#troskovi-isporuke').text(data.troskovi_isporuke);
			$('#ukupna-cena').text(data.ukupna_cena);
			lager.text(data.lager);
		 });
	});

	$('.datum-val').keydown(false);

	$('.JS-realizuj').click(function(){
		var narudzbina_id = $('#narudzbina_id').val();
		if($('#poslato').is(':checked')){
			$.post(base_url + 'admin/ajax/realizuj', {narudzbina_id:narudzbina_id, confirm:0}, function (response){
				if(response.check_stock == true){
					location.reload();
				}else{
					if(confirm(translate("Roba nema na stanju. Da li želite da realizujete narudžbinu?"))){
						$.post(base_url + 'admin/ajax/realizuj', {narudzbina_id:narudzbina_id, confirm:1}, function (response){
							location.reload();
						});
					}						
				}
			});
		} else {
			
			if(confirm('Niste poslali, da li zelite da realizujete?')){
				$.post(base_url + 'admin/ajax/realizuj', {narudzbina_id:narudzbina_id, confirm:0}, function (response){
					if(response.check_stock == true){
						location.reload();
					}else{
						if(confirm(translate("Roba nema na stanju. Da li želite da realizujete narudžbinu?"))){
							$.post(base_url + 'admin/ajax/realizuj', {narudzbina_id:narudzbina_id, confirm:1}, function (response){
								location.reload();
							});
						}						
					}
				});
			}
		}
	});

	$('.JS-prihvati').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/ajax/prihvati', {narudzbina_id:narudzbina_id}, function (response){
			location.reload();
			
		});
	});

	$('.JS-storniraj').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/ajax/storniraj', {narudzbina_id:narudzbina_id}, function (response){
			location.reload();
		});
	});

	$('.JS-nestorniraj').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/ajax/nestorniraj', {narudzbina_id:narudzbina_id}, function (response){
			location.reload();
		});
	});
	$('.JS-delete').click(function() {
		var narudzbina_id = $('#narudzbina_id').val();
		$.post(base_url + 'admin/ajax/obrisi', {narudzbina_id:narudzbina_id}, function (response){	
			alertify.alert(translate('Narudžbina je obrisana!'));
			window.location.href = base_url + 'admin/porudzbine/sve/0/0/0/0/0';
		});
	});

	$('.JS-nazad').click(function() {	
		window.location.href = base_url + 'admin/porudzbine/sve/0/0/0/0/0';
	});

	$('#JSPostaSlanje').change(function(){	
		var id = $(this).val();
		var api_id = $(this).data('api_id');
		var parent_div = $(this).closest('div');

		if(id == api_id){
			parent_div.removeClass('medium-6');
			parent_div.addClass('medium-4');
			parent_div.after('<div id="JSPosaljiDiv" class="field-group columns medium-2"><label>Pošalji</label><input name="posta_zahtev" id="JSPosalji" type="checkbox"></div>');
		}else{
			parent_div.removeClass('medium-4');
			parent_div.addClass('medium-6');
			$('#JSPosaljiDiv').remove();
		}
	});
	$('#CityMailSend').on('click', function(){	

 		var narudzbina_id = $(this).data('narudzbina');
 	
		$.post(base_url + 'admin/ajax/mail_send', {narudzbina_id:narudzbina_id}, function (response){	
			
			
		});
		
	}); 





	var active = 0;
	$('.JSorders-listing').on("contextmenu", function(e) {
		$('.custom-menu').css('top', e.pageY + "px");
		$('.custom-menu').css('left', e.pageX + "px" );

		if(active == 0) {
			$('.custom-menu').addClass('custom-menu-active');
			active = 1;
		} else if(active == 1) {
			$('.custom-menu').removeClass('custom-menu-active');
			active = 0;
		}
		
    	return false;
  	});
	$(document).on('click', function(){
		$('.custom-menu').removeClass('custom-menu-active');
		active = 0;
	});
	$('.custom-menu-item').on('click', function(){
		active = 0;
	});


	$('.JSOrderEdit').on('click',function(){
		window.open(base_url + 'admin/narudzbina/'+$(this).closest('tr').data('id-narudzbina'), '_blank');
	});

     var narudzbina_ids = new Array();
     var selected_narudzbina_ids;
	  $(function() {
	    $( "#selectable" ).selectable({
	    filter: 'tr',
	    stop: function() {
		      	narudzbina_ids = [];
		      	$("td").removeClass("ui-selected");
		      	$("td").find('input').removeClass("ui-selected");
		        
		      	var selectedItems = $('.ui-selected', this);
	          	$('#JSselektovano').html(selectedItems.length);

		        $( ".ui-selected", this ).each(function() {
		          var id = $( this ).data("id-narudzbina");
		          narudzbina_ids.push(id);
		        });
		        narudzbina_ids=narudzbina_ids.filter(function(n){return n !== undefined});
		        selected_narudzbina_ids=narudzbina_ids;

		        console.log(selected_narudzbina_ids);
	        }
	    });
	  });

	$('#JSPorudzbinaSelectAll').on('click',function(){
		$("#selectable").find("tr").addClass("ui-selected");

		selected_narudzbina_ids = all_narudzbina_ids;
		$('#JSselektovano').html(selected_narudzbina_ids.length);
	});

	$('#JSPorudzbinaPrihvati').on('click',function(){
		$.ajax({
				type: "POST",
				url: base_url + 'admin/ajax/promena_statusa_narudzbine',
				data: { status_target: 1, web_b2c_narudzbina_ids: selected_narudzbina_ids },
				success: function(response) {
					if(response.exception){
						alertify.alert(translate('Data akcija je izvršena. Neke od narudzžbina su realizovane ili stornirane.'));
					}else{
						alertify.success(translate('Data akcija je izvršena'));
					}

				  	$.each(response.ids, function( index, value ) {					  			
						$("#selectable").find('[data-id-narudzbina="'+ value +'"]').find('.JSOrderStatus').text('Prih.');
						$("#selectable").find('[data-id-narudzbina="'+ value +'"]').removeClass('nova').addClass('prihvacena');
					});
				}
		});		
	});
	$('#JSPorudzbinaRealizuj').on('click',function(){
		realization(selected_narudzbina_ids,0);
	});
	function realization(selected_narudzbina_ids,confirm) {
		$.ajax({
				type: "POST",
				url: base_url + 'admin/ajax/promena_statusa_narudzbine',
				data: { status_target: 2, web_b2c_narudzbina_ids: selected_narudzbina_ids, confirm: confirm },
				success: function(response) {
					if(response.check_stock == true){
						if(response.exception){
							alertify.alert(translate('Data akcija je izvršena. Neke od narudzžbina su nove ili stornirane.'));
						}else{
							alertify.success(translate('Data akcija je izvršena'));
						}

					  	$.each(response.ids, function( index, value ) {					  			
							$("#selectable").find('[data-id-narudzbina="'+ value +'"]').find('.JSOrderStatus').text('Real.');
							$("#selectable").find('[data-id-narudzbina="'+ value +'"]').removeClass('prihvacena').addClass('realizovana');
						});
					}else{
						
					  	alertify.confirm(translate("Roba nema na stanju. Da li želite da realizujete narudžbinu?"),
						function(e){
						    if(e){
								realization(selected_narudzbina_ids,1);	
						    }
						});
					}
				}
		});
	}

	$('#JSPorudzbinaStorniraj').on('click',function(){
		$.ajax({
			type: "POST",
			url: base_url + 'admin/ajax/promena_statusa_narudzbine',
			data: { status_target: 3, web_b2c_narudzbina_ids: selected_narudzbina_ids },
			success: function(response) {
				alertify.success(translate('Data akcija je izvršena'));

			  	$.each(response.ids, function( index, value ) {					  			
					$("#selectable").find('[data-id-narudzbina="'+ value +'"]').find('.JSOrderStatus').text('Stor.');
					$("#selectable").find('[data-id-narudzbina="'+ value +'"]').removeClass('nova').removeClass('prihvacena').addClass('stornirana');
				});
			}
		});		
	});

	$('#JSPorudzbinaObrisi').on('click',function(){
	  	alertify.confirm(translate("Da li ste sigurni da želite da izvršite akciju brisanja?"),
		function(e){
		    if(e){
				$.ajax({
					type: "POST",
					url: base_url + 'admin/porudzbina/delete-more',
					data: { web_b2c_narudzbina_ids: selected_narudzbina_ids },
					success: function(response) {
						alertify.success(translate('Data akcija je izvršena'));

					  	setTimeout(function(){
					  		location.reload(true);
					  	},1500);
					}
				});		
		    }
		});
	
	});

	$('#JSPorudzbinaStampajPdf').on('click',function(){
		window.open(base_url + 'admin/pdf/'+selected_narudzbina_ids.join('-'), '_blank');
	});
	$('#JSPorudzbinaStampajPonudu').on('click',function(){
		window.open(base_url + 'admin/pdf_ponuda/'+selected_narudzbina_ids.join('-'), '_blank');
	});
	$('#JSPorudzbinaStampajPredracun').on('click',function(){
		window.open(base_url + 'admin/pdf_predracun/'+selected_narudzbina_ids.join('-'), '_blank');
	});
	$('#JSPorudzbinaStampajPracun').on('click',function(){
		window.open(base_url + 'admin/pdf_racun/'+selected_narudzbina_ids.join('-'), '_blank');
	});

	$('#JSPorudzbinaStatus').on('click',function(){
		$('#JSchooseStatusModal').foundation('reveal', 'open');
	});

	$("#narudzbina_status_id").select2({
		placeholder: translate('Izaberi status')
	});

	$('.JSAddStatus').on("click", function(event) {
		var narudzbina_status_id = $('.JSAddStatusInput').val();
		if(narudzbina_status_id != 'null'){
	    	$.post(base_url+'admin/ajax/orders', {action:'dodela_statusa', orders_ids: selected_narudzbina_ids, narudzbina_status_id: narudzbina_status_id}, function (response){
			  	$.each(selected_narudzbina_ids, function( index, value ) {
					$("#selectable").find('[data-id-narudzbina="'+ value +'"]').find('.JSOrderAddedStatus').text(response.status_response[parseInt(value)]);
				});
			  	$.each(response.realized_ids, function( index, value ) {					  			
					$("#selectable").find('[data-id-narudzbina="'+ value +'"]').find('.JSOrderStatus').text('Prih.');
					$("#selectable").find('[data-id-narudzbina="'+ value +'"]').removeClass('nova').addClass('prihvacena');
				});
	    		alertify.success(translate('Data akcija je izvršena.'));
	    	 });
	    }else{
	    	alertify.error(translate('Nema selektovanih tipova!'));
	    }
	});

	//remove Status
	$('.JSRemoveStatus').on("click", function(event) {
		var narudzbina_status_id = $('.JSAddStatusInput').val();
		if(narudzbina_status_id != 'null'){
	    	$.post(base_url+'admin/ajax/orders', {action:'uklanjanje_statusa', orders_ids: selected_narudzbina_ids, narudzbina_status_id: narudzbina_status_id}, function (response){
			  	$.each(selected_narudzbina_ids, function( index, value ) {				  			
				  	$("#selectable").find('[data-id-narudzbina="'+ value +'"]').find('.JSOrderAddedStatus').text(response.status_response[parseInt(value)]);
				});
	    		alertify.success(translate('Data akcija je izvršena.'));
	    	 });
	    }else{
	    	alertify.error(translate('Nema selektovanih tipova!'));
	    }
	});

	$('input[name="import_file"]').on('change',function(){
		$('#JSOrdersImportForm').submit();
	});

	$('.JSSort').click(function(){
		var params = getUrlVars();
		params['sort_column'] = $(this).data('sort_column');
		params['sort_direction'] = $(this).data('sort_direction');
		delete params['page'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	$('#JSOrderExport').click(function(){
		var params = getUrlVars();
		params['export'] = 1;
		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});	


	});


function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value.replace('#','');
    });
    return vars;
}
