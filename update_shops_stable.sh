
shoppath=/var/www/html/auto_shops/;

for dir in $(find $shoppath* -maxdepth 0 -type d ); 
do
    if [ $dir != "$shoppath"loviribolov.selltico.com ] && [ $dir != "$shoppath"shrooms.selltico.com ];
    then

        cp -R /var/www/html/init_shop/app/controllers/. "$dir"/app/controllers/;
        cp -R /var/www/html/init_shop/app/models/. "$dir"/app/models/;
        cp -R /var/www/html/init_shop/app/views/. "$dir"/app/views/;
        cp /var/www/html/init_shop/app/filters.php "$dir"/app/filters.php;
        cp /var/www/html/init_shop/app/routes.php "$dir"/app/routes.php;
        cp -R /var/www/html/init_shop/css/. "$dir"/css/;
        cp -R /var/www/html/init_shop/js/. "$dir"/js/;
        cp -R /var/www/html/init_shop/files/exporti/. "$dir"/files/exporti/;
        cp -R /var/www/html/init_shop/app/commands/UpdateDatabaseCommand.php "$dir"/app/commands/UpdateDatabaseCommand.php;

        php "$dir"/artisan update:db;
    fi
done